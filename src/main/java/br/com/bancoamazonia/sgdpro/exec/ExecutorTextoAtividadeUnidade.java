package br.com.bancoamazonia.sgdpro.exec;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.dao.AvaliacaoAtividadeUnidadeDAO;
import br.com.bancoamazonia.sgdpro.modelo.AtividadeUnidadeSGD;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoAtividadeUnidadeModel;
import br.com.bancoamazonia.sgdpro.modelo.TextoAtividadeUnidadeSGD;

public class ExecutorTextoAtividadeUnidade {

	//private static final Logger LOG = Logger.getLogger(ExecutorTextoAtividadeUnidade.class);

	public static void main(String[] args) throws IOException {

		File file = new File(System.getProperty("user.home")+"/Desktop/ADU201712-Carga.sql");
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		try {
			String semestre = "2";
			String ano = "17";
			AvaliacaoAtividadeUnidadeDAO avaliacaoAtividadeUnidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
			SgdDAO sgdDAO = new SgdDAO();


			 // buscar todas as unidades que foram importadas

			List<AvaliacaoAtividadeUnidadeModel> listarTodasUnidadeAtividadeUnidade = avaliacaoAtividadeUnidadeDAO.listarTodasUnidadeAtividadeUnidade();

			for (AvaliacaoAtividadeUnidadeModel avaliacaoAtividadeUnidadeModel : listarTodasUnidadeAtividadeUnidade)
			{
					int indicador = 0;
					//System.out.println(avaliacaoAtividadeUnidadeModel);


					 // Verificar se está unidade tem acordo de trabalho

					AtividadeUnidadeSGD buscarAcordoTrabalhoPorPeriodoUnidade = sgdDAO.buscarAcordoTrabalhoPorPeriodoUnidade(avaliacaoAtividadeUnidadeModel.getUnidade(), semestre, ano);
					if(buscarAcordoTrabalhoPorPeriodoUnidade != null)
					{


						// Verificar se acordo tem texto do acordo


						TextoAtividadeUnidadeSGD buscarTextoAcordoPorPeriodoUnidade = sgdDAO.buscarTextoAcordoPorPeriodoUnidade(avaliacaoAtividadeUnidadeModel.getUnidade(), semestre, ano);


						 // Caso não exista acordo entre no se

						if(buscarTextoAcordoPorPeriodoUnidade != null)
						{
							StringBuilder sqlTexto = new StringBuilder();


							 // carregar todos os texto que fora importado em outro momento, pelo executor de Atividade Unidade

							List<AvaliacaoAtividadeUnidadeModel> atividadesMysqlPro = avaliacaoAtividadeUnidadeDAO.listarTodasAvaliacoesAtividadeUnidade(avaliacaoAtividadeUnidadeModel.getUnidade());
							if(atividadesMysqlPro != null)
							{
								for (AvaliacaoAtividadeUnidadeModel atividadeUnidade : atividadesMysqlPro) {
									indicador++;
									sqlTexto.append("Insert into SAD.texto_atividade_unidade (ID_TEXTO_ATIVIDADE_UNIDADE,TI_ATIVIDADE,TX_ATIVIDADE,ID_ATIVIDADE_UNIDADE) "
											+ "values (SQ_TEXTO_ATIVIDADE_UNIDADE.nexval,'Indicador "+indicador+"', '"+ atividadeUnidade.getDescricao()+"',"+ buscarAcordoTrabalhoPorPeriodoUnidade.getIdAtividadeUnidade() +"); \n");

									}

								writer.write(sqlTexto.toString());
								writer.newLine();
							}
							//LOG.info(sqlTexto.toString());
						}
					}
			}
			//Criando o conteúdo do arquivo
			writer.flush();
			//Fechando conexão e escrita do arquivo.
			writer.close();
			JOptionPane.showMessageDialog(null, "Serviço processamedo com suceso");
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*String[] unidades = {"GECON","GECOR","GESEC","GECIN","GEACE","GERAC","GEPEC","GEPRE","GERAN","GERED","GEREN","GEAUD"} ;


		AvaliacaoAtividadeUnidadeDAO unidadeDAO = new AvaliacaoAtividadeUnidadeDAO();

		int idAvaliacaoUnidadeCout = 650;
		int idAtividadeUnidadeCount = 61349;
		int idTextoAtividadeUnidadeCout = 2692;
		int periodo = 2;
		int ano = 2017;
		boolean gerarInsert = true;

		for (int i = 0; i < unidades.length; i++){
			++idAvaliacaoUnidadeCout;
			++idAtividadeUnidadeCount;

			StringBuilder sql = new StringBuilder();

			if(gerarInsert) {
				sql.append("Insert into SAD.avaliacao_unidade (ID_AVALIACAO_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,NU_MATRICULA_AVALIADOR,DT_AVALIACAO,DT_CIENCIA,NU_MATRICULA_CIENCIA,ANO_AVALIACAO) values"
						+ " ( "+ idAvaliacaoUnidadeCout +" ,"+ unidadeDAO.retornaCodigoUnidade(unidades[i]) +",'"+ periodo +"º semestre',null,to_date('31/12/17','DD/MM/RR'),null,null,'"+ ano +"'); ");

				sql.append("Insert into SAD.atividade_unidade (ID_ATIVIDADE_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,DT_ATIVIDADE) "
						+ "values ("+idAtividadeUnidadeCount+","+unidadeDAO.retornaCodigoUnidade(unidades[i])+",'"+ periodo +"º semestre',to_date('01/07/17','DD/MM/RR'));");
			} else {
				sql.append("Insert into SAD.atividade_unidade (ID_ATIVIDADE_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,DT_ATIVIDADE) "
						+ "values ("+idAtividadeUnidadeCount+","+unidadeDAO.retornaCodigoUnidade(unidades[i])+",'"+ periodo +"º semestre',to_date('01/07/17','DD/MM/RR'));");
			}

			writer.write(sql.toString());
			writer.newLine();

			//LOG.info(sql.toString());
			List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = unidadeDAO.listarTodasAvaliacoesAtividadeUnidade(unidades[i]);
			if(avaliacoesUnidadesCarga.size() > 0 && avaliacoesUnidadesCarga != null)
			{
				int indicador = 0;
				for (AvaliacaoAtividadeUnidadeModel avaliacaoAtividadeUnidadeModel : avaliacoesUnidadesCarga)
				{
					++idTextoAtividadeUnidadeCout;
					++indicador;
					StringBuilder sqlTexto = new StringBuilder();
					if(gerarInsert) {
						sqlTexto.append("Insert into SAD.texto_atividade_unidade (ID_TEXTO_ATIVIDADE_UNIDADE,TI_ATIVIDADE,TX_ATIVIDADE,ID_ATIVIDADE_UNIDADE) "
								+ "values ("+idTextoAtividadeUnidadeCout+",'Indicador "+indicador+"', '"+ avaliacaoAtividadeUnidadeModel.getDescricao()+"',"+idAtividadeUnidadeCount+");");
					} else {
						sqlTexto.append("Update SAD.texto_atividade_unidade SET ID_ATIVIDADE_UNIDADE = "+idAtividadeUnidadeCount
								+ " where ID_TEXTO_ATIVIDADE_UNIDADE= "+idTextoAtividadeUnidadeCout);
					}

					//LOG.info(sqlTexto.toString());
					writer.write(sqlTexto.toString());
					writer.newLine();

				}
			}
		}

		writer.flush();
		writer.close();
*/
	}

}
