package br.com.bancoamazonia.sgdpro.exec;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.RHDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.dto.EmpregadoDTO;
import br.com.bancoamazonia.sgdpro.modelo.dto.HistoricoLotacaoRHDTO;


public class ExecutorCarregarHistoricoLotacaoEmpregado {
	
	private static final Logger LOG = Logger.getLogger(ExecutorCarregarHistoricoLotacaoEmpregado.class);
	
	public static void main(String[] args) throws IOException {
		
	LOG.info("======================= INICIO PROCESSAMENTO =============================");
		SgdDAO sgdDAO = new SgdDAO();
		RHDAO rhdao = new RHDAO();
		
		String matricula = JOptionPane.showInputDialog("Por favor, Informe a matricula do empregado.");
		List<EmpregadoDTO> buscarTodosEmpregados = sgdDAO.buscarEmpregadoPorMatricula(matricula);
		String exporcao = JOptionPane.showInputDialog("Nome do arquivo para exportação");
		String arquivo = "C:\\CSADM\\SGD\\CARGA\\"+exporcao+".SQL";
		
		if(buscarTodosEmpregados != null){
			int idHistoricoLotacaoCount = 1;
			
			FileWriter fileWriter = new FileWriter(arquivo);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			StringBuilder conteudo = null;
			printWriter.printf("+ --------INICIO DE PROCESSAMENTO ---------+%n");
			for (EmpregadoDTO empregadoDTO : buscarTodosEmpregados) {
				conteudo = new StringBuilder();
					List<HistoricoLotacaoRHDTO> buscarHistoricoLotacaoPorMatricula = rhdao.buscarHistoricoLotacaoPorMatricula(empregadoDTO.getMatricula());
					if(buscarHistoricoLotacaoPorMatricula.size() > 0)
					{						
						conteudo.append(" DELETE FROM INTEGRA.SGD_HISTORICO_LOTACAO WHERE MATRICULA = '"+ empregadoDTO.getMatricula()+"' ; %n");
						
						printWriter.printf(conteudo.toString()+" %n");
						for (HistoricoLotacaoRHDTO historicoLotacaoRHDTO : buscarHistoricoLotacaoPorMatricula) {
							StringBuilder insert = new StringBuilder();
							idHistoricoLotacaoCount++;
							insert.append("INSERT INTO INTEGRA.SGD_HISTORICO_LOTACAO (MATRICULA,CODIGO_UNIDADE,DATA_INICIO_LOTACAO,DATA_FIM_LOTACAO,ID_HISTORICO_LOTACAO)  "
									+ "VALUES ('"+ empregadoDTO.getMatricula() +"','"+ historicoLotacaoRHDTO.getCodigoUnidade() 
									+"',TO_DATE('"+ historicoLotacaoRHDTO.getDataInicioLotacao()+"','DD/MM/RR'),TO_DATE('"+ ((historicoLotacaoRHDTO.getDataFimLotacao() != null) ? historicoLotacaoRHDTO.getDataFimLotacao() : "31/12/4712") +"','DD/MM/RR'),'"+ idHistoricoLotacaoCount +"');");
							
							printWriter.printf(insert.toString()+" %n");
						}
					}else{
						JOptionPane.showMessageDialog(null, "Registro de lotação não cadastrado no sistema do RH, informe imediatamente o gestor da GEPES");
					}
			}
			
			printWriter.printf("======================= FIM PROCESSAMENTO =============================");
			 fileWriter.close();
			JOptionPane.showMessageDialog(null,"\n Arquivo gravada com sucesso em "+ arquivo);
		}
		
	}

}
