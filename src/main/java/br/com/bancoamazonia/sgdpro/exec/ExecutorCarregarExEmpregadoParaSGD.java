package br.com.bancoamazonia.sgdpro.exec;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.RHDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.dto.SGDEmpregadoDTO;
import br.com.bancoamazonia.sgdpro.util.StringUtil;
import br.com.bancoamazonia.sgdpro.vo.AvaliacaoEmpregadoVO;

public class ExecutorCarregarExEmpregadoParaSGD {

	private static final Logger LOG = Logger.getLogger(ExecutorCarregarExEmpregadoParaSGD.class);

	public static void main(String[] args) {

		boolean validarEmpregado = true;

		JTextArea ta = new JTextArea(8, 50);
        ta.setWrapStyleWord(true);
        ta.setLineWrap(true);
        ta.setCaretPosition(0);
        ta.setEditable(false);
		do {

			try {


		String nomeConsultar = JOptionPane.showInputDialog("Informe o nome completo do Funcionario");

		/*
		 * verificar se existe o funcionario na base do sgd. caso, não exista executar consulta no rh para inserior no sgd
		 * */
		if(!"".equals(nomeConsultar))
		{
			SgdDAO sgdDAO = new SgdDAO();

				AvaliacaoEmpregadoVO buscarAvaliacaoExEmpregadoPorNome = sgdDAO.buscarAvaliacaoExEmpregadoPorNome(nomeConsultar);
				if(buscarAvaliacaoExEmpregadoPorNome != null){

					int showConfirmDialog = JOptionPane.showConfirmDialog(null, " Matricula: ["+ buscarAvaliacaoExEmpregadoPorNome.getMatricula() +"] - Nome: ["+ buscarAvaliacaoExEmpregadoPorNome.getNome() +"] \n Confirma dados do empregado?","Confirmação de Empregado", JOptionPane.YES_NO_OPTION);
					if(showConfirmDialog == JOptionPane.YES_OPTION) {
						validarEmpregado = false;
						JOptionPane.showMessageDialog(null, "Empregado já existe no SGD. Por Favor, acessar a aplicação web");

					}else if (showConfirmDialog == JOptionPane.NO_OPTION) {


						StringBuilder insert = new StringBuilder();
						insert.append(" INSERT INTO INTEGRA.SGD_EMPREGADO  (MATRICULA,NOME_EMPREGADO,CARGO,FUNCAO,DATA_INICIO_FUNCAO,CARATER_GESTOR,UNIDADE,CODIGO_DA_UNIDADE,MATRICULA_GESTOR_IMEDIATO,NOME_GESTOR_IMEDIATO) VALUES ");

						RHDAO rhdao = new RHDAO();
						SGDEmpregadoDTO buscarMatriculaEmpregadoPorNome = rhdao.buscarMatriculaEmpregadoPorNome(nomeConsultar);

						if (!StringUtil.isEmpty(buscarMatriculaEmpregadoPorNome.getMatricula()))
						{
							SGDEmpregadoDTO empregado = rhdao.buscarEmpregadoAtivoEInativoPorMatricula(buscarMatriculaEmpregadoPorNome.getMatricula());
							if (!StringUtil.isEmpty(empregado.getMatricula()))
							{
								insert.append(" ('" + empregado.getMatricula() + "','"
										+ empregado.getNomeEmpregado() + "','"+ empregado.getCargo() + "','"
										+ empregado.getFuncao() + "','"
										+ empregado.getDataIniciofuncao() + "','"
										+ empregado.getCaraterGestor() + "','"
										+ empregado.getUnidade() + "','"
										+ empregado.getCodigoUnidade() + "','"
										+ empregado.getMatriculaGestorImediato() + "','"
										+ empregado.getNomeGestorImediato() + "'); ");

								String texto = insert.toString().replaceAll("'null'", "null");
								LOG.info(texto);
								validarEmpregado = false;
								ta.setText(texto);
								JOptionPane.showMessageDialog(null, new JScrollPane(ta), "RESULT", JOptionPane.INFORMATION_MESSAGE);
							}
						}else{
							validarEmpregado =  true;
						}

					}

				}else{
					JOptionPane.showMessageDialog(null, "Buscando registro no RH do funcionario...");

					StringBuilder insert = new StringBuilder();
					insert.append(" INSERT INTO INTEGRA.SGD_EMPREGADO  (MATRICULA,NOME_EMPREGADO,CARGO,FUNCAO,DATA_INICIO_FUNCAO,CARATER_GESTOR,UNIDADE,CODIGO_DA_UNIDADE,MATRICULA_GESTOR_IMEDIATO,NOME_GESTOR_IMEDIATO) VALUES ");

					RHDAO rhdao = new RHDAO();
					SGDEmpregadoDTO buscarMatriculaEmpregadoPorNome = rhdao.buscarMatriculaEmpregadoPorNome(nomeConsultar);

					if (!StringUtil.isEmpty(buscarMatriculaEmpregadoPorNome.getMatricula()))
					{
						SGDEmpregadoDTO empregado = rhdao.buscarEmpregadoAtivoEInativoPorMatricula(buscarMatriculaEmpregadoPorNome.getMatricula());

						int empregadoRH = JOptionPane.showConfirmDialog(null, " Matricula: ["+ empregado.getMatricula() +"] - Nome: ["+ empregado.getNomeEmpregado() +"] \n Confirma dados do empregado?","Confirmação de Empregado", JOptionPane.YES_NO_OPTION);
						if(empregadoRH == JOptionPane.YES_OPTION) {

							insert.append(" ('" + empregado.getMatricula() + "','"
									+ empregado.getNomeEmpregado() + "','"+ empregado.getCargo() + "','"
									+ empregado.getFuncao() + "','"
									+ empregado.getDataIniciofuncao() + "','"
									+ empregado.getCaraterGestor() + "','"
									+ empregado.getUnidade() + "','"
									+ empregado.getCodigoUnidade() + "','"
									+ empregado.getMatriculaGestorImediato() + "','"
									+ empregado.getNomeGestorImediato() + "'); ");
							String texto = insert.toString().replaceAll("'null'", "null");
							LOG.info(texto);
							validarEmpregado = false;
							ta.setText(texto);
							JOptionPane.showMessageDialog(null, new JScrollPane(ta), "RESULT", JOptionPane.INFORMATION_MESSAGE);



						}else if (empregadoRH == JOptionPane.NO_OPTION) {
							validarEmpregado = true;
						}



					}else{
						JOptionPane.showMessageDialog(null, "Funcionario ["+nomeConsultar+"] não localizado, verifique outra forma de consultar");
						validarEmpregado =  true;
					}

				}

		}else{
			JOptionPane.showMessageDialog(null, "Informe um nome válido.");

		}

			} catch (NullPointerException e) {
				JOptionPane.showMessageDialog(null, "Funcionario não localizado : "+ e.getMessage());
				validarEmpregado = true;
			}
		} while (validarEmpregado);

	}

}
