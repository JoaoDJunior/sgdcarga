package br.com.bancoamazonia.sgdpro.exec;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.AvaliacaoAtividadeUnidadeDAO;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoAtividadeUnidadeModel;





public class Executor {
	
	private static final Logger LOG = Logger.getLogger(Executor.class);
	
	public static void main(String[] args) {
		
		/*
		 * PROCESSAR TODOS OS VALORES DA PLANILHA E ADICIONAR NA BASE DO MYSQL LOCAL
		 * */
		
//		new avaliacaoAtividadeUnidadeDAO().carregarRegistraArquivo("C:\\CSADM\\SGD\\2° semestre 2016\\Carga_1semestres.csv");
		
		/*
		 * ANALISA OS REGISTRO DO SGD PARA COMPARAR COM A PLANILHA, QUE FOI REALIZADO A CARGA
		 * */
//		new avaliacaoAtividadeUnidadeDAO().processarSGDCarga(null);
//		
		
		/*
		 * RECUPERAR TODOS OS REGISTROS DA BASE MYSQL LOCAL E ADICIONAR EM UM ARQUIVO PARA EXPORTAÇÃO 
		 * */
		SimpleDateFormat fmt = new SimpleDateFormat("dd_MM_yyyy__HHmmss"); 
		
		new AvaliacaoAtividadeUnidadeDAO().exportarArquivoCarga("C:\\CSADM\\SGD\\2° semestre 2016\\CARGA_2_SEMESTRE_2016_"+fmt.format(new Date())+".SQL");
//		new avaliacaoAtividadeUnidadeDAO().exportarArquivoCargaUpdate("C:\\CSADM\\SGD\\1 SEMESTRE 2016\\nova carga\\carga_1_semestre_2016.sql");

		
		String[] unidades = {"GEPES","GECOL","GEPAC","GEROF","GEMAF","GERPF","GERPJ","GERGC","SERED","GERAT","GERAN","GESNE","GEAFO","GEACE","GEPEC","GECON","GECOR","GERIS","GECIN","GESEC","SECTI","GPROT","GSIST","GPROD","GESOP","SEORP",
				"GEREO","GICOM","GSJUR","SEJUR","GCONS","GPROG","SEAUD","SECRE","OUVIDORIA"} ;
		
		
		AvaliacaoAtividadeUnidadeDAO unidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
		
		int idAvaliacaoUnidadeCout = 628;
		int idTextoAtividadeUnidadeCout = 2399;
		int periodo = 2;
		int ano = 2016;
		
		
		for (int i = 0; i < unidades.length; i++){
			++idAvaliacaoUnidadeCout;
			
			StringBuilder sql = new StringBuilder();
			sql.append("Insert into SAD.avaliacao_unidade (ID_AVALIACAO_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,NU_MATRICULA_AVALIADOR,DT_AVALIACAO,DT_CIENCIA,NU_MATRICULA_CIENCIA,ANO_AVALIACAO) values"
					+ " ( "+ idAvaliacaoUnidadeCout +" ,"+ unidadeDAO.retornaCodigoUnidade(unidades[i]) +",'"+ periodo +"º semestre',null,to_date('31/12/17','DD/MM/RR'),null,null,'"+ ano +"'); ");
			sql.append("Insert into SAD.atividade_unidade (ID_ATIVIDADE_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,DT_ATIVIDADE) "
					+ "values ("+idAvaliacaoUnidadeCout+","+unidadeDAO.retornaCodigoUnidade(unidades[i])+",'2º semestre',to_date('01/07/16','DD/MM/RR'));");
			
			LOG.info(sql.toString());
			List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = unidadeDAO.listarTodasAvaliacoesAtividadeUnidade(unidades[i]);
			if(avaliacoesUnidadesCarga.size() > 0 && avaliacoesUnidadesCarga != null)
			{
				int indicador = 0;
				for (AvaliacaoAtividadeUnidadeModel avaliacaoAtividadeUnidadeModel : avaliacoesUnidadesCarga) 
				{
					++idTextoAtividadeUnidadeCout;
					++indicador;
					StringBuilder sqlTexto = new StringBuilder();
					sqlTexto.append("Insert into SAD.texto_atividade_unidade (ID_TEXTO_ATIVIDADE_UNIDADE,TI_ATIVIDADE,TX_ATIVIDADE,ID_ATIVIDADE_UNIDADE) "
							+ "values ("+idTextoAtividadeUnidadeCout+",'Indicador "+indicador+"', '"+ avaliacaoAtividadeUnidadeModel.getDescricao()+"',"+idAvaliacaoUnidadeCout+");");
					
					LOG.info(sqlTexto.toString());
					
				}
			}
		}
		
		
		
		/*
		 * GERAR ARQUIVO
		 * */
		
		
		/*
		String[] unidades = {"GEPES","GECOL","GEPAC","GEROF","GEMAF","GERPF","GERPJ","GERGC","SERED","GERAT","GERAN","GESNE","GEAFO","GEACE","GEPEC","GECON","GECOR","GERIS","GECIN","GESEC","SECTI","GPROT","GSIST","GPROD","GESOP","SEORP",
				"GEREO","GICOM","GSJUR","SEJUR","GCONS","GPROG","SEAUD","SECRE","OUVIDORIA"} ;
		
		
		avaliacaoAtividadeUnidadeDAO unidadeDAO = new avaliacaoAtividadeUnidadeDAO();
		int valorAtual = 628;
		int valorTexto = 2399;
		
		try 
		{
			
			String caminhoExportacao = "C:"+File.separator +"CSADM"+File.separator +"SGD"+File.separator +"2° semestre 2016"+File.separator +"CARGATESTE.SQL";
			FileWriter fileWriter = new FileWriter(caminhoExportacao);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			
			printWriter.printf("+ --------INICIO DE PROCESSAMENTO ---------+%n");
			
			
			for (int i = 0; i < unidades.length; i++) 
			{
				++valorAtual;
				
				StringBuilder sql = new StringBuilder();
				sql.append("Insert into SAD.avaliacao_unidade (ID_AVALIACAO_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,NU_MATRICULA_AVALIADOR,DT_AVALIACAO,DT_CIENCIA,NU_MATRICULA_CIENCIA,ANO_AVALIACAO) values"
						+ " ( "+ valorAtual +" ,"+ unidadeDAO.retornaCodigoUnidade(unidades[i]) +",'2º semestre',null,to_date('31/12/17','DD/MM/RR'),null,null,'2016'); ");
				
				
				sql.append("Insert into SAD.atividade_unidade (ID_ATIVIDADE_UNIDADE,NU_UNIDADE,TX_PERIODO_AVALIACAO,DT_ATIVIDADE) "
						+ "values ("+valorAtual+","+unidadeDAO.retornaCodigoUnidade(unidades[i])+",'2º semestre',to_date('01/07/16','DD/MM/RR'));");
				
				LOG.info(sql.toString());
				printWriter.printf(sql.toString()+" %n");
				List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = unidadeDAO.getAvaliacoesUnidadesCarga(unidades[i]);
				if(avaliacoesUnidadesCarga.size() > 0 && avaliacoesUnidadesCarga != null)
				{
					int indicador = 0;
					for (AvaliacaoAtividadeUnidadeModel avaliacaoAtividadeUnidadeModel : avaliacoesUnidadesCarga) 
					{
						++valorTexto;
						++indicador;
						StringBuilder sqlTexto = new StringBuilder();
						
						sqlTexto.append("Insert into SAD.texto_atividade_unidade (ID_TEXTO_ATIVIDADE_UNIDADE,TI_ATIVIDADE,TX_ATIVIDADE,ID_ATIVIDADE_UNIDADE) "
								+ "values ("+ valorTexto +",'Indicador "+ indicador +"', '"+ avaliacaoAtividadeUnidadeModel.getDescricao() +"',"+valorAtual+");");
						
						LOG.info(sqlTexto.toString());
						//printWriter.printf(sqlTexto.toString()+" %n");
						
					}
				}
				
			}	
			
			printWriter.printf("+---------FIM DE PROCESSAMENTO-------------+%n");
			
			fileWriter.close();
			System.out.printf("\n Arquivo gravada com sucesso em "+ caminhoExportacao);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		*/
		
	}

}
