package br.com.bancoamazonia.sgdpro.exec;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.dto.HistoricoLotacaoDTO;
import br.com.bancoamazonia.sgdpro.util.FileUtil;

public class ExecutorTrocaUnidadeAvaliacaoEmpregado {

	private static final Logger LOG = Logger.getLogger(ExecutorTrocaUnidadeAvaliacaoEmpregado.class);

	public static void main(String[] args) {

		try {
			final String caminho = "C:\\CSADM\\SGD\\ADI\\2014\\201412\\alterar_empregado_unidade.csv";

			List<String> listaLinhaCarga = FileUtil.lerArquivo(caminho);
			String ano = "14";

			String dataSemestre = "30/12/" + ano;
//			String dataSegundoSemestre = "31/12/" + ano;
			LOG.info("======================== INICIO PROCESSAMENTO ================================================");
			if (listaLinhaCarga.size() > 0) {
				SgdDAO sgdDAO = new SgdDAO();

				for (int i = 1; i < listaLinhaCarga.size(); i++) {
					String[] split = listaLinhaCarga.get(i).split(";");
					if (split.length > 0) {
						if (FileUtil.validaCampoExportacao(split[0]) != null) {
							HistoricoLotacaoDTO buscarHistoricoLotacaoEmpregado = sgdDAO.buscarHistoricoLotacaoEmpregado(split[0].trim(), ano,
											sgdDAO.buscarCodigoAgencia(split[2].trim()));

							if (buscarHistoricoLotacaoEmpregado != null) {
								LOG.info("update INTEGRA.SGD_HISTORICO_LOTACAO set codigo_unidade = '"+sgdDAO.buscarCodigoAgencia(split[3].trim())+"' where id_historico_lotacao = "+buscarHistoricoLotacaoEmpregado.getIdHistoricoLotacao()+";");
							}
						}
					}

				}

			}

			LOG.info("=================================== FIM PROCESSAMENTO ===============================================");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
