package br.com.bancoamazonia.sgdpro.exec;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.dto.AcordoDTO;
import br.com.bancoamazonia.sgdpro.util.FileUtil;





public class ExecutorInserirAcordoForaPeriodo {
	
	private static final Logger LOG = Logger.getLogger(ExecutorInserirAcordoForaPeriodo.class);
	
	public static void main(String[] args) 
	{

		JTextArea ta = new JTextArea(8, 50);
        ta.setWrapStyleWord(true);
        ta.setLineWrap(true);
        ta.setCaretPosition(0);
        ta.setEditable(false);
	try {
			final String caminho = "C:\\CSADM\\SGD\\ACORDO\\EMPREGADOS\\201706\\empregado_sem_acordo_1_sem_2017_v2.csv";
			
			List<String> listaLinhaCarga = FileUtil.lerArquivo(caminho);
			String periodo = "1";
			String ano = "2017";
			String arquivo = "C:\\CSADM\\SGD\\ACORDO\\EMPREGADOS\\201706\\EMPREGADO_SEM_ACORDO_"+ano+"_.SQL";
			
			if(listaLinhaCarga.size() > 0)
			{
			FileWriter fileWriter = new FileWriter(arquivo);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			SgdDAO sgdDAO = new SgdDAO();
			printWriter.printf("+ --------INICIO DE PROCESSAMENTO ---------+%n");
				for (int i = 1; i < listaLinhaCarga.size(); i++) 
				{
					String[] split = listaLinhaCarga.get(i).split(";");
					if(split.length > 0)
					{
						if(FileUtil.validaCampoExportacao(split[0]) != null){
							AcordoDTO buscarAcordoTrabalhoPeriodoEmpregado = sgdDAO.buscarAcordoTrabalhoPeriodoEmpregado(split[0], ano);
							if(buscarAcordoTrabalhoPeriodoEmpregado == null){
								StringBuilder acordo = new StringBuilder();
								
								acordo.append(" Insert into ATIVIDADE_EMPREGADO (ID_ATIVIDADE_EMPREGADO,NU_MATRICULA_EMPREGADO,TX_PERIODO_AVALIACAO,TX_ATIVIDADE,DT_ATIVIDADE,ANO_AVALIACAO) ");
								acordo.append(" values (SQ_ATIVIDADE_EMPREGADO.nextval,'"+split[0]+"','"+periodo+"º semestre','"+split[2]+"',to_date('30/06/"+ano+"','DD/MM/RR'),'"+ano+"'); ");
								printWriter.printf(acordo.toString()+" %n");
							}
						}
					}

				}
				printWriter.printf("======================= FIM PROCESSAMENTO =============================");
				 fileWriter.close();
			
				
			}
			ta.setText(arquivo);
			JOptionPane.showMessageDialog(null, new JScrollPane(ta), "RESULT", JOptionPane.INFORMATION_MESSAGE);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
