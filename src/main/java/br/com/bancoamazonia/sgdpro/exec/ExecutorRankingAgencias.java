package br.com.bancoamazonia.sgdpro.exec;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.dto.RankAgenciaDTO;
import br.com.bancoamazonia.sgdpro.util.FileUtil;





public class ExecutorRankingAgencias {

	private static final Logger LOG = Logger.getLogger(ExecutorRankingAgencias.class);

	public static void main(String[] args)
	{

	try {
		/*
		 * Ranking Agência
		 * */
//			final String caminho = "C:\\CSADM\\SGD\\ranking\\1_semestre_2016\\rankings\\Carga\\Carga_Junho.csv";
			final String caminho = "C:\\CSADM\\SGD\\ranking\\2_semestre_2016\\super_carga.csv";

			List<String> listaLinhaCarga = FileUtil.lerArquivo(caminho);
			RankAgenciaDTO agenciaDTO = new RankAgenciaDTO();
			/*
			List<RankAgenciaDTO> rankingsAgenciasDTOs = new ArrayList<RankAgenciaDTO>();*/

			String periodo = "201612" ;

			if(listaLinhaCarga.size() > 0)
			{
//			AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
			SgdDAO sgdDAO = new SgdDAO();

				for (int i = 1; i < listaLinhaCarga.size(); i++)
				{
					String[] split = listaLinhaCarga.get(i).split(";");
					if(split.length > 0)
					{
						String codigoAgencia = split[0].trim();
						String nomeAgencia =  FileUtil.mudarSimbolosPorAcento(Normalizer.normalize(split[1].trim(), Normalizer.Form.NFD));

						String buscarCodigoAgencia = sgdDAO.buscarCodigoAgencia(FileUtil.mudarSimbolosPorAcento(nomeAgencia.trim()));
						agenciaDTO.setCdAgencia( (buscarCodigoAgencia != null) ? buscarCodigoAgencia : codigoAgencia);
						agenciaDTO.setQtDesempenhoGlobal(split[2]);
						agenciaDTO.setTxPeriodo(periodo);

						if(FileUtil.validaCampoExportacao(nomeAgencia) != null){

							List<RankAgenciaDTO> buscarRankAgenciaPorPeriodoCodigoAgencia = sgdDAO.buscarRankAgenciaPorPeriodoCodigoAgencia(agenciaDTO.getCdAgencia(), periodo);

							String sql = (!buscarRankAgenciaPorPeriodoCodigoAgencia.isEmpty())
									? " UPDATE perspectiva_agencia SET qt_desempenho_global = '"
											+ agenciaDTO.getQtDesempenhoGlobal() + "' WHERE TX_PERIODO = " + periodo
											+ " and CD_AGENCIA = "
											+ buscarRankAgenciaPorPeriodoCodigoAgencia.get(0).getCdAgencia() + "; "
									: " insert into perspectiva_agencia(tx_periodo, cd_agencia, qt_desempenho_global) values('"
											+ agenciaDTO.getTxPeriodo() + "' , '" + agenciaDTO.getCdAgencia() + "' , '"
											+ agenciaDTO.getQtDesempenhoGlobal() + "') ;";


							LOG.info(sql);

						}
					}

				}

				LOG.info("================================= FIM ANALISE ==================================");


			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<RankAgenciaDTO> execute(List<String> listaLinhaCarga, String periodo) {
		try {
			/*
			 * Ranking Agência
			 * */

			List<RankAgenciaDTO> buscarRankAgenciaPorPeriodoCodigoAgencia = new ArrayList<>();

			RankAgenciaDTO agenciaDTO = null;
				if(listaLinhaCarga.size() > 0)
				{
//				AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
				SgdDAO sgdDAO = new SgdDAO();

					for (int i = 1; i < listaLinhaCarga.size(); i++)
					{
						agenciaDTO = new RankAgenciaDTO();
						String[] split = listaLinhaCarga.get(i).split(";");
						if(split.length > 0)
						{
							String codigoAgencia = split[0].trim();
							String nomeAgencia =  FileUtil.mudarSimbolosPorAcento(Normalizer.normalize(split[1].trim(), Normalizer.Form.NFD));

							String buscarCodigoAgencia = sgdDAO.buscarCodigoAgencia(FileUtil.mudarSimbolosPorAcento(nomeAgencia.trim()));
							agenciaDTO.setCdAgencia( (buscarCodigoAgencia != null) ? buscarCodigoAgencia : codigoAgencia);
							agenciaDTO.setQtDesempenhoGlobal(split[2]);
							agenciaDTO.setTxPeriodo(periodo);

							/*if(FileUtil.validaCampoExportacao(nomeAgencia) != null) {

								buscarRankAgenciaPorPeriodoCodigoAgencia = sgdDAO.buscarRankAgenciaPorPeriodoCodigoAgencia(agenciaDTO.getCdAgencia(), periodo);

								String sql = (!buscarRankAgenciaPorPeriodoCodigoAgencia.isEmpty())
										? " UPDATE perspectiva_agencia SET qt_desempenho_global = '"
												+ agenciaDTO.getQtDesempenhoGlobal() + "' WHERE TX_PERIODO = " + periodo
												+ " and CD_AGENCIA = "
												+ buscarRankAgenciaPorPeriodoCodigoAgencia.get(0).getCdAgencia() + "; "
										: " insert into perspectiva_agencia(tx_periodo, cd_agencia, qt_desempenho_global) values('"
												+ agenciaDTO.getTxPeriodo() + "' , '" + agenciaDTO.getCdAgencia() + "' , '"
												+ agenciaDTO.getQtDesempenhoGlobal() + "') ;";
								System.out.println(sql);
							}*/
						}
						buscarRankAgenciaPorPeriodoCodigoAgencia.add(agenciaDTO);
					}

					return buscarRankAgenciaPorPeriodoCodigoAgencia;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		return null;
	}

}
