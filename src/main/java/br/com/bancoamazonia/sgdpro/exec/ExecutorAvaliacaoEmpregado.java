package br.com.bancoamazonia.sgdpro.exec;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoMysqlSGDPro;
import br.com.bancoamazonia.sgdpro.dao.AvaliacaoMySqlProDAO;
import br.com.bancoamazonia.sgdpro.dao.EmpregadoDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.Avaliacao;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;
import br.com.bancoamazonia.sgdpro.util.FileUtil;





public class ExecutorAvaliacaoEmpregado {

	private static final Logger LOG = Logger.getLogger(ExecutorAvaliacaoEmpregado.class);

	public void consultaAvaliacaodoEmpregado(String caminho, String periodo, String ano) {
		try {
//			final String caminho = "C:\\CSADM\\SGD\\2_SEMESTRE_2016\\7917.csv";

			List<String> listaLinhaCarga = FileUtil.lerArquivo(caminho);
			int numero = 0;
			int totalProcessamento = 0;
			int totalRegistro = 0;
			AvaliacaoEmpregado avaliacaoEmpregado = null;
			List<Avaliacao> avaliacoes = null;
			List<AvaliacaoEmpregado> avaliacoesEmpregados = new ArrayList<AvaliacaoEmpregado>();
//			String periodo = "2" ;
	//		String ano = "2016";

			if(listaLinhaCarga.size() > 0){
			AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
			SgdDAO sgdDAO = new SgdDAO();
			totalRegistro = sgdDAO.totalAvaliacoes();

			EmpregadoDAO empregadoDAO = new EmpregadoDAO();
				for (int i = 1; i < listaLinhaCarga.size(); i++)
				{
					String[] split = listaLinhaCarga.get(i).split(";");
					if(split.length > 0){
						if(FileUtil.validaCampoExportacao(split[0]) != null){
							AvaliacaoSGD buscarAvaliacaoFuncionario = sgdDAO.buscarAvaliacaoFuncionario(split[0], periodo, ano);

							if(buscarAvaliacaoFuncionario != null || buscarAvaliacaoFuncionario.getNuMatriculaEmpregado() != null)
							{

								Empregado empregado = empregadoDAO.consultaEmpregado(split[0]);
								Empregado gestor = empregadoDAO.consultaEmpregado(split[1]);
								if(empregado != null && gestor != null)
								{
									avaliacaoEmpregado = new AvaliacaoEmpregado();
									avaliacaoEmpregado.setAno(ano);
									avaliacaoEmpregado.setPeriodo(periodo);
									avaliacaoEmpregado.setEmpregado(empregado);
									avaliacaoEmpregado.setGestor(empregadoDAO.consultaEmpregado(split[1]));

									avaliacoes = new ArrayList<Avaliacao>();

									avaliacoes.add(new Avaliacao("TEMPO_ORGANIZACAO",FileUtil.verificarCampoNulo(split[3]), split[2]));
									avaliacoes.add(new Avaliacao("INICIATIVA",FileUtil.verificarCampoNulo(split[5]), split[4]));
									avaliacoes.add(new Avaliacao("TRABALHO_EM_EQUIPE",FileUtil.verificarCampoNulo(split[7]), split[6]));
									avaliacoes.add(new Avaliacao("COMUNICACAO",FileUtil.verificarCampoNulo(split[9]), split[8]));
									avaliacoes.add(new Avaliacao("FLEXIBILIDADE",FileUtil.verificarCampoNulo(split[11]), split[10]));
									avaliacoes.add(new Avaliacao("NEGOCIACAO",FileUtil.verificarCampoNulo(split[13]), split[12]));
									avaliacoes.add(new Avaliacao("TOMADA_DE_DECISAO",FileUtil.verificarCampoNulo(split[15]), split[14]));
									avaliacoes.add(new Avaliacao("VISAO_SISTEMICA_E_ESTRATEGICA",FileUtil.verificarCampoNulo(split[17]), split[16]));
									avaliacoes.add(new Avaliacao("RELACIONAMENTO_INTERPESSOAL",FileUtil.verificarCampoNulo(split[19]), split[18]));
									avaliacoes.add(new Avaliacao("RESPONSABILIDADE_SOCIAL",FileUtil.verificarCampoNulo(split[21]), split[20]));

									avaliacaoEmpregado.setAvaliacaos(avaliacoes);
									avaliacoesEmpregados.add(avaliacaoEmpregado);
									numero++;
								}

							}
						}
					}

				}

				LOG.info("================================= ANALISANDO DADOS ==================================");
				totalProcessamento =  (totalRegistro + numero);

				if(numero > 0)
				{
					int registro = 0;

					Connection conexao = ConexaoMysqlSGDPro.getConexao();
					PreparedStatement query = null;
					StringBuilder sql = new StringBuilder();
					sql.append(" INSERT INTO `avaliacao` (`Id`, `id_avaliacao_empregado`, `texto`, `valor_escore`, `justificativa`) VALUES (NULL, ?, ?, ?, ?); ");

					for (int i = totalRegistro; i < totalProcessamento; i++)
					{
						avaliacoesEmpregados.get(registro).setIdAvaliacaoEmpregado(String.valueOf(totalRegistro + registro));

						AvaliacaoEmpregado buscarAvaliacao = avaliacaoDAO.buscarAvaliacao(periodo, ano, avaliacoesEmpregados.get(registro).getEmpregado().getMatricula());
						if(buscarAvaliacao == null || buscarAvaliacao.getEmpregado() == null) {
							avaliacaoDAO.inserirAvaliacaoEmpregado(avaliacoesEmpregados.get(registro));
							for (Avaliacao av : avaliacoesEmpregados.get(registro).getAvaliacaos())
							{
								av.setIdAvaliacaoEmpregado(String.valueOf(totalRegistro + registro));
								query = conexao.prepareStatement(sql.toString());

								query.setString(1, FileUtil.verificarCampoNulo(av.getIdAvaliacaoEmpregado()));
								query.setString(2, FileUtil.verificarCampoNulo(av.getNome()));
								query.setString(3, FileUtil.verificarCampoNulo(av.getNota()));
								query.setString(4 , (av.getJustificativa() != null) ? new String(FileUtil.verificarCampoNulo(av.getJustificativa()).getBytes("UTF-8"), "UTF-8") : FileUtil.verificarCampoNulo(av.getJustificativa()));

								query.execute();
							}
							LOG.info(avaliacoesEmpregados.get(registro).toString());
							registro++;
						}

					}
					query.close();
					LOG.info("Total de Registro "+ avaliacoesEmpregados.size());

				}

			}
			JOptionPane.showMessageDialog(null, "Registro salvo no banco da MYSQL");


		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public static void main(String[] args)
	{

	try {
			final String caminho = "C:\\CSADM\\SGD\\ADI\\2016\\201612\\INSERIR AVALIACAO\\COLOCAR_AVALIACAO_FUNCIONARIO.csv";

			List<String> listaLinhaCarga = FileUtil.lerArquivo(caminho);
			int numero = 0;
			int totalProcessamento = 0;
			int totalRegistro = 0;
			AvaliacaoEmpregado avaliacaoEmpregado = null;
			List<Avaliacao> avaliacoes = null;
			List<AvaliacaoEmpregado> avaliacoesEmpregados = new ArrayList<AvaliacaoEmpregado>();
			String periodo = "2" ;
			String ano = "2016";

			if(listaLinhaCarga.size() > 0){
			AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
			SgdDAO sgdDAO = new SgdDAO();
			totalRegistro = sgdDAO.totalAvaliacoes();

			EmpregadoDAO empregadoDAO = new EmpregadoDAO();
				for (int i = 1; i < listaLinhaCarga.size(); i++)
				{
					String[] split = listaLinhaCarga.get(i).split(";");
					if(split.length > 0){
						if(FileUtil.validaCampoExportacao(split[0]) != null){
							AvaliacaoSGD buscarAvaliacaoFuncionario = sgdDAO.buscarAvaliacaoFuncionario(split[0], periodo, ano);

							if(buscarAvaliacaoFuncionario == null || buscarAvaliacaoFuncionario.getNuMatriculaEmpregado() == null)
							{

								Empregado empregado = empregadoDAO.consultaEmpregado(split[0]);
								Empregado gestor = empregadoDAO.consultaEmpregado(split[1]);
								if(empregado != null && gestor != null)
								{
									avaliacaoEmpregado = new AvaliacaoEmpregado();
									avaliacaoEmpregado.setAno(ano);
									avaliacaoEmpregado.setPeriodo(periodo);
									avaliacaoEmpregado.setEmpregado(empregado);
									avaliacaoEmpregado.setGestor(empregadoDAO.consultaEmpregado(split[1]));

									avaliacoes = new ArrayList<Avaliacao>();

									avaliacoes.add(new Avaliacao("TEMPO_ORGANIZACAO",FileUtil.verificarCampoNulo(split[3]), split[2]));
									avaliacoes.add(new Avaliacao("INICIATIVA",FileUtil.verificarCampoNulo(split[5]), split[4]));
									avaliacoes.add(new Avaliacao("TRABALHO_EM_EQUIPE",FileUtil.verificarCampoNulo(split[7]), split[6]));
									avaliacoes.add(new Avaliacao("COMUNICACAO",FileUtil.verificarCampoNulo(split[9]), split[8]));
									avaliacoes.add(new Avaliacao("FLEXIBILIDADE",FileUtil.verificarCampoNulo(split[11]), split[10]));
									avaliacoes.add(new Avaliacao("NEGOCIACAO",FileUtil.verificarCampoNulo(split[13]), split[12]));
									avaliacoes.add(new Avaliacao("TOMADA_DE_DECISAO",FileUtil.verificarCampoNulo(split[15]), split[14]));
									avaliacoes.add(new Avaliacao("VISAO_SISTEMICA_E_ESTRATEGICA",FileUtil.verificarCampoNulo(split[17]), split[16]));
									avaliacoes.add(new Avaliacao("RELACIONAMENTO_INTERPESSOAL",FileUtil.verificarCampoNulo(split[19]), split[18]));
									avaliacoes.add(new Avaliacao("RESPONSABILIDADE_SOCIAL",FileUtil.verificarCampoNulo(split[21]), split[20]));

									avaliacaoEmpregado.setAvaliacaos(avaliacoes);
									avaliacoesEmpregados.add(avaliacaoEmpregado);
									numero++;
								}

							}
						}
					}

				}

				LOG.info("================================= ANALISANDO DADOS ==================================");
				totalProcessamento =  (totalRegistro + numero);

				if(numero > 0)
				{
					int registro = 0;

					Connection conexao = ConexaoMysqlSGDPro.getConexao();
					PreparedStatement query = null;
					StringBuilder sql = new StringBuilder();
					sql.append(" INSERT INTO `avaliacao` (`Id`, `id_avaliacao_empregado`, `texto`, `valor_escore`, `justificativa`) VALUES (NULL, ?, ?, ?, ?); ");

					for (int i = totalRegistro; i < totalProcessamento; i++)
					{
						avaliacoesEmpregados.get(registro).setIdAvaliacaoEmpregado(String.valueOf(totalRegistro + registro));

						AvaliacaoEmpregado buscarAvaliacao = avaliacaoDAO.buscarAvaliacao(periodo, ano, avaliacoesEmpregados.get(registro).getEmpregado().getMatricula());
						if(buscarAvaliacao == null || buscarAvaliacao.getEmpregado() == null) {
							avaliacaoDAO.inserirAvaliacaoEmpregado(avaliacoesEmpregados.get(registro));
							for (Avaliacao av : avaliacoesEmpregados.get(registro).getAvaliacaos())
							{
								av.setIdAvaliacaoEmpregado(String.valueOf(totalRegistro + registro));
								query = conexao.prepareStatement(sql.toString());

								query.setString(1, FileUtil.verificarCampoNulo(av.getIdAvaliacaoEmpregado()));
								query.setString(2, FileUtil.verificarCampoNulo(av.getNome()));
								query.setString(3, FileUtil.verificarCampoNulo(av.getNota()));
								query.setString(4 , (av.getJustificativa() != null) ? new String(FileUtil.verificarCampoNulo(av.getJustificativa()).getBytes("UTF-8"), "UTF-8") : FileUtil.verificarCampoNulo(av.getJustificativa()));

								query.execute();
							}
							LOG.info(avaliacoesEmpregados.get(registro).toString());
							registro++;
						}

					}
					query.close();
					LOG.info("Total de Registro "+ avaliacoesEmpregados.size());

				}

			}
			JOptionPane.showMessageDialog(null, "Registro salvo no banco da MYSQL");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
