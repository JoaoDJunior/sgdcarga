package br.com.bancoamazonia.sgdpro.application.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoMysqlSGDPro;
import br.com.bancoamazonia.sgdpro.dao.AvaliacaoMySqlProDAO;
import br.com.bancoamazonia.sgdpro.dao.EmpregadoDAO;
import br.com.bancoamazonia.sgdpro.modelo.Avaliacao;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;
import br.com.bancoamazonia.sgdpro.util.FileUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.stage.Popup;
import javafx.stage.PopupBuilder;

public class FormularioInserirAvaliacaoController implements Initializable {

	@FXML
    private GridPane gridEmpregado;

    @FXML
    private TextField tfAvaliacao;

    @FXML
    private TextField tfMatricula;

    @FXML
    private TextField tfEmpregado;

    @FXML
    private TextField tfUnidade;

    @FXML
    private TextField tfPeriodo;

    @FXML
    private TextField tfAno;

    @FXML
    private GridPane gridAvaliacao;

    @FXML
    private Button btnInserir;

    @FXML
    private ComboBox<String> cbTempoOrganizacao;

    @FXML
    private ComboBox<String> cbIniciativa;

    @FXML
    private ComboBox<String> cbTrabalhoemEquipe;

    @FXML
    private ComboBox<String> cbComunicacao;

    @FXML
    private ComboBox<String> cbFlexibilidade;

    @FXML
    private ComboBox<String> cbNegociacao;

    @FXML
    private ComboBox<String> cbTomadadeDecisao;

    @FXML
    private ComboBox<String> cbVisaoSistemica;

    @FXML
    private ComboBox<String> cbRelacionamento;

    @FXML
    private ComboBox<String> cbResponsabilidade;

    @FXML
    private TextArea taTempoOrganizacao;

    @FXML
    private TextArea taIniciativa;

    @FXML
    private TextArea taTrabalhoEquipe;

    @FXML
    private TextArea taComunicacao;

    @FXML
    private TextArea taFlexibilidade;

    @FXML
    private TextArea taNegociacao;

    @FXML
    private TextArea taTomadaDecisao;

    @FXML
    private TextArea taVisaoSistemica;

    @FXML
    private TextArea taRelacionamento;

    @FXML
    private TextArea taResponsabilidade;

    private int id;
    private Empregado empregado;
	private String semestre;
	private String ano;

    public FormularioInserirAvaliacaoController(int avaliacaoId, Empregado empregado, String semestre, String ano) {
		this.id = avaliacaoId;
    	this.empregado = empregado;
		this.semestre = semestre;
		this.ano = ano;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		if(!(empregado.getMatricula() == null)) {

			tfAvaliacao.setText(String.valueOf(id));
			tfMatricula.setText(empregado.getMatricula());
			tfEmpregado.setText(empregado.getNome());
			tfUnidade.setText(empregado.getUnidade());
			tfPeriodo.setText(semestre);
			tfAno.setText(ano);
			tfAno.setEditable(false);
			tfAvaliacao.setEditable(false);
			tfEmpregado.setEditable(false);
			tfUnidade.setEditable(false);
			tfMatricula.setEditable(false);
			tfPeriodo.setEditable(false);
		}
		ObservableList<String> notas = FXCollections.observableArrayList("1","2","3","4","5","6","7");
		cbComunicacao.setItems(notas);
		cbComunicacao.valueProperty().addListener(verificaCampoNulo(taComunicacao));
		cbFlexibilidade.setItems(notas);
		cbFlexibilidade.valueProperty().addListener(verificaCampoNulo(taFlexibilidade));
		cbIniciativa.setItems(notas);
		cbIniciativa.valueProperty().addListener(verificaCampoNulo(taIniciativa));
		cbNegociacao.setItems(notas);
		cbNegociacao.valueProperty().addListener(verificaCampoNulo(taNegociacao));
		cbRelacionamento.setItems(notas);
		cbRelacionamento.valueProperty().addListener(verificaCampoNulo(taRelacionamento));
		cbResponsabilidade.setItems(notas);
		cbResponsabilidade.valueProperty().addListener(verificaCampoNulo(taResponsabilidade));
		cbTempoOrganizacao.setItems(notas);
		cbTempoOrganizacao.valueProperty().addListener(verificaCampoNulo(taTempoOrganizacao));
		cbTomadadeDecisao.setItems(notas);
		cbTomadadeDecisao.valueProperty().addListener(verificaCampoNulo(taTomadaDecisao));
		cbTrabalhoemEquipe.setItems(notas);
		cbTrabalhoemEquipe.valueProperty().addListener(verificaCampoNulo(taTrabalhoEquipe));
		cbVisaoSistemica.setItems(notas);
		cbVisaoSistemica.valueProperty().addListener(verificaCampoNulo(taVisaoSistemica));

		ObservableList<Node> teste = gridAvaliacao.getChildren();

		for(int i = 0;i < teste.size(); i++) {
			System.out.println(teste.get(i).toString());
		}
	}


	@FXML
	public void inserirAvaliacao(ActionEvent event) throws UnsupportedEncodingException {

		List<Avaliacao> avaliacoes = null;
		AvaliacaoEmpregado avaliacaoEmpregado = null;
		EmpregadoDAO empregadoDAO = new EmpregadoDAO();
		List<AvaliacaoEmpregado> avaliacoesEmpregados = new ArrayList<AvaliacaoEmpregado>();

		//Empregado gestor = empregadoDAO.consultaEmpregado(matrigestor);

		if(empregado != null/* && gestor != null*/) {

			avaliacaoEmpregado = new AvaliacaoEmpregado();
			avaliacaoEmpregado.setAno(ano);
			avaliacaoEmpregado.setPeriodo(tfPeriodo.getText());
			avaliacaoEmpregado.setEmpregado(empregado);
			//avaliacaoEmpregado.setGestor(empregadoDAO.consultaEmpregado(matricGestor));

			avaliacoes = new ArrayList<Avaliacao>();

			avaliacoes.add(new Avaliacao("TEMPO_ORGANIZACAO",FileUtil.verificarCampoNulo(taTempoOrganizacao.getText()), cbTempoOrganizacao.getValue()));
			avaliacoes.add(new Avaliacao("INICIATIVA",FileUtil.verificarCampoNulo(taIniciativa.getText()), cbIniciativa.getValue()));
			avaliacoes.add(new Avaliacao("TRABALHO_EM_EQUIPE",FileUtil.verificarCampoNulo(taTrabalhoEquipe.getText()), cbTrabalhoemEquipe.getValue()));
			avaliacoes.add(new Avaliacao("COMUNICACAO",FileUtil.verificarCampoNulo(taComunicacao.getText()), cbComunicacao.getValue()));
			avaliacoes.add(new Avaliacao("FLEXIBILIDADE",FileUtil.verificarCampoNulo(taFlexibilidade.getText()), cbFlexibilidade.getValue()));
			avaliacoes.add(new Avaliacao("NEGOCIACAO",FileUtil.verificarCampoNulo(taNegociacao.getText()), cbNegociacao.getValue()));
			avaliacoes.add(new Avaliacao("TOMADA_DE_DECISAO",FileUtil.verificarCampoNulo(taTomadaDecisao.getText()), cbTomadadeDecisao.getValue()));
			avaliacoes.add(new Avaliacao("VISAO_SISTEMICA_E_ESTRATEGICA",FileUtil.verificarCampoNulo(taVisaoSistemica.getText()), cbVisaoSistemica.getValue()));
			avaliacoes.add(new Avaliacao("RELACIONAMENTO_INTERPESSOAL",FileUtil.verificarCampoNulo(taRelacionamento.getText()), cbRelacionamento.getValue()));
			avaliacoes.add(new Avaliacao("RESPONSABILIDADE_SOCIAL",FileUtil.verificarCampoNulo(taResponsabilidade.getText()), cbResponsabilidade.getValue()));

			avaliacaoEmpregado.setAvaliacaos(avaliacoes);
			avaliacoesEmpregados.add(avaliacaoEmpregado);

			inserirAvaliacaonaBase(avaliacoesEmpregados);

		}



	}

	private ChangeListener<String> verificaCampoNulo(TextArea textArea) {

		ChangeListener<String> event = new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if(newValue.equals("6")) {
					textArea.setDisable(false);
					textArea.setPromptText("Por favor, preencha a justificativa para esta nota");;
				} else {
					textArea.setPromptText("");
					textArea.setDisable(true);
				}

			}
		};
		return event;
	}

	private void popupMensage() {

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Faltou informações");
		alert.setHeaderText(null);
		alert.setContentText("I have a great message for you!");

		alert.showAndWait();
	}

	private void inserirAvaliacaonaBase(List<AvaliacaoEmpregado> avaliacoesEmpregados) throws UnsupportedEncodingException {

		AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
		Connection conexao = ConexaoMysqlSGDPro.getConexao();
		PreparedStatement query = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO `avaliacao` (`Id`, `id_avaliacao_empregado`, `texto`, `valor_escore`, `justificativa`) VALUES (NULL, ?, ?, ?, ?); ");

		avaliacoesEmpregados.get(0).setIdAvaliacaoEmpregado(String.valueOf(tfAvaliacao.getText()));

		try {
			AvaliacaoEmpregado buscarAvaliacao = avaliacaoDAO.buscarAvaliacao(semestre, ano, avaliacoesEmpregados.get(0).getEmpregado().getMatricula());
			if(buscarAvaliacao == null || buscarAvaliacao.getEmpregado() == null) {
				avaliacaoDAO.inserirAvaliacaoEmpregado(avaliacoesEmpregados.get(0));
				for (Avaliacao av : avaliacoesEmpregados.get(0).getAvaliacaos()){
					av.setIdAvaliacaoEmpregado(String.valueOf(tfAvaliacao.getText()));
					query = conexao.prepareStatement(sql.toString());

					query.setString(1, FileUtil.verificarCampoNulo(av.getIdAvaliacaoEmpregado()));
					query.setString(2, FileUtil.verificarCampoNulo(av.getNome()));
					query.setString(3, FileUtil.verificarCampoNulo(av.getNota()));
					query.setString(4 , (av.getJustificativa() != null) ? new String(FileUtil.verificarCampoNulo(av.getJustificativa()).getBytes("UTF-8"), "UTF-8") : FileUtil.verificarCampoNulo(av.getJustificativa()));

					query.execute();
				}
			}
			query.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


}
