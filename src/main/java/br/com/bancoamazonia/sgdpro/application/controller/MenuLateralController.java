package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MenuLateralController implements Initializable {

	@FXML
    private Button btnInserirAvaliacao;

    @FXML
    private Button btnConsultarAvaliacao;

    @FXML
    private Button btnExportarAdu;

    @FXML
    private Button btnImportarAdu;

    @FXML
    private Button btnExportarAdi;

    @FXML
    private Button btnimportarAdi;

    @FXML
    private Button btnManual;

    private final String fxmlPath = "view/";

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

    @FXML
    void consultarAvaliacao(ActionEvent event) {
    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"ConsultarAvaliacao.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Consultar Avaliacões");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}
    }

    @FXML
    void exportarAdi(ActionEvent event) {
    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"ExportarADI.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Exportar ADI");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}

    }

    @FXML
    void exportarAdu(ActionEvent event) {
    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"ExportarADU.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Exportar ADU");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}
    }

    @FXML
    void importarAdi(ActionEvent event) {
    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"ImportarADI.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Importar ADI");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}
    }

    @FXML
    void importarAdu(ActionEvent event) {
    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"ImportarADU.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Importar ADU");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}
    }

    @FXML
    void inserirAvaliacao(ActionEvent event) {

    	try {
    		Stage palco = new Stage();
    		palco.initModality(Modality.APPLICATION_MODAL);
			Parent root = FXMLLoader.load(getClass().getResource(fxmlPath+"InserirAvaliacao.fxml"));
			Scene cena = new Scene(root);
			palco.setTitle("SGD: Inserir Avaliacões");
			palco.setScene(cena);
			palco.setResizable(false);
			palco.show();
		} catch (IOException e) {
			catchErrorLog(e);
		}

    }

    @FXML
    void manual(ActionEvent event) {

    }

    public void catchErrorLog(Exception e) {

    	Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Algo Aconteceu de errado");
		alert.setHeaderText("Aparentemente não foi encrontrado o arquivo");
		alert.setContentText("!");

		Exception ex = new IOException(e);

		// Create expandable Exception.
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String exceptionText = sw.toString();

		Label label = new Label("Descrição da Exceção");

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);

		alert.showAndWait();
    }

}
