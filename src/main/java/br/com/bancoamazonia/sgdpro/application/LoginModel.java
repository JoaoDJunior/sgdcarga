package br.com.bancoamazonia.sgdpro.application;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import br.com.pdcase.pdsegu.ws.AutenticaServiceWrapper;
import br.com.pdcase.pdsegu.ws.SeguServiceProxy;
import br.com.pdcase.pdsegu.ws.UsuarioBean;

public class LoginModel {
	Connection connection;

	public LoginModel() {
		/*connection = SQLConnection.Connector();
		if(connection == null) {
			//System.exit(1);
		}*/
	}

	public boolean isDBConnected(){
		try {
			return !connection.isClosed();
		} catch (Exception e) {
			return false;
		}
	}
		public boolean isLogin (String user, String pass) throws SQLException {
			try {
				AutenticaServiceWrapper validarUsuario = new SeguServiceProxy().validarUsuario(user, pass);
				String resultado = validarUsuario.getResultado();
				switch (resultado) {
				case "-3":
					JOptionPane.showMessageDialog(null, validarUsuario.getMensagem());
					return false;
				case "-4":
					JOptionPane.showMessageDialog(null, validarUsuario.getMensagem());
					return false;
				case "0":
					UsuarioBean usuario = validarUsuario.getUsuario();
					System.out.println(usuario.getNome());
					return true;
				default:
					break;
				}
			} catch (RemoteException e) {
				e.printStackTrace();
				return false;
			}
			return false;
		}

		public boolean isLogin2(String user, String pass) throws SQLException {
			PreparedStatement stmt = null;
			ResultSet resultSet = null;
			try {
				stmt = connection.prepareStatement("SELECT * FROM dados_colaboradores where id= ? and senha= ?");
				stmt.setString(1,user.toString());
				stmt.setString(2,pass.toString());

				resultSet = stmt.executeQuery();

				if (resultSet.next()) {
					return true;
				}
				else {
					return false;
				}
			} catch (SQLException  e) {
				throw new RuntimeException(e);
			} finally {
				resultSet.close();
				stmt.close();
			}
		}
	}

