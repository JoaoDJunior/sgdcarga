package br.com.bancoamazonia.sgdpro.application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLConnection {
	public static Connection Connector() {
		try {

			return DriverManager.getConnection("jdbc:mysql://localhost:3306/syscoffee?useTimezone=true&serverTimezone=UTC", "root", "");

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

}
