package br.com.bancoamazonia.sgdpro.application.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MenuUsuarioController implements Initializable {

	@FXML
	private Label userlbl;

	@FXML
	private Label lbl;

	@FXML
	private Button ImpADI;

	private String fxmlPath = "view/";

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
	public void GetUser(String user) {
		userlbl.setText(user);
	}

	public void InserirAvaliacao(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"InserirAvaliacao.fxml").openStream());
			InserirAvaliacaoController InserirAvaliacaoController = (InserirAvaliacaoController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Inserir Avalia��es");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void ExportarADU(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"ExportarADU.fxml").openStream());
			ExportarADUController ExportarADUController = (ExportarADUController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Exportar ADU");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}


	public void ImportarADU(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"ImportarADU.fxml").openStream());
			@SuppressWarnings("unused")
			ImportarADUController importarADUController = (ImportarADUController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setResizable(false);
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Importar ADU");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void ExportarADI(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"ExportarADI.fxml").openStream());
			ExportarADIController ExportarADIController = (ExportarADIController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Exportar ADI");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void ImportarADI(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"ImportarADI.fxml").openStream());
			ImportarADIController importarADIController = (ImportarADIController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setResizable(false);
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Importar ADI");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void ConsultarAvaliacao(ActionEvent event) {
		try {
			//((Node)event.getSource()).getScene().getWindow().hide();
			Stage secondaryStage = new Stage();
			secondaryStage.initModality(Modality.APPLICATION_MODAL);
			FXMLLoader loader2 = new FXMLLoader();
			Pane root = loader2.load(getClass().getResource(fxmlPath+"ConsultarAvaliacao.fxml").openStream());
			ConsultarAvaliacaoController consultarAvaliacaoController = (ConsultarAvaliacaoController)loader2.getController();
			Scene scene2 = new Scene(root);
			scene2.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			secondaryStage.setScene(scene2);
			secondaryStage.setTitle("Consultar Avaliações");
			secondaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void SignOut(ActionEvent event) {
		try {
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource(fxmlPath+"Login.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource(fxmlPath+"application.css").toExternalForm());
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}


}


