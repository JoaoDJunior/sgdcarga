package br.com.bancoamazonia.sgdpro.application.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.chart.AreaChart;

import javafx.scene.control.DatePicker;

public class GraficoAvaliacaoController implements Initializable{
	@FXML
	private DatePicker dpAnoInicio;
	@FXML
	private DatePicker dpAnoFim;
	@FXML
	private Button btnBuscar;
	@FXML
	private Label lblStatus;
	@FXML
	private AreaChart areaChartFuncionario;

	@Override
	public void initialize(URL location, ResourceBundle resources) {


	}

	@FXML
	public void buscar(ActionEvent event) {

	}
}
