package br.com.bancoamazonia.sgdpro.application;

import java.net.URL;
import java.util.ResourceBundle;

import br.com.bancoamazonia.sgdpro.application.controller.HomeUsuarioController;
import br.com.bancoamazonia.sgdpro.application.controller.MenuUsuarioController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class LoginController implements Initializable {
	public LoginModel loginModel = new LoginModel();

	@FXML
	private Label isConnected;

	@FXML
	private TextField txtUsuario;

	@FXML
	private TextField pwdUsuario;

	@FXML
	private Button btnLogin;

	@FXML
	private Button btnsair;

	public void initialize(URL location, ResourceBundle resources) {
		/*if(loginModel.isDBConnected()) {
			isConnected.setText("Connected");
		} else {
			isConnected.setText("Not Connected");
		}*/

		txtUsuario.setText("14208");
		pwdUsuario.setText("123");
	}

	public void Login (ActionEvent event) {
		try {
			if (loginModel.isLogin(txtUsuario.getText(),pwdUsuario.getText())) {
					isConnected.setText("usuario e senha correta");
					((Node)event.getSource()).getScene().getWindow().hide();
					Stage primaryStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("controller/view/HomeUsuario.fxml").openStream());
					HomeUsuarioController home = loader.getController();
					home.setUser(txtUsuario.getText());
		//			MenuUsuarioController menuUsuarioController = (MenuUsuarioController)loader.getController();
		//			menuUsuarioController.GetUser(txtUsuario.getText());
					Scene scene = new Scene(root);
					scene.getStylesheets().add(getClass().getResource("controller/view/application.css").toExternalForm());
					primaryStage.setResizable(true);
					primaryStage.setScene(scene);
					primaryStage.setTitle("Menu Principal");
					primaryStage.show();
				} else {
					isConnected.setText("Usu�rio e senha inv�lido");
				}
			}catch (Exception e) {
				isConnected.setText("usuario e senha incorreta");
				e.printStackTrace();
			}

	}
	@FXML
	void EnterLogin(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			try {
				if (loginModel.isLogin2(txtUsuario.getText(),pwdUsuario.getText())) {
					isConnected.setText("usuario e senha correta");
					((Node)event.getSource()).getScene().getWindow().hide();
					Stage primaryStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/application/MenuUsuario.fxml").openStream());
					MenuUsuarioController menuUsuarioController = (MenuUsuarioController)loader.getController();
					menuUsuarioController.GetUser(txtUsuario.getText());
					Scene scene = new Scene(root);
					scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
					primaryStage.setResizable(false);
					primaryStage.setScene(scene);
					primaryStage.setTitle("Menu Principal");
					primaryStage.show();


				} else {
				isConnected.setText("Usu�rio e senha inv�lido");
			}
			}	catch (Exception e) {
					isConnected.setText("usuario e senha incorreta");
					e.printStackTrace();
				}
		}

	}
	@FXML
	public void Fechar(ActionEvent event) {

        System.exit(0);
    }
}