package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import br.com.bancoamazonia.sgdpro.dao.AvaliacaoMySqlProDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.exec.ExecutorAvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.exportacao.ExportaAvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.exportacao.ExportaAvaliacaoEmpregadoCorrigir;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.TvAvaliacaoEmpregado;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImportarADIController implements Initializable {

	Task copyWorker;

	ImportarADIModel importarADIModel = new ImportarADIModel();

	@FXML
    private TabPane tpDados;

	@FXML
    private Tab tabAvaliacao;

	@FXML
    private Tab tabScript;

	private ObservableList<AvaliacaoSGD> dadosEmpregadosPlanilha = FXCollections.observableArrayList();

	@FXML
    private TableView<AvaliacaoSGD> tvDados;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcId;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcMatriculaEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcTxPeriodo;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcMatriculaAvaliador;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcDataAvaliacao;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcDataCiencia;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcAnoAvaliacao;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcCargoEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcFuncaoEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcNomeAvaliador;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcCodigoUnidade;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcUnidade;

	@FXML
	private ProgressBar pbStatus;

	@FXML
	public TextField tfCaminho;

	String caminho;

	@FXML
    private TextArea taScript;

	@FXML
    private Button btnGerarScript;

	@FXML
	private Button btnProcessar;

	@FXML
	private Button btnCancelar;

	@FXML
	private Button btnBuscar;

	@FXML
    private Label lblStatus;

	@FXML
	public ComboBox<String> cbSemestre;

	ObservableList<String> semestre = FXCollections.observableArrayList("1","2");

	@FXML
	public ComboBox<String> cbAno;

	ObservableList<String> ano = FXCollections.observableArrayList("2014","2015","2016","2017");

	public void initialize(URL location, ResourceBundle resources) {

		taScript.setEditable(false);

		btnGerarScript.setDisable(true);

		cbSemestre.setItems(semestre);

		cbAno.setItems(ano);

		tcId.setCellValueFactory(new PropertyValueFactory<>("idAvaliacaoEmpregado"));
		tcMatriculaEmpregado.setCellValueFactory(new PropertyValueFactory<>("nuMatriculaEmpregado"));
		tcTxPeriodo.setCellValueFactory(new PropertyValueFactory<>("txPeriodoAvaliacao"));
		tcMatriculaAvaliador.setCellValueFactory(new PropertyValueFactory<>("nuMatriculaAvaliador"));
		tcDataAvaliacao.setCellValueFactory(new PropertyValueFactory<>("dtAvaliacao"));
		tcDataCiencia.setCellValueFactory(new PropertyValueFactory<>("dtCiencia"));
		tcAnoAvaliacao.setCellValueFactory(new PropertyValueFactory<>("anoAvaliacao"));
		tcCargoEmpregado.setCellValueFactory(new PropertyValueFactory<>("cargoEmpregado"));
		tcFuncaoEmpregado.setCellValueFactory(new PropertyValueFactory<>("funcaoEmpregado"));
		tcNomeAvaliador.setCellValueFactory(new PropertyValueFactory<>("nomeAvaliador"));
		tcCodigoUnidade.setCellValueFactory(new PropertyValueFactory<>("codigoUnidade"));
		tcUnidade.setCellValueFactory(new PropertyValueFactory<>("nomeUnidade"));

	}

/*	private void buscaDados() {
		SgdDAO sgdDAO = new SgdDAO();
		sgdDAO.buscarAvaliacaoFuncionario(matricula, periodo, ano)
	} */

    @FXML
    void gerarScript(ActionEvent event) {

		ExportaAvaliacaoEmpregadoCorrigir exportaAvaliacaoEmpregadoCorrigir = new ExportaAvaliacaoEmpregadoCorrigir();
		List<String> dados = exportaAvaliacaoEmpregadoCorrigir.gerarSql();
		String dado = null;
		for(int i = 0; i <dados.size(); i++) {
			dado += "\n";
			dado += dados.get(i);
		}
		if(taScript.getText().isEmpty()) {
			taScript.setText(dado + "\n");
		} else {
			taScript.clear();
			taScript.setText(dado + "\n");
		}
    }

	private void listaDados() {

		SgdDAO sgdDAO = null;
		AvaliacaoSGD avaliacaoSGD = new AvaliacaoSGD();
		AvaliacaoMySqlProDAO avaliacaoMySqlProDAO = new AvaliacaoMySqlProDAO();
		List<AvaliacaoEmpregado> avaliacaoEmpregados = avaliacaoMySqlProDAO.listarAvaliacoes();

		AvaliacaoEmpregado avaliacaoEmpregado = null;
		for (int i = 0; i < avaliacaoEmpregados.size(); i++) {
			sgdDAO = new SgdDAO();
			avaliacaoEmpregado = new AvaliacaoEmpregado();
			avaliacaoEmpregado = avaliacaoEmpregados.get(i);
			avaliacaoSGD = sgdDAO.buscarAvaliacaoFuncionario(avaliacaoEmpregado.getEmpregado().getMatricula(), cbSemestre.getValue(), cbAno.getValue());

			if(avaliacaoSGD.getNuMatriculaEmpregado().isEmpty()) {

				lblStatus.setText("Avaliação não encontrada, favor importar dados.");

			} else {
				if(dadosEmpregadosPlanilha.isEmpty()) {
					dadosEmpregadosPlanilha.add(avaliacaoSGD);
					tvDados.setItems(dadosEmpregadosPlanilha);
					lblStatus.setText("Script gerado");
					btnGerarScript.setDisable(false);
				} else {
					dadosEmpregadosPlanilha.clear();
					dadosEmpregadosPlanilha.add(avaliacaoSGD);
					tvDados.refresh();
					lblStatus.setText("Script gerado");
					btnGerarScript.setDisable(false);
				}

			}

		}

	}

	public void ImportarPlanilha(ActionEvent event) {

		FileChooser chooser = new FileChooser();

		File file = chooser.showOpenDialog(btnBuscar.getScene().getWindow());
		tfCaminho.setText(file.getPath().toString());
		caminho = tfCaminho.getText();

	}
	private void saveFileRoutine(File file) throws IOException{
		String txt = tfCaminho.getText();
		file.createNewFile();
		FileWriter writer = new FileWriter(file);
		writer.write(txt);
		writer.close();
	}

	@FXML
	public void inicProcessamento (ActionEvent event) {
		ExecutorAvaliacaoEmpregado executorAvaliacaoEmpregado = new ExecutorAvaliacaoEmpregado();
		executorAvaliacaoEmpregado.consultaAvaliacaodoEmpregado(caminho, cbSemestre.getValue(), cbAno.getValue());
		listaDados();
	}

	public void Cancelar(ActionEvent event) {
		try {
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/application/MenuUsuario.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}


}

