package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import br.com.bancoamazonia.sgdpro.dao.RelatorioDAO;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoBean;
import br.com.bancoamazonia.sgdpro.modelo.DadosBean;
import br.com.bancoamazonia.sgdpro.modelo.EmpregadoBean;
import br.com.bancoamazonia.sgdpro.modelo.TvAvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.util.FileUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ConsultarAvaliacaoController implements Initializable {

	@FXML
    private Label lblStatus;

    @FXML
    private TextField tfMatricula;

    @FXML
    private Button btnBuscar;

    @FXML
    private Button btnGerar;

    @FXML
    private Button btnGrafico;

    @FXML
    private DatePicker dpAnoInicio;

    @FXML
    private DatePicker dpAnoFim;

    @FXML
	private ProgressBar pbStatus;

    @FXML
    private TableView<TvAvaliacaoEmpregado> tvEmpregado;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcMatricula;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcEmpregado;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcAvaliador;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcPeriodo;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcAno;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcIdAvaliacao;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, String> tcUnidade;

    @FXML
    private TableColumn<TvAvaliacaoEmpregado, CheckBox> tcSelection;

    private ObservableList<TvAvaliacaoEmpregado> empregado = FXCollections.observableArrayList();

    List<EmpregadoBean> empregadoBeans;

    String nome;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		System.out.println(new File("ImportarADIModel.class"));

	//	pbStatus.progressProperty().bind(tvEmpregado.prefWidthProperty());

		dpAnoInicio.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				LocalDate date = dpAnoInicio.getValue();
		        System.err.println("Selected date: " + date);

			}
		});

		dpAnoFim.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				LocalDate date = dpAnoFim.getValue();
		        System.err.println("Selected date: " + date);

			}
		});

		dpAnoInicio.setConverter(new javafx.util.StringConverter<LocalDate>() {
			String pattern = "yyyy";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

			{
				dpAnoInicio.setPromptText(pattern.toLowerCase());
			}

			@Override
			public String toString(LocalDate date) {
				if(date!=null) {
					return dateFormatter.format(date);
				} else {
					return "";
				}

			}

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
			}

		});


		javafx.util.StringConverter<LocalDate> converter = new javafx.util.StringConverter<LocalDate>() {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy");

			@Override
			public LocalDate fromString(String string) {
				if (string != null && !string.isEmpty()) {
					return LocalDate.parse(string, dateTimeFormatter);
				} else {
					return null;
				}
			}

			@Override
			public String toString(LocalDate date) {
				if (date != null) {
					return dateTimeFormatter.format(date);
				} else {
					return "";
				}
			}
		};

		dpAnoInicio.setConverter(converter);
		dpAnoInicio.setPromptText("yyyy");
		dpAnoFim.setConverter(converter);
		dpAnoFim.setPromptText("yyyy");

		tvEmpregado.setItems(empregado);

	}

	@FXML
	private void voltar() {

	}

	@FXML
	private void buscaAvaliacoes(ActionEvent event) {

		if(tfMatricula.getText().isEmpty()) {
			lblStatus.setText("Preencha todos os dados");
		} else {
			RelatorioDAO relatorioDAO = new RelatorioDAO();
			try {
				System.out.println(dpAnoInicio.getValue().toString() + dpAnoFim.getValue().toString());
				empregadoBeans = relatorioDAO.EmpregadoBeansDAO(tfMatricula.getText(),
						String.valueOf(dpAnoInicio.getValue().getYear()),
						String.valueOf(dpAnoFim.getValue().getYear()));
				setTableView(empregadoBeans);

			} catch (SQLException e) {
				catchErrorLog(e);
			}
		}
	}

	private void setTableView(List<EmpregadoBean> avaliacaoBeans) {

		tcSelection.setCellValueFactory(new PropertyValueFactory<>("selection"));
		tcIdAvaliacao.setCellValueFactory(new PropertyValueFactory<>("idAvaliacao"));
		tcMatricula.setCellValueFactory(new PropertyValueFactory<>("matricula"));
		tcEmpregado.setCellValueFactory(new PropertyValueFactory<>("empregado"));
		tcAvaliador.setCellValueFactory(new PropertyValueFactory<>("avaliador"));
		tcUnidade.setCellValueFactory(new PropertyValueFactory<>("unidade"));
		tcPeriodo.setCellValueFactory(new PropertyValueFactory<>("periodo"));
		tcAno.setCellValueFactory(new PropertyValueFactory<>("ano"));

		preparaTableView();

		if(tvEmpregado.getProperties().isEmpty()) {
			tvEmpregado.setItems(empregado);
		} else {
			tvEmpregado.getProperties().clear();
			tvEmpregado.setItems(empregado);
		}

	}

	private void preparaTableView() {

		List<DadosBean> dadosBeans = new ArrayList<DadosBean>();
		List<AvaliacaoBean> avaliacaoBeans = new ArrayList<AvaliacaoBean>();
		empregado = FXCollections.observableArrayList();

				@SuppressWarnings("unchecked")
				Service<ObservableList<TvAvaliacaoEmpregado>> service = new Service() {
                    protected Task createTask() {
                        return new Task() {
                            @Override
                            protected ObservableList<TvAvaliacaoEmpregado> call() throws Exception {
                                updateMessage("Importando . . .");
                                DadosBean dadosBean = null;
                                TvAvaliacaoEmpregado tvAvaliacaoEmpregado = null;
                                AvaliacaoBean avaliacaoBean = null;
                                EmpregadoBean empregadoBean = null;
                                for(int i = 0; i<empregadoBeans.size(); i++){
                                	Thread.sleep(200);
                                	dadosBean = new DadosBean();
                                	avaliacaoBean = new AvaliacaoBean();
                                	empregadoBean = new EmpregadoBean();
                                	tvAvaliacaoEmpregado = new TvAvaliacaoEmpregado();
                                	empregadoBean = empregadoBeans.get(i);
                                	dadosBeans.addAll(empregadoBean.getDadosBean());
                                	avaliacaoBeans.addAll(empregadoBean.getAvaliacaoBean());
                                	avaliacaoBean = avaliacaoBeans.get(i);
                                	dadosBean = dadosBeans.get(i);
                                	tvAvaliacaoEmpregado.setMatricula(dadosBean.getMAT());
                                	tvAvaliacaoEmpregado.setEmpregado(dadosBean.getNOME());
                                	tvAvaliacaoEmpregado.setAvaliador(dadosBean.getNOME_AVALIADOR());
                                	tvAvaliacaoEmpregado.setPeriodo(dadosBean.getTX_PERIODO_AVALIACAO());
                                	tvAvaliacaoEmpregado.setAno(String.valueOf(dadosBean.getANO_AVALIACAO()));
                                	tvAvaliacaoEmpregado.setUnidade(dadosBean.getNOME_UNIDADE());
                                	tvAvaliacaoEmpregado.setIdAvaliacao(String.valueOf(avaliacaoBean.getID_AVALIACAO_EMPREGADO()));
                                	tvAvaliacaoEmpregado.setSelection(new CheckBox());
                                	empregado.add(i, tvAvaliacaoEmpregado);
                                	updateProgress(i+1,empregadoBeans.size());
                        		}

                             //   lvArquivo.setItems(listadaPlanilha);
                                btnBuscar.setDisable(false);
                                updateMessage("Finished.");
                                return FXCollections.observableArrayList();
                            }
                        };
                    }
                };
                //btnProcessar.textProperty().bind(service.messageProperty());
                btnBuscar.disableProperty().bind(service.runningProperty());
                //lwArquivo.itemsProperty().bind(service.valueProperty());
                pbStatus.progressProperty().bind(service.progressProperty());
                service.stateProperty().addListener(new ChangeListener<Worker.State>() {
                    @Override
                    public void changed(ObservableValue<? extends Worker.State> observableValue, Worker.State oldState, Worker.State newState) {
                        if (newState == Worker.State.SUCCEEDED) {
                            System.out.println("This is ok, this thread " + Thread.currentThread() + " is the JavaFX Application thread.");
                            JOptionPane.showMessageDialog(null, "Importação concluída com sucesso!", "Pronto", 1);
                        }
                    }
                });
                service.start();
		}

	@FXML
	private void gerarRelatorio(ActionEvent event) {

		Map parametros = new HashMap();
		parametros.put("matricula", "0000");

		File jasperReport = new File(getClass().getResource("../../../../../../relatorios/Empregado.jasper").toString());

		System.out.println(jasperReport.exists() + jasperReport.getAbsolutePath());

		File outDir = new File(System.getProperty("user.home")+"/Downloads/");
		outDir.mkdir();

		EmpregadoBean empregadoBean = null;

		TvAvaliacaoEmpregado tvAvaliacaoEmpregado = null;

		List<EmpregadoBean> dados = null;

		try {
			for(int i = 0; i < empregadoBeans.size(); i++) {

				dados = new ArrayList<EmpregadoBean>();

				tvAvaliacaoEmpregado = new TvAvaliacaoEmpregado();

				tvAvaliacaoEmpregado = empregado.get(i);

				if(tvAvaliacaoEmpregado.getSelection().isSelected()) {
					empregadoBean = empregadoBeans.get(i);

					dados.add(empregadoBean);

					JRBeanCollectionDataSource bean = new JRBeanCollectionDataSource(dados);

					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport.getPath(), parametros, bean);

					JasperExportManager.exportReportToPdfFile(jasperPrint, outDir + empregadoBean.getNomedoArquivo()+".pdf");
				}
			}

		} catch (JRException e) {
			catchErrorLog(e);
		}
	}

    @FXML
    void setGrafico(ActionEvent event) {

    	try {
			AnchorPane anchorPane = FXMLLoader.load(getClass().getResource("view/GraficoAvaliacao.fxml"));

		} catch (IOException e) {
			catchErrorLog(e);
		}

    }

	private void Caminho(ActionEvent event) {

	}

	 private void catchErrorLog(Exception e) {

	    	Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Algo Aconteceu de errado");
			alert.setHeaderText("Aparentemente não foi encontrado o arquivo");
			alert.setContentText("!");
			Exception ex = null;
			if(e.getClass().getName().equals("IOException")) {
				ex = new IOException(e);
			} if(e.getClass().getName().equals("JRException")) {
				ex = new JRException(e);
			} if(e.getClass().getName().equals("SQLException")) {
				ex = new SQLException(e);
			}
			// Create expandable Exception.
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			String exceptionText = sw.toString();

			Label label = new Label("Descrição da Exceção");

			TextArea textArea = new TextArea(exceptionText);
			textArea.setEditable(false);
			textArea.setWrapText(true);

			textArea.setMaxWidth(Double.MAX_VALUE);
			textArea.setMaxHeight(Double.MAX_VALUE);
			GridPane.setVgrow(textArea, Priority.ALWAYS);
			GridPane.setHgrow(textArea, Priority.ALWAYS);

			GridPane expContent = new GridPane();
			expContent.setMaxWidth(Double.MAX_VALUE);
			expContent.add(label, 0, 0);
			expContent.add(textArea, 0, 1);

			// Set expandable Exception into the dialog pane.
			alert.getDialogPane().setExpandableContent(expContent);

			alert.showAndWait();
	    }

}
