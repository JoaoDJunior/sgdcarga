package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import br.com.bancoamazonia.sgdpro.dao.AvaliacaoAtividadeUnidadeDAO;
import br.com.bancoamazonia.sgdpro.exec.ExecutorRankingAgencias;
import br.com.bancoamazonia.sgdpro.modelo.dto.RankAgenciaDTO;
import br.com.bancoamazonia.sgdpro.util.FileUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ImportarADUController implements Initializable {

	private Task copyWorker;

	private File file;

	@FXML
	private Label lblStatus;;

	@FXML
	private ListView<String> lvArquivo;

	private ObservableList<String> listadaPlanilha = FXCollections.observableArrayList();

	@FXML
	private ProgressBar pbStatus;

	@FXML
	private TextField tfCaminho;

	@FXML
	private Button btnProcessar;

	@FXML
	private Button btnCancelar;

	@FXML
	private Button btnBuscar;

	@FXML
	private ComboBox<String> cbSemestre;

	private ObservableList<String> semestre = FXCollections.observableArrayList("1","2");

	@FXML
	private ComboBox<String> cbAno;

	private ObservableList<String> ano = FXCollections.observableArrayList("2014","2015","2016","2017","2018");

	@FXML
    private CheckBox cbAgencia;

    @FXML
    private CheckBox cbUnidade;

	private String caminho;

	private List<String> lista = new ArrayList<String>();

	private String periodo;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		cbSemestre.setItems(semestre);

		cbAno.setItems(ano);

		btnProcessar.setDisable(true);

		btnCancelar.setDisable(true);

		cbAgencia.addEventHandler(ActionEvent.ANY, new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if(cbAgencia.isSelected()) {
					cbUnidade.setSelected(false);
				}
			}
		});

		cbUnidade.addEventHandler(ActionEvent.ANY, new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				if(cbUnidade.isSelected()) {
					cbAgencia.setSelected(false);
				}
			}
		});

		tfCaminho.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(!(cbSemestre.getValue().isEmpty() && cbAno.getValue().isEmpty() && tfCaminho.getText().isEmpty())
						&& (cbAgencia.isSelected() || cbUnidade.isSelected())) {
					btnProcessar.setDisable(false);
				} else {
					btnProcessar.setDisable(true);
				}
			}
		});

		lvArquivo.itemsProperty().addListener(new ChangeListener<ObservableList<String>>() {

			@Override
			public void changed(ObservableValue<? extends ObservableList<String>> observable, ObservableList<String> oldValue, ObservableList<String> newValue) {
				if(!newValue.isEmpty()) {
					try {
						inserirAvaliacaoRank();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
		});
	}

	private void listaPlanilhaImportada() throws Exception {
		FileUtil fileUtil = new FileUtil();
		List<String> lista = new ArrayList<String>();
		lista = fileUtil.lerArquivo(file.getAbsolutePath());
		for(int i =0; i<lista.size(); i++) {
			String dado;
			dado = lista.get(i);
			listadaPlanilha.add(i, dado);
		}
		lvArquivo.setItems(listadaPlanilha);
		btnProcessar.setDisable(false);
	}

	public void ImportarPlanilha(ActionEvent event) {

		FileChooser chooser = new FileChooser();

		File file = chooser.showOpenDialog(btnBuscar.getScene().getWindow());
		if(file.exists() && (file.length()>0)) {
			tfCaminho.setText(file.getPath().toString());
			caminho = tfCaminho.getText();
		} else {
			file = null;
		}

	}

	public void inicProcessamento (ActionEvent event) throws SQLException {

		AvaliacaoAtividadeUnidadeDAO atividadeUnidadeDao = new AvaliacaoAtividadeUnidadeDAO();
		btnCancelar.setDisable(false);
		pbStatus.prefWidthProperty().bind(lvArquivo.prefWidthProperty());
		btnProcessar.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				@SuppressWarnings("unchecked")
				Service<ObservableList<String>> service = new Service() {
					protected Task createTask() {
						return new Task() {
							@Override
							protected ObservableList<String> call() throws Exception {
								updateMessage("Importando . . .");
								lista.clear();
								lista = FileUtil.lerArquivo(caminho);
								if(cbUnidade.isSelected()) {
									for(int i = 0; i < lista.size(); i++) {
										String[] split = lista.get(i).split(";");
										listadaPlanilha.add(i, lista.get(i));
										atividadeUnidadeDao.carregarRegistraArquivo(split);
										updateProgress(i,lista.size());
										Thread.sleep(30);
									}
								} else {
									inserirAvaliacaoRank();
								}
								lvArquivo.setItems(listadaPlanilha);
								btnProcessar.setDisable(false);
								updateMessage("Finished.");
								return FXCollections.observableArrayList();
							}
						};
					}
				};
				//btnProcessar.textProperty().bind(service.messageProperty());
				btnProcessar.disableProperty().bind(service.runningProperty());
		//		lvArquivo.itemsProperty().bind(service.valueProperty());
				pbStatus.progressProperty().bind(service.progressProperty());
				service.stateProperty().addListener(new ChangeListener<Worker.State>() {
					@Override
					public void changed(ObservableValue<? extends Worker.State> observableValue, Worker.State oldState, Worker.State newState) {

						if (newState == Worker.State.SUCCEEDED) {
							/*try {
								inserirAvaliacaoRank();
							} catch (SQLException e) {
								e.printStackTrace();
							}*/
							System.out.println("This is ok, this thread " + Thread.currentThread() + " is the JavaFX Application thread.");
							JOptionPane.showMessageDialog(null, "Importação concluída com sucesso!", "Pronto", 1);
							btnProcessar.setText("Pronto!");
						}
					}
				});
				service.start();
			}
		});

	}

	public void Cancelar(ActionEvent event) {
		try {
			((Node)event.getSource()).getScene().getWindow().hide();
			Stage primaryStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("view/MenuUsuario.fxml").openStream());
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	private void inserirAvaliacaoRank() throws SQLException {
		if(cbAgencia.isSelected()) {
			if(cbSemestre.getValue().equals("1")) {
				periodo = cbAno.getValue() + "06";
			} else if(cbSemestre.getValue().equals("2")) {
				periodo = cbAno.getValue() + "12";
			}
			ExecutorRankingAgencias rank = new ExecutorRankingAgencias();
			List<RankAgenciaDTO> rankAgencia = new ArrayList<>();
			rankAgencia = rank.execute(lista, periodo);
			AvaliacaoAtividadeUnidadeDAO unidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
			unidadeDAO.inserirRankdasAgencias(rankAgencia);
		} else if(cbUnidade.isSelected()) {

		}
	}
}
