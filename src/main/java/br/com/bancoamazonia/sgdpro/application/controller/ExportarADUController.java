package br.com.bancoamazonia.sgdpro.application.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.dao.AvaliacaoAtividadeUnidadeDAO;
import br.com.bancoamazonia.sgdpro.modelo.TvRankAgencia;
import br.com.bancoamazonia.sgdpro.modelo.dto.RankAgenciaDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.fxml.Initializable;

public class ExportarADUController implements Initializable{

	@FXML
    private ComboBox<String> cbSemestre;

	private ObservableList<String> semestre = FXCollections.observableArrayList("1","2");

    @FXML
    private ComboBox<String> cbAno;

    private ObservableList<String> ano = FXCollections.observableArrayList("2014","2015","2016","2017","2018");

    @FXML
    private Button btnBuscar;

    @FXML
    private Button btnExportarSQL;

    @FXML
    private Tab tabDados;

    @FXML
    private TableView<TvRankAgencia> tvDados;

    ObservableList<TvRankAgencia> listaRankAgencias = FXCollections.observableArrayList();

    @FXML
    private TableColumn<TvRankAgencia, String> tcId;

    @FXML
    private TableColumn<TvRankAgencia, String> tcPeriodo;

    @FXML
    private TableColumn<TvRankAgencia, String> tcAgencia;

    @FXML
    private TableColumn<TvRankAgencia, String> tcDesempenho;

    @FXML
    private Tab taSqcript;

    @FXML
    private TextArea taScript;

    private String periodo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    	cbSemestre.setItems(semestre);

		cbAno.setItems(ano);

		btnExportarSQL.setDisable(true);

		tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
		tcPeriodo.setCellValueFactory(new PropertyValueFactory<>("txPeriodo"));
		tcAgencia.setCellValueFactory(new PropertyValueFactory<>("cdAgencia"));
		tcDesempenho.setCellValueFactory(new PropertyValueFactory<>("qtDesempenho"));
    }

    @FXML
    public void buscarNaBase(ActionEvent event) throws SQLException {

    	//if(cbAgencia.isSelected()) {
			if(cbSemestre.getValue().equals("1")) {
				periodo = cbAno.getValue() + "06";
			} else if(cbSemestre.getValue().equals("2")) {
				periodo = cbAno.getValue() + "12";
			}
    	//}
    	AvaliacaoAtividadeUnidadeDAO unidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
		listaRankAgencias = unidadeDAO.buscaDadosRankAgencia(periodo);
		setTabview();
    }

    private void setTabview() {

    	if(!listaRankAgencias.isEmpty()) {
    		btnExportarSQL.setDisable(false);
    		tvDados.setItems(listaRankAgencias);

    	} else {

    	}

    }

    @FXML
    public void exportarSQL(ActionEvent event) {

    	if(!listaRankAgencias.isEmpty()) {

    		TvRankAgencia tvRankAgencia = null;

    		RankAgenciaDTO agenciaDTO = null;

    		List<RankAgenciaDTO> buscarRankAgenciaPorPeriodoCodigoAgencia = new ArrayList<>();

    		SgdDAO sgdDAO = new SgdDAO();

    		List<String> dados = new ArrayList<>();

    		for(int i = 0; i < listaRankAgencias.size(); i++) {

    			agenciaDTO = new RankAgenciaDTO();

    			tvRankAgencia = new TvRankAgencia();

    			tvRankAgencia = listaRankAgencias.get(i);

    			agenciaDTO.setTxPeriodo(tvRankAgencia.getTxPeriodo());

    			agenciaDTO.setCdAgencia(tvRankAgencia.getCdAgencia());

    			agenciaDTO.setQtDesempenhoGlobal(tvRankAgencia.getQtDesempenho());

    			buscarRankAgenciaPorPeriodoCodigoAgencia = sgdDAO.buscarRankAgenciaPorPeriodoCodigoAgencia(agenciaDTO.getCdAgencia(), periodo);

				String sql = (!buscarRankAgenciaPorPeriodoCodigoAgencia.isEmpty())
						? " UPDATE perspectiva_agencia SET qt_desempenho_global = '"
								+ agenciaDTO.getQtDesempenhoGlobal() + "' WHERE TX_PERIODO = " + periodo
								+ " and CD_AGENCIA = "
								+ buscarRankAgenciaPorPeriodoCodigoAgencia.get(0).getCdAgencia() + "; "
						: " insert into perspectiva_agencia(tx_periodo, cd_agencia, qt_desempenho_global) values('"
								+ agenciaDTO.getTxPeriodo() + "' , '" + agenciaDTO.getCdAgencia() + "' , '"
								+ agenciaDTO.getQtDesempenhoGlobal() + "') ;";

				dados.add(sql);
		}

    		String dado = null;
    		for(int i = 0; i <dados.size(); i++) {
    			dado += "\n";
    			dado += dados.get(i);
    		}
    		if(taScript.getText().isEmpty()) {
    			taScript.setText(dado + "\n");
    		} else {
    			taScript.clear();
    			taScript.setText(dado + "\n");
    		}

    	} else {

    	}


    }


}
