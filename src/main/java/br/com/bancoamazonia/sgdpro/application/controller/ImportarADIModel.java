package br.com.bancoamazonia.sgdpro.application.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

public class ImportarADIModel implements Initializable {

	@FXML
	public ListView<String> lwArquivo;

	@FXML
	public ProgressBar progresso;

	@FXML
	public static TextField tfCaminho;

	@FXML
	public TextField tfData;

	@FXML
	public Button btnProcessar;

	@FXML
	public Button btnCancelar;

	@FXML
	public Button Buscar;

	@FXML
	public ComboBox<String> semestre;

	ObservableList<String> selectSemestre = FXCollections.observableArrayList("1","2");

	@FXML
	public ComboBox<String> ano;

	ObservableList<String> selectAno = FXCollections.observableArrayList("2014","2015","2016","2017");

	@Override
	public void initialize(URL location, ResourceBundle resources) {


	}

	public ListView<String> getLwArquivo() {
		return lwArquivo;
	}

	public void setLwArquivo(ListView<String> lwArquivo) {
		this.lwArquivo = lwArquivo;
	}

	public ProgressBar getProgresso() {
		return progresso;
	}

	public void setProgresso(ProgressBar progresso) {
		this.progresso = progresso;
	}

	public static TextField getTfCaminho() {
		return tfCaminho;
	}

	public static void setTfCaminho(TextField tfCaminho) {
		ImportarADIModel.tfCaminho = tfCaminho;
	}

	public TextField getTfData() {
		return tfData;
	}

	public void setTfData(TextField tfData) {
		this.tfData = tfData;
	}

	public Button getBtnProcessar() {
		return btnProcessar;
	}

	public void setBtnProcessar(Button btnProcessar) {
		this.btnProcessar = btnProcessar;
	}

	public Button getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(Button btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public Button getBuscar() {
		return Buscar;
	}

	public void setBuscar(Button buscar) {
		Buscar = buscar;
	}

	public ComboBox<String> getSemestre() {
		return semestre;
	}

	public void setSemestre(ComboBox<String> semestre) {
		this.semestre = semestre;
	}

	public ComboBox<String> getAno() {
		return ano;
	}

	public void setAno(ComboBox<String> ano) {
		this.ano = ano;
	}



}
