package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.jfree.data.time.FixedMillisecond;

import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class InserirAvaliacaoController implements Initializable {

	@FXML
    private MenuBar menuBar;

	@FXML
    private AnchorPane apOptions;

    @FXML
    private Button btnBuscar;

    @FXML
    private TableView<AvaliacaoSGD> tvDados;

    private ObservableList<AvaliacaoSGD> dadosEmpregadosPlanilha = FXCollections.observableArrayList();

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcId;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcMatriculaEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcTxPeriodo;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcMatriculaAvaliador;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcDataAvaliacao;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcDataCiencia;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcAnoAvaliacao;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcCargoEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcFuncaoEmpregado;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcNomeAvaliador;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcCodigoUnidade;

    @FXML
    private TableColumn<AvaliacaoSGD, String> tcUnidade;

    @FXML
    private TextField tfMatricula;

    @FXML
	public ComboBox<String> cbSemestre;

	ObservableList<String> semestre = FXCollections.observableArrayList("1","2");

	@FXML
	public ComboBox<String> cbAno;

	ObservableList<String> ano = FXCollections.observableArrayList("2014","2015","2016","2017");


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    	apOptions.setVisible(false);

    	cbSemestre.setItems(semestre);

		cbAno.setItems(ano);

		tcId.setCellValueFactory(new PropertyValueFactory<>("idAvaliacaoEmpregado"));
		tcMatriculaEmpregado.setCellValueFactory(new PropertyValueFactory<>("nuMatriculaEmpregado"));
		tcTxPeriodo.setCellValueFactory(new PropertyValueFactory<>("txPeriodoAvaliacao"));
		tcMatriculaAvaliador.setCellValueFactory(new PropertyValueFactory<>("nuMatriculaAvaliador"));
		tcDataAvaliacao.setCellValueFactory(new PropertyValueFactory<>("dtAvaliacao"));
		tcDataCiencia.setCellValueFactory(new PropertyValueFactory<>("dtCiencia"));
		tcAnoAvaliacao.setCellValueFactory(new PropertyValueFactory<>("anoAvaliacao"));
		tcCargoEmpregado.setCellValueFactory(new PropertyValueFactory<>("cargoEmpregado"));
		tcFuncaoEmpregado.setCellValueFactory(new PropertyValueFactory<>("funcaoEmpregado"));
		tcNomeAvaliador.setCellValueFactory(new PropertyValueFactory<>("nomeAvaliador"));
		tcCodigoUnidade.setCellValueFactory(new PropertyValueFactory<>("codigoUnidade"));
		tcUnidade.setCellValueFactory(new PropertyValueFactory<>("nomeUnidade"));
    }

    @FXML
    void buscarAvaliacao(ActionEvent event) {
    		SgdDAO sgdDAO = new SgdDAO();
    		AvaliacaoSGD avaliacaoSGD = new AvaliacaoSGD();
			avaliacaoSGD = sgdDAO.listarAvaliacoes(cbSemestre.getValue(), cbAno.getValue(), tfMatricula.getText());

			if(!(avaliacaoSGD.getNuMatriculaAvaliador() == null)) {

				if(dadosEmpregadosPlanilha.isEmpty()) {
					dadosEmpregadosPlanilha.add(avaliacaoSGD);
					tvDados.setItems(dadosEmpregadosPlanilha);
					//			lblStatus.setText("Script gerado");
					//			btnGerarScript.setDisable(false);
				} else {
					dadosEmpregadosPlanilha.clear();
					dadosEmpregadosPlanilha.add(avaliacaoSGD);
					tvDados.refresh();
					//			lblStatus.setText("Script gerado");
					//			btnGerarScript.setDisable(false);
				}

			} else {
				//		lblStatus.setText("Avaliação não encontrada, favor importar dados.");
				dadosEmpregadosPlanilha.clear();

				tvDados.refresh();

			}
	    	try {
	    		FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MenuInserirAvaliacao.fxml"));
	    		loader.setController(new MenuInserirAvaliacaoController(dadosEmpregadosPlanilha, tfMatricula.getText(), cbSemestre.getValue(), cbAno.getValue()));
	    		Parent root = loader.load();
	    		apOptions.getChildren().add(root);
	    		apOptions.setVisible(true);
			} catch (IOException e) {
				e.printStackTrace();
			}

    }


}
