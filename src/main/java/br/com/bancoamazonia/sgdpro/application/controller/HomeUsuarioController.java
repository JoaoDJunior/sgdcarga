package br.com.bancoamazonia.sgdpro.application.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.transitions.hamburger.HamburgerNextArrowBasicTransition;

import br.com.bancoamazonia.sgdpro.dao.EmpregadoDAO;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;
import br.com.bancoamazonia.sgdpro.modelo.EmpregadoBean;
import br.com.bancoamazonia.sgdpro.modelo.TvAvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.TvAvaliacaoUnidade;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.fxml.Initializable;

public class HomeUsuarioController implements Initializable {

	@FXML
    private StackPane rootPane;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private BorderPane borderPane;

    @FXML
    private MenuBar menuBar;

    @FXML
    private AnchorPane centerAnchorPane;

    @FXML
    private JFXTabPane jfxTabPane;

    @FXML
    private Tab tabEmpregado;

    @FXML
    private VBox vbDados;

    @FXML
    private GridPane gpEmpregado;

    @FXML
    private Label lblNomeEmpegado;

    @FXML
    private Label lblMatricula;

    @FXML
    private Label lblUnidade;

    @FXML
    private Label lblFuncao;

    @FXML
    private Label lblCodUnd;

    @FXML
    private Label lblCargo;

    @FXML
    private TableView<TvAvaliacaoEmpregado> tvEmpregado;

    @FXML
    private Tab tabUnidade;

    @FXML
    private VBox vbUnidade;

    @FXML
    private GridPane gpUnidade;

    @FXML
    private Label lblCodUnidade;

    @FXML
    private Label lblNomeUnidade;

    @FXML
    private Label lblMatriculaGestor;

    @FXML
    private Label lblNomeGestor;

    @FXML
    private TableView<TvAvaliacaoUnidade> tvUnidade;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private JFXDrawer drawerMenu;

    private ObservableList<TvAvaliacaoEmpregado> empregado = FXCollections.observableArrayList();

    private ObservableList<TvAvaliacaoUnidade> unidade = FXCollections.observableArrayList();

    List<EmpregadoBean> empregadoBeans;

	private String user;

    public static AnchorPane rootP;


	public void initialize(URL location, ResourceBundle resources) {

		 rootP = anchorPane;
	//	 buscarDadosEmpregado(user);
	//	 buscarDadosUnidade();
		 try {
	            VBox box = FXMLLoader.load(getClass().getResource("view/MenuLateral.fxml"));
	            drawerMenu.setSidePane(box);
	     } catch (IOException ex) {
	            Logger.getLogger(HomeUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
	     }

	     HamburgerNextArrowBasicTransition transition = new HamburgerNextArrowBasicTransition(hamburger);
	     transition.setRate(-1);
	     hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED,(Event e)->{
	         transition.setRate(transition.getRate()*-1);
	         transition.play();
	         if(drawerMenu.isShown()) {
	           	 drawerMenu.close();
	         }else {
	        	 drawerMenu.open();
	         }
	        });

	     tvEmpregado.setItems(empregado);
	     tvUnidade.setItems(unidade);
	}


	private void buscarDadosUnidade() {

	}

	private void buscarDadosEmpregado(String user) {
		Empregado empregado = new Empregado();
		EmpregadoDAO empregadoDao = new EmpregadoDAO();
		try {
			empregado = empregadoDao.consultaEmpregado(user);
			setLabels(empregado);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public void setUser(String user) {
		this.user = user;
	}

	private void setLabels(Empregado empregado) {

		if(!(empregado.getMatricula() == null)) {
			lblMatricula.setText(empregado.getMatricula());
			lblNomeEmpegado.setText(empregado.getNome());
			lblUnidade.setText(empregado.getUnidade());
			lblFuncao.setText(empregado.getFuncao());
			lblCodUnd.setText(empregado.getCodigoUnidade());
			lblCargo.setText(empregado.getCargo());
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Aviso");
			alert.setHeaderText("Erro na consulta do empregado");
			alert.setContentText("Empregado não consta na base de dados!");
			alert.showAndWait();
		}

	}

	private void setTabview() {



	}

	private void collumnsFactory() {

	}

}
