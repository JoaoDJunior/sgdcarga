package br.com.bancoamazonia.sgdpro.application.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.print.attribute.standard.DialogTypeSelection;

import br.com.bancoamazonia.sgdpro.dao.EmpregadoDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;

public class MenuInserirAvaliacaoController implements Initializable {
	@FXML
	private Button btnInserir;
	@FXML
	private Button btnAlterar;
	@FXML
	private Button btnBuscar;
	@FXML
	private Button btnDeletar;
	@FXML
	private TextField tfCaminho;

	private ObservableList<AvaliacaoSGD> dadosEmpregadosPlanilha;
	private String matricula;
	private String semestre;
	private String ano;

	public MenuInserirAvaliacaoController(ObservableList<AvaliacaoSGD> dadosEmpregadosPlanilha, String matricula, String semestre, String ano) {
		this.dadosEmpregadosPlanilha = dadosEmpregadosPlanilha;
		this.matricula = matricula;
		this.semestre = semestre;
		this.ano = ano;
	}

	public void initialize(URL location, ResourceBundle resources) {

	}

	@FXML
	public void inserirAvaliacao(ActionEvent event) {

		if((tfCaminho.getText().isEmpty()) && (dadosEmpregadosPlanilha.isEmpty())) {
			Empregado empregado = new Empregado();
			EmpregadoDAO empregadoDAO = new EmpregadoDAO();
			SgdDAO sgdDAO = new SgdDAO();
			empregado = empregadoDAO.consultaEmpregado(matricula);
			int avaliacaoId = sgdDAO.totalAvaliacoes();
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FormularioInserirAvaliacao.fxml"));
				loader.setController(new FormularioInserirAvaliacaoController(avaliacaoId, empregado, semestre, ano));
				Parent pane = loader.load();
				Scene cena = new Scene(pane);
				Stage palco = new Stage();
				palco.setScene(cena);
				palco.initModality(Modality.APPLICATION_MODAL);
				palco.setResizable(false);
				palco.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Encontramos algumas inconsistências!");
			alert.setHeaderText("O empregado já possui avaliação no periodo selecionado! \nSiga as instruções abaixo!");
			alert.setContentText("1. Verifique se a avaliação a qual deseja inserir, está no periodo correto. \n2. ");

			alert.showAndWait();

		}

	}

	@FXML
	public void alterarAvaliacao(ActionEvent event) {

		if(tfCaminho.getText().isEmpty() && dadosEmpregadosPlanilha.isEmpty()) {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FormularioInserirAvaliacao.fxml"));
	//			loader.setController(new FormularioInserirAvaliacaoController(dadosEmpregadosPlanilha));
				Parent pane = loader.load();
				Scene cena = new Scene(pane);
				Stage palco = new Stage();
				palco.setScene(cena);
				palco.initModality(Modality.APPLICATION_MODAL);
				palco.setResizable(false);
				palco.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if(dadosEmpregadosPlanilha.isEmpty()) {

			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Encontramos algumas inconsistências!");
			alert.setHeaderText("O empregado já possui avaliação no periodo selecionado! \nSiga as instruções abaixo!");
			alert.setContentText("1. Verifique se a avaliação a qual deseja inserir, está no periodo correto. \n2. ");

			alert.showAndWait();


		} else {
			carregarDadosdaPlanilha();
		}
	}


	@FXML
	public void buscar(ActionEvent event) {

	}

	@FXML
	public void deletarAvaliacao(ActionEvent event) {

	}
	private void carregarDadosdaPlanilha() {

		ProgressBar barradeprogresso = new ProgressBar();

		Service<Void> service = new Service<Void>() {
		    @Override
		    protected Task<Void> createTask() {
		        return new Task<Void>() {
		            @Override
		            protected Void call()
		                    throws InterruptedException {
		                updateMessage("Finding friends . . .");
		                updateProgress(0, 10);
		                for (int i = 0; i < 10; i++) {
		                    Thread.sleep(300);
		                    updateProgress(i + 1, 10);
		                    updateMessage("Found " + (i + 1) + " friends!");
		                }
		                updateMessage("Found all.");
		                return null;
		            }
		        };
		    }
		};
		barradeprogresso.progressProperty().bind(service.progressProperty());

		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Carregando registros");

		alert.getDialogPane().setExpandableContent(barradeprogresso);

		alert.showAndWait();

		service.start();


	}
}
