package br.com.bancoamazonia.sgdpro.dao;

public class SqlConsultas {

	public String SqlDados() {

	    StringBuilder sqlDados = new StringBuilder();

	    sqlDados.append("select ai.*, ai2.lotacao, dbms_lob.substr( TX_ATIVIDADE, 4000, 1 ) tx_atividade from ( ");
	    sqlDados.append("select b.employee_number mat, ");
	    sqlDados.append("NU_MATRICULA_EMPREGADO, ");
	    sqlDados.append("b.last_name nome, ");
	    sqlDados.append("a.TX_PERIODO_AVALIACAO, ");
	    sqlDados.append("a.ANO_AVALIACAO, ");
	    sqlDados.append("a.DT_AVALIACAO, ");
	    sqlDados.append("a.CARGO_EMPREGADO, ");
	    sqlDados.append("a.FUNCAO_EMPREGADO, ");
	    sqlDados.append("a.NOME_UNIDADE, ");
	    sqlDados.append("a.CODIGO_UNIDADE, ");
	    sqlDados.append("c.employee_number mat_avaliador, ");
	    sqlDados.append("c.last_name nome_avaliador, ");
	    sqlDados.append("a.ID_AVALIACAO_EMPREGADO ");
	    sqlDados.append("from AVALIACAO_EMPREGADO a, ");
	    sqlDados.append("apps.per_all_people_f@DBL_HR_PROD.BASA.COM b, ");
	    sqlDados.append("apps.per_all_people_f@DBL_HR_PROD.BASA.COM c ");
	    sqlDados.append("where ltrim(a.NU_MATRICULA_EMPREGADO) =ltrim(b.employee_number) ");
	    sqlDados.append("and   ltrim(a.NU_MATRICULA_AVALIADOR) =ltrim(c.employee_number) ");
	    sqlDados.append("and b.effective_start_date = (select max(a1.effective_start_date) ");
	    sqlDados.append("                                        from apps.per_all_people_f@DBL_HR_PROD.BASA.COM a1 ");
	    sqlDados.append("                                        where a1.person_id=b.person_id) ");
	    sqlDados.append("and c.effective_start_date = (select max(a1.effective_start_date) ");
	    sqlDados.append("                                        from apps.per_all_people_f@DBL_HR_PROD.BASA.COM a1 ");
	    sqlDados.append("                                        where a1.person_id=c.person_id) ");
	    sqlDados.append("   and b.employee_number= ? AND a.ANO_AVALIACAO between ? and ?  ) ai ");
			sqlDados.append("   left join (select ltrim(rtrim(b.employee_number)) matricula  , ");
			sqlDados.append("                 ltrim(rtrim(b.last_name))       nome  , ");
			sqlDados.append("                e.name lotacao, ");
			sqlDados.append("                f1.date_from inicio, ");
			sqlDados.append("                f1.date_to fim, ");
			sqlDados.append("                d.meaning motivo ");
			sqlDados.append("                                   from PER_PERSON_ANALYSES@DBL_HR_PROD.BASA.COM f1, ");
			sqlDados.append("                                   apps.PER_ANALYSIS_CRITERIA@DBL_HR_PROD.BASA.COM g1, ");
			sqlDados.append("                                   apps.per_all_people_f@DBL_HR_PROD.BASA.COM          b, ");
			sqlDados.append("                                   apps.hr_all_organization_units@DBL_HR_PROD.BASA.COM e, ");
			sqlDados.append("                                   apps.fnd_lookup_values@DBL_HR_PROD.BASA.COM d ");
			sqlDados.append("                                  where f1.analysis_criteria_id = g1.analysis_criteria_id ");
			sqlDados.append("                                    and  f1.ID_FLEX_NUM=50334 ");
			sqlDados.append("                                     and b.person_id = f1.person_id ");
			sqlDados.append("                                     and e.organization_id= g1.segment2 ");
			sqlDados.append("                                     and d.lookup_type = 'EMP_ASSIGN_REASON' ");
			sqlDados.append("                                     and d.language = 'PTB' ");
			sqlDados.append("                                     and d.lookup_code = g1.segment1 ");
			sqlDados.append("                                     and b.EFFECTIVE_START_DATE in ( ");
			sqlDados.append("                                         select max(EFFECTIVE_START_DATE) from apps.per_all_people_f@DBL_HR_PROD.BASA.COM  b1 ");
			sqlDados.append("                                         where b1.person_id=b.person_id ) ");
			sqlDados.append("                                     and rtrim(b.employee_number)= ? ");
			sqlDados.append("                                     order by f1.date_from desc) ai2 ");
			sqlDados.append("on ai.mat=ai2.matricula ");
			sqlDados.append("and ai.dt_avaliacao between ai2.inicio and nvl(ai2.fim,sysdate) ");
			sqlDados.append("left join ATIVIDADE_EMPREGADO c ");
			sqlDados.append("on c.NU_MATRICULA_EMPREGADO = ai.NU_MATRICULA_EMPREGADO ");
			sqlDados.append("and c.TX_PERIODO_AVALIACAO=ai.TX_PERIODO_AVALIACAO ");
			sqlDados.append("and c.ano_avaliacao= ai.ano_avaliacao ");
			sqlDados.append("order by dt_avaliacao DESC ");

			return sqlDados.toString();
	}

	public String SqlAvaliacao() {

		StringBuilder sqlAvaliacao = new StringBuilder();

		sqlAvaliacao.append("SELECT ");
		sqlAvaliacao.append("avaliacao.ID_AVALIACAO_EMPREGADO, ");
		sqlAvaliacao.append("avaliacao.NU_MATRICULA_EMPREGADO, ");
		sqlAvaliacao.append("p_avaliacao.ID_ESCORE, ");
		sqlAvaliacao.append("padrao.NO_PADRAO_DESEMPENHO, ");
		sqlAvaliacao.append("padrao.DS_PADRAO_DESEMPENHO, ");
		sqlAvaliacao.append("p_avaliacao.TX_JUSTIFICATIVA_GESTOR, ");
		sqlAvaliacao.append("acao.TX_ACAO_DESENVOLVIMENTO ");

		sqlAvaliacao.append("from avaliacao_empregado avaliacao ");
		sqlAvaliacao.append("inner join AVALIACAO_EMPDO_PADRAO_DSMHO p_avaliacao on avaliacao.id_avaliacao_empregado = p_avaliacao.id_avaliacao_empregado ");
		sqlAvaliacao.append("inner join acao_desenvolvimento acao on avaliacao.id_avaliacao_empregado = acao.id_avaliacao_empregado ");
		sqlAvaliacao.append("INNER JOIN PADRAO_DESEMPENHO padrao ON p_avaliacao.id_padrao_desempenho = padrao.id_padrao_desempenho ");
		sqlAvaliacao.append("where avaliacao.nu_matricula_empregado = ? and avaliacao.tx_periodo_avaliacao like ?  and avaliacao.ano_avaliacao= ? ");

		return sqlAvaliacao.toString();
	}

	public String SqlIndicadores() {

		StringBuilder sqlIndicadores = new StringBuilder();

		sqlIndicadores.append(" SELECT ");
		sqlIndicadores.append(" AVALIACAO.NU_UNIDADE, ");
		sqlIndicadores.append(" AVALIACAO.TX_PERIODO_AVALIACAO, ");
		sqlIndicadores.append(" AVALIACAO.ANO_AVALIACAO, ");
		sqlIndicadores.append(" TEXTO.TI_ATIVIDADE, ");
		sqlIndicadores.append(" TEXTO.TX_ATIVIDADE, ");
		sqlIndicadores.append(" ATIVIDADE.PERC_DESEMPENHO_REALIZADO, ");
		sqlIndicadores.append(" ATIVIDADE.QT_DESEMPENHO_PREVISTO, ");
		sqlIndicadores.append(" ATIVIDADE.QT_DESEMPENHO_REALIZADO, ");
		sqlIndicadores.append(" ATIVIDADE.DESVIO, ");
		sqlIndicadores.append(" ATIVIDADE.NOTA, ");
		sqlIndicadores.append(" ATIVIDADE.PESO ");
		sqlIndicadores.append(" FROM avaliacao_unidade_atividade ATIVIDADE ");
		sqlIndicadores.append(" INNER JOIN avaliacao_unidade AVALIACAO ON ATIVIDADE.id_avaliacao_unidade = AVALIACAO.id_avaliacao_unidade ");
		sqlIndicadores.append(" INNER JOIN texto_atividade_unidade TEXTO ON ATIVIDADE.id_atividade_unidade = TEXTO.id_atividade_unidade AND ATIVIDADE.id_texto_atividade_unidade = TEXTO.id_texto_atividade_unidade ");
		sqlIndicadores.append(" WHERE AVALIACAO.nu_unidade =?  AND AVALIACAO.tx_periodo_avaliacao LIKE ? AND AVALIACAO.ano_avaliacao =? ");

		return sqlIndicadores.toString();
	}

	public String sqlGrafico() {

		StringBuilder sqlGrafico = new StringBuilder();

		sqlGrafico.append(" select ID_ESCORE, ");
		sqlGrafico.append(" NO_PADRAO_DESEMPENHO ");
		sqlGrafico.append(" from AVALIACAO_EMPDO_PADRAO_DSMHO L, ");
		sqlGrafico.append(" PADRAO_DESEMPENHO M ");
		sqlGrafico.append(" where L.ID_PADRAO_DESEMPENHO = M.ID_PADRAO_DESEMPENHO ");
		sqlGrafico.append(" AND ID_AVALIACAO_EMPREGADO  = ? ");

		return sqlGrafico.toString();
	}

	public String sqlPerpectivaAgencia() {

		StringBuilder sqlPerspectivaAgencia = new StringBuilder();

		sqlPerspectivaAgencia.append("SELECT * FROM sad.PERSPECTIVA_AGENCIA PA WHERE PA.CD_AGENCIA = 1 AND PA.TX_PERIODO = ?");

		return sqlPerspectivaAgencia.toString();
	}

}

