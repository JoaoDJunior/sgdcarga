package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.RuntimeErrorException;

import br.com.bancoamazonia.sgdpro.application.controller.ConsultarAvaliacaoController;
import br.com.bancoamazonia.sgdpro.application.controller.ResultadoIndicadores;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoBean;
import br.com.bancoamazonia.sgdpro.modelo.CalculoAvaliacao;
import br.com.bancoamazonia.sgdpro.modelo.DadosBean;
import br.com.bancoamazonia.sgdpro.modelo.EmpregadoBean;
import br.com.bancoamazonia.sgdpro.modelo.GraficoBean;
import br.com.bancoamazonia.sgdpro.modelo.IndicadoresBean;
import br.com.bancoamazonia.sgdpro.modelo.ParametrosBean;
import br.com.bancoamazonia.sgdpro.modelo.PerspectivaAgencia;

public class RelatorioDAO implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	static {
        try {

            Class.forName( "oracle.jdbc.OracleDriver" );

        } catch ( ClassNotFoundException exc ) {

            exc.printStackTrace();

        }
    }

    public static Connection getConnection(
            String url,
            String usuario,
            String senha ) throws SQLException {

        return DriverManager.getConnection( url, usuario, senha );

    }

    public static Connection getSGDConnection() throws SQLException {

        return getConnection(
                "jdbc:oracle:thin:sad@//oranexp.bancoamazonia.sa:1521/nexp.basa.com",
                "sad",
                "sad_prd" );

    }

    public List<EmpregadoBean> EmpregadoBeansDAO(String matricula, String anoInicio, String anoFim) throws SQLException {

    	EmpregadoBean empregadoBean = null;
    	List<EmpregadoBean> empregadoBeans = new ArrayList<EmpregadoBean>();
    	List<DadosBean> dadosBeans = new ArrayList<DadosBean>();

  //  	int quantidade = quantidadeAvaliacao(matricula);
    	dadosBeans = DadosBeansDAO(matricula, anoInicio, anoFim);

    	for(int i = 0; i < dadosBeans.size(); i++) {
    			empregadoBean = new EmpregadoBean();

    			DadosBean dadosBean = new DadosBean();

    			List<DadosBean> dado = new ArrayList<DadosBean>();

    			dadosBean = dadosBeans.get(i);

    			dado.add(dadosBean);

	    		empregadoBean.setDadosBean(dado); // aqui pode receber direto a linha 78

	    		String semestre = dadosBean.getTX_PERIODO_AVALIACAO();
	    		String ano = dadosBean.getANO_AVALIACAO().toString();
	    		String codUnidade = dadosBean.getCODIGO_UNIDADE();
	    		String periodo = null;
	    		if(semestre.equals("2º semestre")) {
	    			periodo = ano+"12";
	    		} else {
	    			periodo = ano+"06";
	    		}
	    		BigDecimal idAvaliacao = dadosBean.getID_AVALIACAO_EMPREGADO();

	    		List<AvaliacaoBean> avaliacaoBeans = new ArrayList<AvaliacaoBean>();
	    		avaliacaoBeans = AvaliacaoBeansDAO( matricula,semestre, ano );
	    		empregadoBean.setAvaliacaoBean(avaliacaoBeans);

	    		List<IndicadoresBean> indicadoresBeans = new ArrayList<IndicadoresBean>();
	    		indicadoresBeans = IndicadoresBeansDAO(codUnidade, semestre, ano);
	    		empregadoBean.setIndicadoresBean(indicadoresBeans);

	    		List<GraficoBean> graficoBeans = new ArrayList<GraficoBean>();
	    		graficoBeans = GraficoBeansDAO(idAvaliacao);
	    		empregadoBean.setGraficoBean(graficoBeans);

	    		String nome = dadosBean.getMAT() +"_"+ dadosBean.getTX_PERIODO_AVALIACAO() +"_"+ dadosBean.getANO_AVALIACAO();
	    		empregadoBean.setNomedoArquivo(nome);

	    		ResultadoIndicadores resultadoIndicadores = new ResultadoIndicadores();

	    		List<ResultadoIndicadores> resultadoIndicadoresBeans = new ArrayList<ResultadoIndicadores>();
/*
	    		resultadoIndicadores.setNotaEmpregado((avaliacaoBeans.get(9).getTotal().divide(new BigDecimal(10))).doubleValue());

	    		resultadoIndicadores.setNotaFinalUnidade(CalculoAvaliacao.CalculoNotaUnidade(indicadoresBeans, buscarNotadaMatriz(periodo)));

	    		resultadoIndicadores.setNotaFinalEmpregado(CalculoAvaliacao.CalculoNotaFinalEmpregado(resultadoIndicadores.getNotaEmpregado(), resultadoIndicadores.getNotaFinalUnidade()));

	    		resultadoIndicadoresBeans.add(resultadoIndicadores);

	    		empregadoBean.setResultadoIndicadores(resultadoIndicadoresBeans);
*/
	    		empregadoBeans.add(empregadoBean);


    	}

    	return empregadoBeans;

    }

    private static int quantidadeAvaliacao(String matricula) throws SQLException {

    	int quantidade = 0;
    	String query = "SELECT COUNT(*) FROM SAD.AVALIACAO_EMPREGADO WHERE NU_MATRICULA_EMPREGADO = ? GROUP BY NU_MATRICULA_EMPREGADO;";
    	PreparedStatement stmt = null;
    	ResultSet rs = null;

    	try {
			stmt = getSGDConnection().prepareStatement(query);
			stmt.setString(1, matricula);
			rs = stmt.executeQuery();

			while(rs.next()) {
				quantidade = Integer.parseInt(rs.getString("COUNT(*)"));
			}

			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}

    	return quantidade;
    }

    private static List<DadosBean> DadosBeansDAO(String matricula, String anoInicio, String anoFim) throws SQLException {

    	List<DadosBean> dadosBeans = new ArrayList<DadosBean>(); //-- Antiga implementa��o a qual o m�todo retornava uma lista pronta do tipo dadosbean
    	SqlConsultas sql = new SqlConsultas();
    	String queryDados = sql.SqlDados();
    	DadosBean dadosBean = null;

    	PreparedStatement stmt = null;
	    ResultSet rs = null;
		try {
		stmt = getSGDConnection().prepareStatement(queryDados);
		stmt.setString(1, matricula);
		stmt.setString(2, anoInicio);
		stmt.setString(3, anoFim);
		stmt.setString(4, matricula);
	    rs = stmt.executeQuery();

	    while(rs.next()) {

	    	dadosBean = new DadosBean();

	    	dadosBean.setMAT(rs.getString("MAT"));
	    	dadosBean.setNU_MATRICULA_EMPREGADO(rs.getBigDecimal("NU_MATRICULA_EMPREGADO"));
	    	dadosBean.setNOME(rs.getString("NOME"));
	    	dadosBean.setMAT_AVALIADOR(rs.getString("MAT_AVALIADOR"));
	    	dadosBean.setNOME_AVALIADOR(rs.getString("NOME_AVALIADOR"));
	    	dadosBean.setLOTACAO(rs.getString("LOTACAO"));
	    	dadosBean.setCARGO_EMPREGADO(rs.getString("CARGO_EMPREGADO"));
	    	dadosBean.setCODIGO_UNIDADE(rs.getString("CODIGO_UNIDADE"));
	    	dadosBean.setNOME_UNIDADE(rs.getString("NOME_UNIDADE"));
	    	dadosBean.setID_AVALIACAO_EMPREGADO(rs.getBigDecimal("iD_AVALIACAO_EMPREGADO"));
	    	dadosBean.setDT_AVALIACAO(rs.getTimestamp("DT_AVALIACAO"));
	    	dadosBean.setANO_AVALIACAO(rs.getBigDecimal("ANO_AVALIACAO"));
	    	dadosBean.setTX_PERIODO_AVALIACAO(rs.getString("TX_PERIODO_AVALIACAO"));
	    	dadosBean.setTX_ATIVIDADE(rs.getString("TX_ATIVIDADE"));

	    	dadosBeans.add(dadosBean);


	    }
	    rs.close();
	    stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}

		return dadosBeans;

    }

    private static List<AvaliacaoBean> AvaliacaoBeansDAO(String matricula, String periodo, String ano) throws SQLException {

    	List<AvaliacaoBean> avaliacaoBeans = new ArrayList<AvaliacaoBean>(); //-- Antiga implementa��o a qual o m�todo retornava uma lista pronta do tipo AvaliacaoBean

    	SqlConsultas sql = new SqlConsultas();
    	String queryAvaliacao = sql.SqlAvaliacao();
    	AvaliacaoBean avaliacaoBean = null;
    	CalculoAvaliacao calculoAvaliacao = new CalculoAvaliacao();

    	PreparedStatement stmt = null;
	    ResultSet rs = null;
		try {
		stmt = getSGDConnection().prepareStatement(queryAvaliacao);
		stmt.setString(1,matricula);
		stmt.setString(2, periodo);
		stmt.setString(3, ano);
	    rs = stmt.executeQuery();


	    while(rs.next()) {

	    	avaliacaoBean = new AvaliacaoBean();

	    	avaliacaoBean.setNU_MATRICULA_EMPREGADO(rs.getBigDecimal("NU_MATRICULA_EMPREGADO"));
	    	avaliacaoBean.setID_AVALIACAO_EMPREGADO(rs.getBigDecimal("ID_AVALIACAO_EMPREGADO"));
	    	avaliacaoBean.setID_ESCORE(rs.getBigDecimal("ID_ESCORE"));
	    	avaliacaoBean.setNO_PADRAO_DESEMPENHO(rs.getString("NO_PADRAO_DESEMPENHO"));
	    	avaliacaoBean.setDS_PADRAO_DESEMPENHO(rs.getString("DS_PADRAO_DESEMPENHO"));
	    	avaliacaoBean.setTX_JUSTIFICATIVA_GESTOR(rs.getString("TX_JUSTIFICATIVA_GESTOR"));
	    	avaliacaoBean.setTX_ACAO_DESENVOLVIMENTO(rs.getString("TX_ACAO_DESENVOLVIMENTO"));


	    	avaliacaoBeans.add(avaliacaoBean);


	    }
	    List<BigDecimal> listaPontuacao = calculoAvaliacao.CalculoEscore(avaliacaoBeans);
	    avaliacaoBeans = calculoAvaliacao.CalculoNotas(listaPontuacao, avaliacaoBeans);
	    rs.close();
	    stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}
		return avaliacaoBeans;
    }

    private static List<IndicadoresBean> IndicadoresBeansDAO(String codUnidade, String periodo, String ano) throws SQLException {

    	List<IndicadoresBean> indicadoresBeans = new ArrayList<IndicadoresBean>(); //-- Antiga implementa��o a qual o m�todo retornava uma lista pronta do tipo indicadoresbean

    	SqlConsultas sql = new SqlConsultas();
    	String queryIndicadores = sql.SqlIndicadores();
    	IndicadoresBean indicadoresBean = null;

    	PreparedStatement stmt = null;
	    ResultSet rs = null;
		try {
		stmt = getSGDConnection().prepareStatement(queryIndicadores);
		stmt.setString(1, codUnidade);
		stmt.setString(2, periodo);
		stmt.setString(3, ano);
	    rs = stmt.executeQuery();


	    while(rs.next()) {

	    	indicadoresBean = new IndicadoresBean();

	    	indicadoresBean.setNU_UNIDADE(rs.getBigDecimal("NU_UNIDADE"));
	    	indicadoresBean.setTX_PERIODO_AVALIACAO(rs.getString("TX_PERIODO_AVALIACAO"));
	    	indicadoresBean.setANO_AVALIACAO(rs.getBigDecimal("ANO_AVALIACAO"));
	    	indicadoresBean.setTI_ATIVIDADE(rs.getString("TI_ATIVIDADE"));
	    	indicadoresBean.setTX_ATIVIDADE(rs.getString("TX_ATIVIDADE"));
	    	indicadoresBean.setPERC_DESEMPENHO_REALIZADO(rs.getBigDecimal("PERC_DESEMPENHO_REALIZADO"));
	    	indicadoresBean.setQT_DESEMPENHO_PREVISTO(rs.getBigDecimal("QT_DESEMPENHO_PREVISTO"));
	    	indicadoresBean.setQT_DESEMPENHO_REALIZADO(rs.getBigDecimal("QT_DESEMPENHO_REALIZADO"));
	    	indicadoresBean.setDESVIO(rs.getBigDecimal("DESVIO"));
	    	indicadoresBean.setNOTA(rs.getBigDecimal("NOTA"));
	    	indicadoresBean.setPESO(rs.getBigDecimal("PESO"));

	    	indicadoresBeans.add(indicadoresBean);

	    }



	    rs.close();
	    stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}
		return indicadoresBeans;
    }

    private static List<GraficoBean> GraficoBeansDAO(BigDecimal idAvaliacao) throws SQLException {

    	List<GraficoBean> graficoBeans = new ArrayList<GraficoBean>(); //-- Antiga implementa��o a qual o m�todo retornava uma lista pronta do tipo graficobean

    	SqlConsultas sql = new SqlConsultas();
    	String queryGrafico = sql.sqlGrafico();
    	GraficoBean graficoBean = null;

    	PreparedStatement stmt = null;
	    ResultSet rs = null;

		try {

			stmt = getSGDConnection().prepareStatement(queryGrafico);
			stmt.setBigDecimal(1, idAvaliacao);
		    rs = stmt.executeQuery();

		    while(rs.next()) {

		    	graficoBean = new GraficoBean();

		    	graficoBean.setID_ESCORE(rs.getBigDecimal("ID_ESCORE"));
		    	graficoBean.setNO_PADRAO_DESEMPENHO(rs.getString("NO_PADRAO_DESEMPENHO"));

		    	graficoBeans.add(graficoBean);

		    }
		    rs.close();
		    stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}
		return graficoBeans;
    }

    private static PerspectivaAgencia buscarNotadaMatriz(String periodo) throws SQLException {

    	SqlConsultas sql = new SqlConsultas();

    	PerspectivaAgencia perspectivaAgencia = null;

    	PreparedStatement stmt = null;
    	ResultSet rs = null;

    	try {
			stmt = getSGDConnection().prepareStatement(sql.sqlPerpectivaAgencia());
			stmt.setString(1, periodo);
			rs = stmt.executeQuery();

			while(rs.next()) {

				perspectivaAgencia = new PerspectivaAgencia();

				perspectivaAgencia.setTxperiodo(rs.getString(1));
				perspectivaAgencia.setCodAgencia(rs.getInt(2));
				perspectivaAgencia.setDesempenhoGlobal(rs.getDouble(3));
				perspectivaAgencia.setPeso(rs.getDouble(7));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			getSGDConnection().close();
		}
    	return perspectivaAgencia;

    }
}
