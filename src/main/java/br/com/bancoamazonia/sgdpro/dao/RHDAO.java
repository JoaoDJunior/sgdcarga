package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoRH;
import br.com.bancoamazonia.sgdpro.modelo.dto.HistoricoLotacaoRHDTO;
import br.com.bancoamazonia.sgdpro.modelo.dto.SGDEmpregadoDTO;

public class RHDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PreparedStatement query;
	private Connection conexao;
	private ResultSet resultado;

	

	
	public List<HistoricoLotacaoRHDTO> buscarHistoricoLotacaoPorMatricula(String matricula)
	{
		
		try {
			conexao = ConexaoRH.getConexao();
			
			List<HistoricoLotacaoRHDTO> historicoLotacaoRHDTOs = new ArrayList<HistoricoLotacaoRHDTO>();
			HistoricoLotacaoRHDTO historicoLotacaoRHDTO = null;
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT  ");
			sql.append(" 	LTRIM(RTRIM(B.EMPLOYEE_NUMBER)) MATRICULA , ");
			sql.append(" 	E.ATTRIBUTE20 CODIGO_UNIDADE, ");
			sql.append(" 	E.NAME LOTACAO, ");
			sql.append(" 	TO_CHAR(F1.DATE_FROM,'DD/MM/YYYY') INICIO, ");
			sql.append(" 	TO_CHAR(F1.DATE_TO,'DD/MM/YYYY') FIM ");
			sql.append(" FROM PER_PERSON_ANALYSES F1,  ");
			sql.append(" 	PER_ANALYSIS_CRITERIA G1, ");
			sql.append(" 	APPS.PER_ALL_PEOPLE_F          B, ");
			sql.append(" 	APPS.HR_ALL_ORGANIZATION_UNITS E, ");
			sql.append(" 	APPS.FND_LOOKUP_VALUES D ");
			sql.append(" WHERE F1.ANALYSIS_CRITERIA_ID = G1.ANALYSIS_CRITERIA_ID ");
			sql.append(" 	AND  F1.ID_FLEX_NUM=50334  ");
			sql.append(" 	AND B.PERSON_ID = F1.PERSON_ID ");
			sql.append(" 	AND E.ORGANIZATION_ID= G1.SEGMENT2 ");
			sql.append(" 	AND D.LOOKUP_TYPE = 'EMP_ASSIGN_REASON' ");
			sql.append(" 	AND D.LANGUAGE = 'PTB' ");
			sql.append(" 	AND D.LOOKUP_CODE = G1.SEGMENT1 ");
			sql.append(" 	AND B.EFFECTIVE_START_DATE IN (SELECT MAX(EFFECTIVE_START_DATE)  ");
			sql.append(" 		FROM APPS.PER_ALL_PEOPLE_F  B1 WHERE B1.PERSON_ID=B.PERSON_ID ) ");
			sql.append(" 	AND RTRIM(B.EMPLOYEE_NUMBER) = ? ");
			sql.append(" ORDER BY F1.DATE_FROM DESC ");


			query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);
						
			resultado = query.executeQuery();
			while (resultado.next()) {
				historicoLotacaoRHDTO = new HistoricoLotacaoRHDTO();
				historicoLotacaoRHDTO.setMatricula(resultado.getString("MATRICULA"));
				historicoLotacaoRHDTO.setCodigoUnidade(resultado.getString("CODIGO_UNIDADE"));
				historicoLotacaoRHDTO.setLotacao(resultado.getString("LOTACAO"));
				historicoLotacaoRHDTO.setDataInicioLotacao(resultado.getString("INICIO"));
				historicoLotacaoRHDTO.setDataFimLotacao(resultado.getString("FIM"));
				historicoLotacaoRHDTOs.add(historicoLotacaoRHDTO);
			}
			conexao.close();
			return historicoLotacaoRHDTOs;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	
	public SGDEmpregadoDTO buscarMatriculaEmpregadoPorNome(String nome)
	{
		
		try {
			conexao = ConexaoRH.getConexao();
			StringBuilder sql = new StringBuilder();
			
			sql.append(" select employee_number MATRICULA from apps.per_all_people_f b where upper(last_name) like upper(?) and rownum < 2 ");
			
			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%"+nome.trim()+"%");
			SGDEmpregadoDTO sgdEmpregadoDTO= null;
			resultado = query.executeQuery();
			while (resultado.next()) {
				sgdEmpregadoDTO = new SGDEmpregadoDTO();
				sgdEmpregadoDTO.setMatricula(resultado.getString("MATRICULA"));
			}
			conexao.close();
			return sgdEmpregadoDTO;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	
	public SGDEmpregadoDTO buscarEmpregadoAtivoEInativoPorMatricula(String matricula)
	{
		
		try {
			conexao = ConexaoRH.getConexao();
			StringBuilder sql = new StringBuilder();
			
			sql.append(" select distinct ");
			sql.append("        b.employee_number              Matricula, ");
			sql.append("        b.last_name                    Nome_Empregado, ");
			sql.append("        g.name                         Cargo, ");
			sql.append("        ai.Funcao                      Funcao, ");
			sql.append("        ai.data_inicio                 Data_Inicio_Funcao, ");
			sql.append("        ai.carater                     Carater_Gestor, ");
			sql.append("        e.name                         Unidade, ");
			sql.append("        e.attribute20                  Codigo_da_Unidade, ");
			sql.append("        b1.employee_number             Matricula_Gestor_imediato, ");
			sql.append("        b1.last_name                   Nome_Gestor_Imediato ");
			sql.append("   from apps.per_all_people_f          b, ");
			sql.append("        apps.per_all_people_f          b1, ");
			sql.append("        apps.hr_all_organization_units e, ");
			sql.append("        apps.per_all_assignments_f     f, ");
			sql.append("        apps.per_all_assignments_f     f1, ");
			sql.append("        apps.per_jobs                  g, ");
			sql.append("        apps.per_periods_of_service    h, ");
			sql.append("        PER_PERSON_TYPE_USAGES_F    USA, ");
			sql.append("        PER_PERSON_TYPES            TYP, ");
			sql.append("       (select aa.person_id, ");
			sql.append("               dd.name            funcao     , ");
			sql.append("               cc.name            carater    , ");
			sql.append("               aa.start_date      data_inicio, ");
			sql.append("               aa.end_date        data_fim ");
			sql.append("            from apps.per_roles                 aa, ");
			sql.append("                 apps.hr_all_organization_units cc, ");
			sql.append("                 apps.per_jobs                  dd ");
			sql.append("            where aa.job_id          = dd.job_id ");
			sql.append("              and aa.organization_id = cc.organization_id (+) ");
			sql.append("              and aa.end_date is null ");
			sql.append("       )ai ");
			sql.append("  where f.job_id          = g.job_id ");
			sql.append("    and e.organization_id = f.organization_id ");
			sql.append("    and b.person_id       = f.person_id ");
			sql.append("    and b.person_id       = f1.person_id ");
			sql.append("    and f1.supervisor_id  = b1.person_id(+) ");
			sql.append("    AND B.PERSON_ID       = H.PERSON_ID ");
			sql.append("    AND AI.PERSON_ID      (+)= B.PERSON_ID ");
			sql.append("    and b.employee_number = ? ");
			sql.append("    AND b.PERSON_ID           = USA.PERSON_ID ");
			sql.append("    AND USA.PERSON_TYPE_ID    = TYP.PERSON_TYPE_ID ");
			sql.append("    AND TYP.USER_PERSON_TYPE  IN ('Empregado','Diretor','Conselheiro') ");
			sql.append("    and rownum < 2 ");
			sql.append(" order by b.employee_number ");
			
			
			query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);
			SGDEmpregadoDTO sgdEmpregadoDTO = null;
			resultado = query.executeQuery();
			while (resultado.next()) {
				sgdEmpregadoDTO = new SGDEmpregadoDTO();
				sgdEmpregadoDTO.setMatricula(resultado.getString("MATRICULA"));
				sgdEmpregadoDTO.setNomeEmpregado(resultado.getString("NOME_EMPREGADO"));
				sgdEmpregadoDTO.setCargo(resultado.getString("CARGO"));
				sgdEmpregadoDTO.setFuncao(resultado.getString("FUNCAO"));
				sgdEmpregadoDTO.setDataIniciofuncao(resultado.getString("DATA_INICIO_FUNCAO"));
				sgdEmpregadoDTO.setCaraterGestor(resultado.getString("CARATER_GESTOR"));
				sgdEmpregadoDTO.setUnidade(resultado.getString("UNIDADE"));
				sgdEmpregadoDTO.setCodigoUnidade(resultado.getString("CODIGO_DA_UNIDADE"));
				sgdEmpregadoDTO.setMatriculaGestorImediato(resultado.getString("MATRICULA_GESTOR_IMEDIATO"));
				sgdEmpregadoDTO.setNomeGestorImediato(resultado.getString("NOME_GESTOR_IMEDIATO"));
				
			}
			conexao.close();
			return sgdEmpregadoDTO;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
}
