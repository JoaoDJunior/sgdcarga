package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.TextAtividadeModel;

public class TextAtividadeUnidadeDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private PreparedStatement query;
	private Connection conexao;
	private ResultSet resultado;
	
	public TextAtividadeUnidadeDAO() {
	}
	
	public List<TextAtividadeModel> consultarAtividades(String ano, String semestre, String data, String coordenacao)
	{

		StringBuilder sql = new StringBuilder();
		List<TextAtividadeModel> atividadeModels = new ArrayList<TextAtividadeModel>();

		sql.append("  select atu.id_atividade_unidade, tau.id_texto_atividade_unidade, avu.id_avaliacao_unidade, tau.tx_atividade from atividade_unidade atu ");
		sql.append("  inner join avaliacao_unidade avu on atu.nu_unidade = avu.nu_unidade ");
		sql.append("  inner join texto_atividade_unidade tau on atu.id_atividade_unidade = tau.id_atividade_unidade ");
		sql.append("  where avu.ano_avaliacao = ?");
		sql.append("  and atu.tx_periodo_avaliacao like ? ");
		sql.append("  and atu.dt_atividade >= ? ");
		sql.append("  and atu.nu_unidade = (select distinct(codigo_unidade) from mv_bam_hr_unidade_v where upper(nome_unidade) like upper(?)) ");
		try {
			TextAtividadeModel texto = null;
			  conexao = ConexaoSGD.getConexao();
			  query = conexao.prepareStatement(sql.toString());
			  query.setString(1, ano);
			  query.setString(2, semestre+"%semestre");
			  query.setString(3, data);
			  query.setString(4, "%"+coordenacao+"%");
			 
		  resultado = query.executeQuery();
		  while (resultado.next()) {
			  texto = new TextAtividadeModel();
			  texto.setIdAtividadeUnidade(resultado.getInt("ID_ATIVIDADE_UNIDADE"));
			  texto.setIdAvaliacaoUnidade(resultado.getInt("ID_TEXTO_ATIVIDADE_UNIDADE"));
			  texto.setIdTextoAtividadeUnidade(resultado.getInt("ID_AVALIACAO_UNIDADE"));
			  texto.setTxAtividade(resultado.getString("TX_ATIVIDADE"));
			  atividadeModels.add(texto);
		  }
		  return atividadeModels;
		  		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public void carregarRegistraArquivo(){
		
	}
	
	
}
