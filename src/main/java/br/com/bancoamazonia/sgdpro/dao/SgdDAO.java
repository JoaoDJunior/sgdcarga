package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoMysqlSGDPro;
import br.com.bancoamazonia.sgdpro.conexao.ConexaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.AtividadeUnidadeSGD;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.TextoAtividadeUnidadeSGD;
import br.com.bancoamazonia.sgdpro.modelo.dto.AcordoDTO;
import br.com.bancoamazonia.sgdpro.modelo.dto.EmpregadoDTO;
import br.com.bancoamazonia.sgdpro.modelo.dto.HistoricoLotacaoDTO;
import br.com.bancoamazonia.sgdpro.modelo.dto.RankAgenciaDTO;
import br.com.bancoamazonia.sgdpro.vo.AvaliacaoEmpregadoVO;

public class SgdDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(SgdDAO.class);

	private PreparedStatement query;
	private Connection conexao;
	private ResultSet resultado;



	public int totalAvaliacoes()
	{
		try {
			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			sql.append(" select max(id_avaliacao_empregado)+1 total from avaliacao_empregado ");
			LOG.info("executando sql...");
			query = conexao.prepareStatement(sql.toString());
			int total = 0;
			resultado = query.executeQuery();
			while (resultado.next())
			{
				total = resultado.getInt("total");
			}
			conexao.close();
			return total;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return 0;
	}


	public AvaliacaoSGD listarAvaliacoes(String periodo, String ano, String matricula)
	{
		try {
			AvaliacaoSGD avaliacao = new AvaliacaoSGD();
			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			sql.append(" select * from avaliacao_empregado where nu_matricula_empregado = ? and ano_avaliacao = ? and tx_periodo_avaliacao like ? ");
			query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);
			query.setString(2, ano);
			query.setString(3, periodo+"%");

			resultado = query.executeQuery();
			while (resultado.next())
			{
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("ID_AVALIACAO_EMPREGADO"));
				avaliacao.setNuMatriculaEmpregado(resultado.getString("NU_MATRICULA_EMPREGADO"));
				avaliacao.setTxPeriodoAvaliacao(resultado.getString("TX_PERIODO_AVALIACAO"));
				avaliacao.setNuMatriculaAvaliador(resultado.getString("NU_MATRICULA_AVALIADOR"));
				avaliacao.setDtAvaliacao(resultado.getString("DT_AVALIACAO"));
				avaliacao.setDtCiencia(resultado.getString("DT_CIENCIA"));
				avaliacao.setAnoAvaliacao(resultado.getString("ANO_AVALIACAO"));
				avaliacao.setCargoEmpregado(resultado.getString("CARGO_EMPREGADO"));
				avaliacao.setFuncaoEmpregado(resultado.getString("FUNCAO_EMPREGADO"));
				avaliacao.setNomeAvaliador(resultado.getString("NOME_AVALIADOR"));
				avaliacao.setCodigoUnidade(resultado.getString("CODIGO_UNIDADE"));
				avaliacao.setNomeUnidade(resultado.getString("NOME_UNIDADE"));
			}
			conexao.close();
			return avaliacao;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}

	public String buscaNomeEmpregado(String matricula) {
		try {
			String nomeEmpregado = null;
			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT e.NOME_EMPREGADO FROM SAD.BAM_HR_EMPREGADO_V e WHERE MATRICULA = ?;");
			query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);

			resultado = query.executeQuery();
			while (resultado.next())
			{
				nomeEmpregado= resultado.getString("NOME_EMPREGADO");

			}
			conexao.close();
			return nomeEmpregado;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}



	public AvaliacaoSGD buscarAvaliacaoFuncionario(String matricula, String periodo, String ano)
	{
		try {

			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			AvaliacaoSGD avaliacao = null;

			sql.append(" SELECT  ");
			sql.append(" ID_AVALIACAO_EMPREGADO,  ");
			sql.append(" NU_MATRICULA_EMPREGADO,  ");
			sql.append(" NU_MATRICULA_AVALIADOR,  ");
			sql.append(" CODIGO_UNIDADE, NOME_UNIDADE ");
			sql.append("   FROM  ");
			sql.append(" AVALIACAO_EMPREGADO  ");
			sql.append("   WHERE  ");
			sql.append(" TX_PERIODO_AVALIACAO LIKE ?  ");
			sql.append(" AND ANO_AVALIACAO = ?  ");
			sql.append(" AND NU_MATRICULA_EMPREGADO = ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, periodo + "%");
			query.setString(2, ano);
			query.setString(3, matricula);

			resultado = query.executeQuery();
			while (resultado.next()) {
				avaliacao = new AvaliacaoSGD();
				avaliacao.setIdAvaliacaoEmpregado(resultado .getString("ID_AVALIACAO_EMPREGADO"));
				avaliacao.setNuMatriculaEmpregado(resultado.getString("NU_MATRICULA_EMPREGADO"));
				avaliacao.setNuMatriculaAvaliador(resultado.getString("NU_MATRICULA_AVALIADOR"));
				avaliacao.setCodigoUnidade(resultado.getString("CODIGO_UNIDADE"));
				avaliacao.setNomeUnidade(resultado.getString("NOME_UNIDADE"));
			}
			conexao.close();
			return avaliacao;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}



/*	public AvaliacaoSGD buscarAcordoTrabalhoUnidadePorPeriodo(String unidade, String periodo, String ano)
	{
		try {

			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			AvaliacaoSGD avaliacao = null;

			sql.append(" SELECT * FROM ATIVIDADE_UNIDADE WHERE NU_UNIDADE = (SELECT CODIGO_UNIDADE FROM INTEGRA.SGD_UNIDADE WHERE UPPER(NOME_UNIDADE) LIKE UPPER(?)) ");
			sql.append(" AND ATIVIDADE_UNIDADE.TX_PERIODO_AVALIACAO LIKE ? ");
			sql.append(" and ATIVIDADE_UNIDADE.DT_ATIVIDADE like ?  ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, periodo + "%");
			query.setString(2, ano);
			query.setString(3, unidade);

			resultado = query.executeQuery();
			while (resultado.next()) {
				avaliacao = new AvaliacaoSGD();
				avaliacao.setIdAvaliacaoEmpregado(resultado .getString("ID_AVALIACAO_EMPREGADO"));
				avaliacao.setNuMatriculaEmpregado(resultado.getString("NU_MATRICULA_EMPREGADO"));
				avaliacao.setNuMatriculaAvaliador(resultado.getString("NU_MATRICULA_AVALIADOR"));
				avaliacao.setCodigoUnidade(resultado.getString("CODIGO_UNIDADE"));
				avaliacao.setNomeUnidade(resultado.getString("NOME_UNIDADE"));
			}
			conexao.close();
			return avaliacao;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}*/


	public List<RankAgenciaDTO> buscarRankAgenciaPorPeriodoNome(String nomeAgencia, String periodo)
	{
		try {
			List<RankAgenciaDTO> ranksAgenciasDTOs = new ArrayList<RankAgenciaDTO>();

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT ");
			sql.append("  distinct A.TX_PERIODO, A.CD_AGENCIA, A.QT_DESEMPENHO_GLOBAL ");
			sql.append(" FROM PERSPECTIVA_AGENCIA A ");
			sql.append("   INNER JOIN MV_BAM_HR_UNIDADE_V U ON U.CODIGO_UNIDADE = A.CD_AGENCIA ");
			sql.append(" WHERE CD_AGENCIA = (select distinct CODIGO_UNIDADE from MV_BAM_HR_UNIDADE_V where upper(NOME_UNIDADE) like upper(?) and rownum < 2)  AND TX_PERIODO = ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%" + nomeAgencia + "%" );
			query.setString(2, periodo);

			resultado = query.executeQuery();
			while (resultado.next()) {

				RankAgenciaDTO agenciaDTO = new RankAgenciaDTO();
				agenciaDTO.setTxPeriodo(resultado .getString("TX_PERIODO"));
				agenciaDTO.setCdAgencia(resultado .getString("CD_AGENCIA"));
				agenciaDTO.setQtDesempenhoGlobal(resultado .getString("QT_DESEMPENHO_GLOBAL"));

				ranksAgenciasDTOs.add(agenciaDTO);
			}
			conexao.close();
			return ranksAgenciasDTOs;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return new ArrayList<RankAgenciaDTO>();
	}


	public HistoricoLotacaoDTO buscarHistoricoLotacaoEmpregado(String matricula, String periodo, String unidade )
	{
		try {

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();


			sql.append(" SELECT * ");
			sql.append(" FROM INTEGRA.SGD_HISTORICO_LOTACAO  ");
			sql.append(" WHERE CODIGO_UNIDADE = ?  ");
			sql.append(" AND DATA_FIM_LOTACAO like ?  ");
			sql.append(" AND MATRICULA = ? AND ROWNUM < 2 ORDER BY DATA_FIM_LOTACAO DESC");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, unidade );
			query.setString(2, "%"+periodo+"%");
			query.setString(3, matricula);

			HistoricoLotacaoDTO historicoLotacao = null;
			resultado = query.executeQuery();
			while (resultado.next()) {

				historicoLotacao = new HistoricoLotacaoDTO();

				historicoLotacao.setIdHistoricoLotacao(resultado .getString("ID_HISTORICO_LOTACAO"));
				historicoLotacao.setMatricula(resultado .getString("MATRICULA"));
				historicoLotacao.setCodigoUnidade(resultado .getString("CODIGO_UNIDADE"));
				historicoLotacao.setDataInicioLotacao(resultado .getString("DATA_INICIO_LOTACAO"));
				historicoLotacao.setDataFimLotacao(resultado .getString("DATA_FIM_LOTACAO"));

			}
			conexao.close();
			return historicoLotacao;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}



	public List<RankAgenciaDTO> buscarRankAgenciaPorPeriodoCodigoAgencia(String codigoAgencia, String periodo)
	{
		try {
			conexao = ConexaoSGD.getConexao();
			List<RankAgenciaDTO> ranksAgenciasDTOs = new ArrayList<RankAgenciaDTO>();

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT DISTINCT A.TX_PERIODO, A.CD_AGENCIA, A.QT_DESEMPENHO_GLOBAL ");
			sql.append(" FROM PERSPECTIVA_AGENCIA A  WHERE A.CD_AGENCIA = ?  AND A.TX_PERIODO = ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, codigoAgencia);
			query.setString(2, periodo);

			resultado = query.executeQuery();
			while (resultado.next()) {

				RankAgenciaDTO agenciaDTO = new RankAgenciaDTO();
				agenciaDTO.setTxPeriodo(resultado .getString("TX_PERIODO"));
				agenciaDTO.setCdAgencia(resultado .getString("CD_AGENCIA"));
				agenciaDTO.setQtDesempenhoGlobal(resultado .getString("QT_DESEMPENHO_GLOBAL"));

				ranksAgenciasDTOs.add(agenciaDTO);
			}
			conexao.close();
			return ranksAgenciasDTOs;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return new ArrayList<RankAgenciaDTO>();
	}

	public List<EmpregadoDTO> buscarTodosEmpregados()
	{
		try {
			conexao = ConexaoSGD.getConexao();
			List<EmpregadoDTO> EmpregadosDTOs = new ArrayList<EmpregadoDTO>();

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT DISTINCT MATRICULA, NOME_EMPREGADO FROM INTEGRA.SGD_EMPREGADO ORDER BY MATRICULA ASC ");

			query = conexao.prepareStatement(sql.toString());
			resultado = query.executeQuery();
			while (resultado.next()) {
				EmpregadoDTO empregadoDTO = new EmpregadoDTO();
				empregadoDTO.setMatricula(resultado .getString("MATRICULA"));
				empregadoDTO.setNome(resultado .getString("NOME_EMPREGADO"));

				EmpregadosDTOs.add(empregadoDTO);
			}
			conexao.close();
			return EmpregadosDTOs;

		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public List<EmpregadoDTO> buscarEmpregadoPorMatricula(String matricula)
	{
		try {
			conexao = ConexaoSGD.getConexao();
			List<EmpregadoDTO> EmpregadosDTOs = new ArrayList<EmpregadoDTO>();

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT MATRICULA, NOME_EMPREGADO FROM INTEGRA.SGD_EMPREGADO WHERE MATRICULA = ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);

			resultado = query.executeQuery();
			while (resultado.next()) {
				EmpregadoDTO empregadoDTO = new EmpregadoDTO();
				empregadoDTO.setMatricula(resultado .getString("MATRICULA"));
				empregadoDTO.setNome(resultado .getString("NOME_EMPREGADO"));

				EmpregadosDTOs.add(empregadoDTO);
			}
			conexao.close();
			return EmpregadosDTOs;

		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public String buscarCodigoAgencia(String nomeAgencia)
	{
		try {

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT distinct CODIGO_UNIDADE FROM MV_BAM_HR_UNIDADE_V WHERE UPPER(NOME_UNIDADE) LIKE UPPER(?) ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%" + nomeAgencia + "%" );
			resultado = query.executeQuery();
			String codigoAgencia = null;
			while (resultado.next()) {
					codigoAgencia =  resultado.getString("CODIGO_UNIDADE");
			}
			conexao.close();
			return codigoAgencia;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public AtividadeUnidadeSGD buscarAcordoTrabalhoPorPeriodoUnidade(String unidade, String semestre, String ano)
	{
		try {

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM ATIVIDADE_UNIDADE WHERE NU_UNIDADE IN (SELECT CODIGO_UNIDADE FROM INTEGRA.SGD_UNIDADE WHERE UPPER(NOME_UNIDADE) ");
			sql.append(" LIKE UPPER(?)) AND ATIVIDADE_UNIDADE.TX_PERIODO_AVALIACAO LIKE ? and ATIVIDADE_UNIDADE.DT_ATIVIDADE like ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%" + unidade + "%" );
			query.setString(2, semestre + "%" );
			query.setString(3, "%" + ano + "%" );

			AtividadeUnidadeSGD atividadeUnidadeSGD = null;
			resultado = query.executeQuery();
			while (resultado.next()) {
				atividadeUnidadeSGD = new AtividadeUnidadeSGD(
						resultado.getInt("ID_ATIVIDADE_UNIDADE"),
						resultado.getString("NU_UNIDADE"),
						resultado.getString("TX_PERIODO_AVALIACAO"),
						resultado.getString("DT_ATIVIDADE"));
			}
			conexao.close();
			return atividadeUnidadeSGD;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public AcordoDTO buscarAcordoTrabalhoPeriodoEmpregado(String matricula, String ano)
	{
		try {

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" select * from ATIVIDADE_EMPREGADO where ANO_AVALIACAO = ? and NU_MATRICULA_EMPREGADO = ? ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, ano );
			query.setString(2, matricula);

			AcordoDTO acordoDTO = null;
			resultado = query.executeQuery();
			while (resultado.next()) {
				acordoDTO = new AcordoDTO(resultado.getString("ID_ATIVIDADE_EMPREGADO"),
						resultado.getString("NU_MATRICULA_EMPREGADO"),
						resultado.getString("TX_PERIODO_AVALIACAO"),
						resultado.getString("TX_ATIVIDADE"),
						resultado.getString("DT_ATIVIDADE"),
						resultado.getString("ANO_AVALIACAO"));
			}
			conexao.close();
			return acordoDTO;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public TextoAtividadeUnidadeSGD buscarTextoAcordoPorPeriodoUnidade(String unidade, String semestre, String ano)
	{
		try {

			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM TEXTO_ATIVIDADE_UNIDADE WHERE ID_ATIVIDADE_UNIDADE IN (SELECT ID_ATIVIDADE_UNIDADE FROM ATIVIDADE_UNIDADE WHERE NU_UNIDADE IN  ");
			sql.append(" (SELECT CODIGO_UNIDADE FROM INTEGRA.SGD_UNIDADE WHERE UPPER(NOME_UNIDADE) LIKE UPPER(?)) AND ATIVIDADE_UNIDADE.TX_PERIODO_AVALIACAO LIKE ? AND ATIVIDADE_UNIDADE.DT_ATIVIDADE LIKE ?) ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%" + unidade + "%" );
			query.setString(2, semestre + "%" );
			query.setString(3, "%" + ano + "%" );

			TextoAtividadeUnidadeSGD textoAtividadeUnidadeSGD =  null;
			resultado = query.executeQuery();
			while (resultado.next()) {

				  textoAtividadeUnidadeSGD = new TextoAtividadeUnidadeSGD(
						resultado.getInt("ID_TEXTO_ATIVIDADE_UNIDADE"),
						resultado.getString("TI_ATIVIDADE"),
						resultado.getString("TX_ATIVIDADE"),
						resultado.getInt("ID_ATIVIDADE_UNIDADE"));
			}
			conexao.close();
			return textoAtividadeUnidadeSGD;

		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}

	public String gerarSqlRemoverAvalicaoFuncionario(String matricula)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" DELETE avaliacao_empdo_padrao_dsmho WHERE id_avaliacao_empregado in (?)  ");
		return sql.toString();

	}


	public AvaliacaoEmpregadoVO buscarAvaliacaoExEmpregadoPorNome(String nomeConsultar)
	{
			try {
			conexao = ConexaoSGD.getConexao();

			StringBuilder sql = new StringBuilder();
			sql.append(" select nome_empregado, matricula from "
					+ " INTEGRA.SGD_EMPREGADO where upper(nome_empregado) like upper(?) and rownum < 2 ");

			query = conexao.prepareStatement(sql.toString());
			query.setString(1, "%" + nomeConsultar + "%" );

			AvaliacaoEmpregadoVO avaliacaoEmpregadoVO =  null;
			resultado = query.executeQuery();
			while (resultado.next()) avaliacaoEmpregadoVO = new AvaliacaoEmpregadoVO( resultado.getString("MATRICULA"), resultado.getString("NOME_EMPREGADO"));
			conexao.close();
			return avaliacaoEmpregadoVO;

		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
}
