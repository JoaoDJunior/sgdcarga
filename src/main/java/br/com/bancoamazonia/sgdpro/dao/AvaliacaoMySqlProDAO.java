package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoMysqlSGDPro;
import br.com.bancoamazonia.sgdpro.modelo.Avaliacao;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;
import br.com.bancoamazonia.sgdpro.util.FileUtil;

public class AvaliacaoMySqlProDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(AvaliacaoMySqlProDAO.class);
	
	private PreparedStatement query;
	private Connection conexao;
	private ResultSet resultado;
	
	
	public String carregarAvaliacoes(String caminho){
		try {
			
			List<String> lerArquivo = FileUtil.lerArquivo(caminho);
			int numero = 0;
			if(lerArquivo.size() > 0){
				for (String linha : lerArquivo) 
				{
					String[] split = linha.split(";");
					if(split.length > 0)
					{
						if(FileUtil.validaCampoExportacao(split[0]) != null)
						{
							++numero;
					
						}
					}
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public AvaliacaoEmpregado buscarAvaliacao(String periodo, String ano, String matricula)
	{
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();
			AvaliacaoEmpregado avaliacao = null;
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" select * from avaliacao_empregado where matriculaEmpregado = ? and ano = ? and periodo = ? ");
			
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			
			query.setString(1, matricula);
			query.setString(2, ano);
			query.setString(3, periodo);
			
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				avaliacao = new AvaliacaoEmpregado();
				
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("idAvaliacaoEmpregado"));
				avaliacao.setAno(resultado.getString("ano"));
				avaliacao.setPeriodo(resultado.getString("periodo"));
				avaliacao.setEmpregado(new Empregado(resultado.getString("matriculaEmpregado")));
				avaliacao.setGestor(new Empregado(resultado.getString("matriculaGestor")));
				
			}
			conexao.close();
			return avaliacao;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	public List<AvaliacaoEmpregado> listarAvaliacoes()
	{
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();
			AvaliacaoEmpregado avaliacao = null;
			List<AvaliacaoEmpregado> avaliacoesEmpregados = new ArrayList<AvaliacaoEmpregado>();
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT * FROM AVALIACAO_EMPREGADO ORDER BY id ASC ");
			
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				avaliacao = new AvaliacaoEmpregado();
				
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("idAvaliacaoEmpregado"));
				avaliacao.setAno(resultado.getString("ano"));
				avaliacao.setPeriodo(resultado.getString("periodo"));
				avaliacao.setEmpregado(new Empregado(resultado.getString("matriculaEmpregado")));
				avaliacao.setGestor(new Empregado(resultado.getString("matriculaGestor")));
				avaliacoesEmpregados.add(avaliacao);
			}
			conexao.close();
			return avaliacoesEmpregados;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	
	public void inserirAvaliacaoEmpregado(AvaliacaoEmpregado avaliacaoEmpregado)
	{
		
		StringBuilder sql = new StringBuilder();
		sql.append(" INSERT INTO `avaliacao_empregado` (`Id`, `matriculaEmpregado`, `matriculaGestor`, `periodo`, `ano`, `idAvaliacaoEmpregado`) VALUES (NULL, ?, ?, ?, ?, ?) ");
		
		try {
				
			  conexao = ConexaoMysqlSGDPro.getConexao();
			  query = conexao.prepareStatement(sql.toString());
			  
			  query.setString(1, FileUtil.verificarCampoNulo(avaliacaoEmpregado.getEmpregado().getMatricula()));
			  query.setString(2, FileUtil.verificarCampoNulo(avaliacaoEmpregado.getGestor().getMatricula()));
			  query.setString(3, FileUtil.verificarCampoNulo(avaliacaoEmpregado.getPeriodo()));
			  query.setString(4, FileUtil.verificarCampoNulo(avaliacaoEmpregado.getAno()));
			  query.setString(5, FileUtil.verificarCampoNulo(avaliacaoEmpregado.getIdAvaliacaoEmpregado()));
			  
			  
			  query.execute();
			  query.close();
		  		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void inserirAvaliacoesEmpregado(Avaliacao avaliacao)
	{
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" INSERT INTO `avaliacao` (`Id`, `id_avaliacao_empregado`, `texto`, `valor_escore`, `justificativa`) VALUES (NULL, ?, ?, ?, ?); ");
		
		try {
			
			conexao = ConexaoMysqlSGDPro.getConexao();
			query = conexao.prepareStatement(sql.toString());
			LOG.info(avaliacao.getIdAvaliacaoEmpregado() +" - "+ avaliacao.getNome());
			
			query.setString(1, FileUtil.verificarCampoNulo(avaliacao.getIdAvaliacaoEmpregado()));
			query.setString(2, FileUtil.verificarCampoNulo(avaliacao.getNome()));
			query.setString(3, FileUtil.verificarCampoNulo(avaliacao.getNota()));
			query.setString(4, FileUtil.verificarCampoNulo(avaliacao.getJustificativa()));
			
			
			query.execute();
			query.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public List<String> exportarAvaliacaoEmpregado(String periodo, String ano)
	{
		try {
			AvaliacaoEmpregado avaliacao = null;
			conexao = ConexaoMysqlSGDPro.getConexao();
			List<AvaliacaoEmpregado> avaliacoes = new ArrayList<AvaliacaoEmpregado>();
			StringBuilder sql = new StringBuilder();
			sql.append(" select * from avaliacao_empregado where periodo = ? and ano = ?; ");
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			query.setString(1, periodo);
			query.setString(2, ano);
			
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				avaliacao = new AvaliacaoEmpregado();
				avaliacao.setAno(resultado.getString("ano"));
				avaliacao.setPeriodo(resultado.getString("periodo"));
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("idAvaliacaoEmpregado"));
				avaliacao.setEmpregado(new Empregado(resultado.getString("matriculaEmpregado")));
				avaliacao.setGestor(new Empregado(resultado.getString("matriculaGestor")));
				
				avaliacoes.add(avaliacao);
			}
			conexao.close();
			
			EmpregadoDAO empregadoDAO = new EmpregadoDAO();
			List<String> exportacoes = new ArrayList<String>();
			
			for (AvaliacaoEmpregado avaliacaoEmpregado : avaliacoes) 
			{
				
				StringBuilder exporta = new StringBuilder();
				Empregado empregado = empregadoDAO.consultaEmpregado(avaliacaoEmpregado.getEmpregado().getMatricula());
				Empregado gestor = empregadoDAO.consultaEmpregado(avaliacaoEmpregado.getGestor().getMatricula());
				
				exporta.append(" Insert into avaliacao_empregado (ID_AVALIACAO_EMPREGADO,NU_MATRICULA_EMPREGADO,TX_PERIODO_AVALIACAO,NU_MATRICULA_AVALIADOR, ");
				exporta.append(" DT_AVALIACAO,DT_CIENCIA,ANO_AVALIACAO,CARGO_EMPREGADO, ");
				exporta.append(" FUNCAO_EMPREGADO,NOME_AVALIADOR,CODIGO_UNIDADE,NOME_UNIDADE) \n values  (");
				exporta.append(" SQ_AVALIACAO_EMPREGADO.nextval, '"
						+ avaliacaoEmpregado.getEmpregado().getMatricula() + "','"
						+ avaliacao.getPeriodo() + "º semestre','"
						+ avaliacaoEmpregado.getGestor().getMatricula()+ "',to_date('" + FileUtil.getDataPorPeriodo(periodo, ano) + "','DD/MM/RR'), NULL,'"
						+ avaliacao.getAno() + "','"
						+ empregado.getCargo() + "','"
						+ empregado.getFuncao() + "','"
						+ gestor.getNome() + "','"
						+ empregado.getCodigoUnidade() + "','"
						+ empregado.getUnidade() + "'); ");
				exporta.append(" \n\n ");
				
				List<String> exportaAvaliacoesEmpregado = exportaAvaliacoesEmpregado(avaliacaoEmpregado);
				if(exportaAvaliacoesEmpregado.size() > 0){
					for (String av : exportaAvaliacoesEmpregado) {
						exporta.append(av);
					}
				}
				
				exportacoes.add(exporta.toString());
			}
			
			
			return exportacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	public List<String> exportarAvaliacaoPorFuncionario(AvaliacaoEmpregado avaliacaoEmpregado)
	{
		try {
			
			EmpregadoDAO empregadoDAO = new EmpregadoDAO();
			List<String> exportacoes = new ArrayList<String>();
		
				StringBuilder exporta = new StringBuilder();
				Empregado empregado = empregadoDAO.consultaEmpregado( avaliacaoEmpregado.getEmpregado().getMatricula());
				Empregado gestor = empregadoDAO.consultaEmpregado(avaliacaoEmpregado.getGestor().getMatricula());
				
				exporta.append(" Insert into avaliacao_empregado (ID_AVALIACAO_EMPREGADO,NU_MATRICULA_EMPREGADO,TX_PERIODO_AVALIACAO,NU_MATRICULA_AVALIADOR, ");
				exporta.append(" DT_AVALIACAO,DT_CIENCIA,ANO_AVALIACAO,CARGO_EMPREGADO, ");
				exporta.append(" FUNCAO_EMPREGADO,NOME_AVALIADOR,CODIGO_UNIDADE,NOME_UNIDADE) \n values  (");
				exporta.append(" SQ_AVALIACAO_EMPREGADO.nextval, '"
						+ avaliacaoEmpregado.getEmpregado().getMatricula() + "','"
						+ avaliacaoEmpregado.getPeriodo() + "º semestre','"
						+ avaliacaoEmpregado.getGestor().getMatricula()+ "',to_date('" + FileUtil.getDataPorPeriodo(avaliacaoEmpregado.getPeriodo(),avaliacaoEmpregado.getAno()) + "','DD/MM/RR'), NULL,'"
						+ avaliacaoEmpregado.getAno() + "','"
						+ empregado.getCargo() + "','"
						+ empregado.getFuncao() + "','"
						+ gestor.getNome() + "','"
						+ empregado.getCodigoUnidade() + "','"
						+ empregado.getUnidade() + "'); ");
				exporta.append(" \n\n ");
				
				List<String> exportaAvaliacoesEmpregado = exportaAvaliacoesEmpregado(avaliacaoEmpregado);
				if(exportaAvaliacoesEmpregado.size() > 0){
					for (String av : exportaAvaliacoesEmpregado) {
						exporta.append(av);
					}
				}
				
				exportacoes.add(exporta.toString());
			
			
			return exportacoes;
		} catch (Exception e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	public List<String> exportaAvaliacoesEmpregado(AvaliacaoEmpregado avaliacaoEmpregado)
	{
		try
		{
			
			List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
			conexao = ConexaoMysqlSGDPro.getConexao();
			StringBuilder sql = new StringBuilder();
			sql.append(" select * from avaliacao where id_avaliacao_empregado = ? ");
			
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			query.setString(1, avaliacaoEmpregado.getIdAvaliacaoEmpregado());
			
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				Avaliacao avaliacao = new Avaliacao();
				
				avaliacao.setJustificativa(resultado.getString("justificativa"));
				avaliacao.setNome(resultado.getString("texto"));
				avaliacao.setNota(resultado.getString("valor_escore"));
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("id_avaliacao_empregado"));
				
				avaliacoes.add(avaliacao);
			}
			
			conexao.close();
			
			List<String> exportacoes = new ArrayList<String>();
			
			for (Avaliacao avaliacao : avaliacoes) 
			{
		
				StringBuilder exporta = new StringBuilder();
				
				exporta.append(" Insert into avaliacao_empdo_padrao_dsmho (ID_PADRAO_DESEMPENHO,ID_AVALIACAO_EMPREGADO,ID_ESCORE,IN_STATUS,ID_AVLAO_EMPDO_PADRAO_DSMHO,DT_AVALIACAO, ");
				exporta.append(" TX_JUSTIFICATIVA_GESTOR) \n values ('"+ FileUtil.analisarDesempenho(avaliacao.getNome())+"',(select max(id_avaliacao_empregado) from avaliacao_empregado), ");
				exporta.append("  '"+avaliacao.getNota()+"','1', SQ_AVLAO_EMPDO_PADRAO_DSMHO.nextval ,to_date('"+FileUtil.getDataPorPeriodo("2", "2016")+"','DD/MM/RR'), '"+avaliacao.getJustificativa()+"'); ");
				exporta.append(" \n");
				exportacoes.add(exporta.toString());
				
			}
			
			
			return exportacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	public List<String> geraSqlAvaliacoesEmpregado(AvaliacaoEmpregado avaliacaoEmpregado)
	{
		try
		{
			
			List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
			conexao = ConexaoMysqlSGDPro.getConexao();
			StringBuilder sql = new StringBuilder();
			sql.append(" select * from avaliacao where id_avaliacao_empregado = ? ");
			
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			query.setString(1, avaliacaoEmpregado.getIdAvaliacaoEmpregado());
			
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				Avaliacao avaliacao = new Avaliacao();
				
				avaliacao.setJustificativa(resultado.getString("justificativa"));
				avaliacao.setNome(resultado.getString("texto"));
				avaliacao.setNota(resultado.getString("valor_escore"));
				avaliacao.setIdAvaliacaoEmpregado(resultado.getString("id_avaliacao_empregado"));
				
				avaliacoes.add(avaliacao);
			}
			
			conexao.close();
			
			List<String> exportacoes = new ArrayList<String>();
			
			for (Avaliacao avaliacao : avaliacoes) 
			{
				
				StringBuilder exporta = new StringBuilder();
				
				exporta.append(" Insert into avaliacao_empdo_padrao_dsmho (ID_PADRAO_DESEMPENHO,ID_AVALIACAO_EMPREGADO,ID_ESCORE,IN_STATUS,ID_AVLAO_EMPDO_PADRAO_DSMHO,DT_AVALIACAO, ");
				exporta.append(" TX_JUSTIFICATIVA_GESTOR) \n values ('"+ FileUtil.analisarDesempenho(avaliacao.getNome())+"',"+avaliacao.getIdAvaliacaoEmpregado()+", ");
				exporta.append("  '"+avaliacao.getNota()+"','1', SQ_AVLAO_EMPDO_PADRAO_DSMHO.nextval ,to_date('"+FileUtil.getDataPorPeriodo(avaliacaoEmpregado.getPeriodo(), avaliacaoEmpregado.getAno())+"','DD/MM/RR'), '"+avaliacao.getJustificativa()+"'); ");
				exporta.append(" \n");
				exportacoes.add(exporta.toString());
				
			}
			
			
			return exportacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	


}
