package br.com.bancoamazonia.sgdpro.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.Empregado;

public class EmpregadoDAO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Connection conexao;
	private ResultSet resultado;
	
	
	
	public Empregado consultaEmpregado(String matricula)
	{
		try {
			conexao = ConexaoSGD.getConexao();
			Empregado empregado = null;
			StringBuilder sql = new StringBuilder();
			sql.append(" select * from bam_hr_empregado_v where matricula =  ? ");
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			query.setString(1, matricula);
			
			resultado = query.executeQuery();
			while (resultado.next()) 
			{
				empregado = new Empregado();
				empregado.setCargo(resultado.getString("CARGO"));
				empregado.setCodigoUnidade(resultado.getString("CODIGO_DA_UNIDADE"));
				empregado.setFuncao(resultado.getString("FUNCAO"));
				empregado.setMatricula(resultado.getString("MATRICULA"));
				empregado.setNome(resultado.getString("NOME_EMPREGADO"));
				empregado.setUnidade(resultado.getString("UNIDADE"));
			}
			conexao.close();
			return empregado;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}
	
	
	


}
