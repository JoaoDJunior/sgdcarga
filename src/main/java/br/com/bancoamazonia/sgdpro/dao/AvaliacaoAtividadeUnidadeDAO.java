package br.com.bancoamazonia.sgdpro.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.conexao.ConexaoMysqlSGDPro;
import br.com.bancoamazonia.sgdpro.conexao.ConexaoSGD;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoAtividadeUnidadeModel;
import br.com.bancoamazonia.sgdpro.modelo.TvRankAgencia;
import br.com.bancoamazonia.sgdpro.modelo.dto.RankAgenciaDTO;
import br.com.bancoamazonia.sgdpro.util.FileUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AvaliacaoAtividadeUnidadeDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	//private static final Logger LOG = Logger.getLogger(AvaliacaoAtividadeUnidadeDAO.class);

	private PreparedStatement query;
	private Connection conexao;
//	private String sql;
	private ResultSet resultado;

	public AvaliacaoAtividadeUnidadeDAO() {
	}



	public List<AvaliacaoAtividadeUnidadeModel> listarTodasAvaliacoesAtividadeUnidade(String unidade)
	{
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();
			List<AvaliacaoAtividadeUnidadeModel> avaliacoes = new ArrayList<AvaliacaoAtividadeUnidadeModel>();
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM `avaliacao_atividade_unidade` ");
			if(unidade != null && !unidade.equals("")){
				sql.append(" WHERE unidade = ? ");
			}

			PreparedStatement query = conexao.prepareStatement(sql.toString());

			if(unidade != null && !unidade.equals("")){
				query.setString(1, unidade);
			}

			resultado = query.executeQuery();
				while (resultado.next()) {
				AvaliacaoAtividadeUnidadeModel avaliacao = new AvaliacaoAtividadeUnidadeModel();
				avaliacao.setId(resultado.getLong("id"));
				avaliacao.setDescricao(FileUtil.mudarSimbolosPorAcento(resultado.getString("descricao")));
				avaliacao.setUnidade(FileUtil.modificaTexto(resultado.getString("unidade").trim(), " ","") );
				avaliacao.setIdUnidade(resultado.getString("id_unidade"));
				avaliacao.setIdAtividadeUnidade(resultado.getString("id_atividade_unidade"));
				avaliacao.setIdtextoAtividadeUnidade(resultado.getString("id_text_atividade_unidade"));
				avaliacao.setIdAvaliacaoUnidade(resultado.getString("id_avaliacao_unidade"));
				avaliacao.setIdEscore(resultado.getString("id_escore"));
				avaliacao.setPercRealizado(resultado.getString("perc_realizado"));
				avaliacao.setQtPrevisto(resultado.getString("qt_previsto"));
				avaliacao.setQtRealizado(resultado.getString("qt_realizado"));
				avaliacao.setInCargaSistema(resultado.getString("in_carga_sistema"));
				avaliacao.setDesvio(resultado.getString("desvio"));
				avaliacao.setNota(resultado.getString("nota"));
				avaliacao.setPeso(resultado.getString("peso"));

				avaliacoes.add(avaliacao);
			}
			conexao.close();
			return avaliacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public List<AvaliacaoAtividadeUnidadeModel> listarTodasUnidadeAtividadeUnidade()
	{
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();
			List<AvaliacaoAtividadeUnidadeModel> avaliacoes = new ArrayList<AvaliacaoAtividadeUnidadeModel>();
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT DISTINCT unidade FROM `avaliacao_atividade_unidade` ");
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			resultado = query.executeQuery();
			while (resultado.next()) {
				AvaliacaoAtividadeUnidadeModel avaliacao = new AvaliacaoAtividadeUnidadeModel();
				avaliacao.setUnidade(FileUtil.modificaTexto(resultado.getString("unidade").trim(), " ","") );
				avaliacoes.add(avaliacao);
			}
			conexao.close();
			return avaliacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public List<AvaliacaoAtividadeUnidadeModel> getCarga()
	{
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();
			List<AvaliacaoAtividadeUnidadeModel> avaliacoes = new ArrayList<AvaliacaoAtividadeUnidadeModel>();
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM `avaliacao_atividade_unidade` ");
			PreparedStatement query = conexao.prepareStatement(sql.toString());

			resultado = query.executeQuery();
			while (resultado.next()) {

				AvaliacaoAtividadeUnidadeModel avaliacao = new AvaliacaoAtividadeUnidadeModel();
				avaliacao.setId(resultado.getLong("id"));
				avaliacao.setDescricao(resultado.getString("descricao"));
				avaliacao.setUnidade(resultado.getString("unidade"));
				avaliacao.setIdUnidade(resultado.getString("id_unidade"));
				avaliacao.setIdAtividadeUnidade(resultado.getString("id_atividade_unidade"));
				avaliacao.setIdtextoAtividadeUnidade(resultado.getString("id_text_atividade_unidade"));
				avaliacao.setIdAvaliacaoUnidade(resultado.getString("id_avaliacao_unidade"));
				avaliacao.setIdEscore(resultado.getString("id_escore"));
				avaliacao.setPercRealizado(resultado.getString("perc_realizado"));
				avaliacao.setQtPrevisto(resultado.getString("qt_previsto"));
				avaliacao.setQtRealizado(resultado.getString("qt_realizado"));
				avaliacao.setInCargaSistema(resultado.getString("in_carga_sistema"));
				avaliacao.setDesvio(resultado.getString("desvio"));
				avaliacao.setNota(resultado.getString("nota"));
				avaliacao.setPeso(resultado.getString("peso"));

				avaliacoes.add(avaliacao);
			}
			conexao.close();
			return avaliacoes;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}

	public String retornaCodigoUnidade(String unidade)
	{
		try {
			conexao = ConexaoSGD.getConexao();
			String valor = null;
			StringBuilder sql = new StringBuilder();
			sql.append(" select distinct(codigo_unidade) from mv_bam_hr_unidade_v where upper(nome_unidade) like upper(?) ");
			PreparedStatement query = conexao.prepareStatement(sql.toString());
			query.setString(1, unidade);

			resultado = query.executeQuery();
			while (resultado.next()) {
				valor = resultado.getString("CODIGO_UNIDADE");
			}
			conexao.close();
			return valor;
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}


	public List<AvaliacaoAtividadeUnidadeModel> localizarTextoPorUnidade(String texto, String idUnidade, String unidade)
	{
		try {
			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();
			List<AvaliacaoAtividadeUnidadeModel> models = new ArrayList<AvaliacaoAtividadeUnidadeModel>();

			sql.append(" select acordo.nu_unidade, acordo.id_atividade_unidade, text.id_texto_atividade_unidade, av.id_avaliacao_unidade, text.tx_atividade from atividade_unidade acordo ");
			sql.append(" inner join texto_atividade_unidade text on text.id_atividade_unidade = acordo.id_atividade_unidade ");
			sql.append(" inner join avaliacao_unidade av on av.nu_unidade = acordo.nu_unidade ");
			sql.append(" where ");
			sql.append(" acordo.nu_unidade = (select distinct(codigo_unidade) from mv_bam_hr_unidade_v where upper(nome_unidade) like upper(?)) ");
			sql.append(" and av.ano_avaliacao = 2017 ");
			sql.append(" and av.tx_periodo_avaliacao like '2%' ");
			sql.append(" and acordo.tx_periodo_avaliacao like '2%' ");
			sql.append(" and acordo.dt_atividade >= '01/07/2017' ");


			PreparedStatement query = conexao.prepareStatement(sql.toString());

			query.setString(1, unidade);


			resultado = query.executeQuery();
				while (resultado.next()) {

					AvaliacaoAtividadeUnidadeModel model = new AvaliacaoAtividadeUnidadeModel();
					model.setIdUnidade(resultado.getString("NU_UNIDADE"));
					model.setIdAtividadeUnidade(resultado.getString("ID_ATIVIDADE_UNIDADE"));
					model.setIdtextoAtividadeUnidade(resultado.getString("ID_TEXTO_ATIVIDADE_UNIDADE"));
					model.setIdAvaliacaoUnidade(resultado.getString("ID_AVALIACAO_UNIDADE"));
					model.setDescricao(resultado.getString("TX_ATIVIDADE"));

					models.add(model);
			}
				conexao.close();
				return models;

		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}

	public List<AvaliacaoAtividadeUnidadeModel> selecionarTextoAtividadeSGD(String coordenacao){
		try {
			conexao = ConexaoSGD.getConexao();
			StringBuilder sql = new StringBuilder();

			sql.append(" select atu.id_atividade_unidade, tau.id_texto_atividade_unidade, avu.id_avaliacao_unidade, tau.tx_atividade from atividade_unidade atu ");
			sql.append(" inner join avaliacao_unidade avu on atu.nu_unidade = avu.nu_unidade ");
			sql.append(" inner join texto_atividade_unidade tau on atu.id_atividade_unidade = tau.id_atividade_unidade ");
			sql.append(" where avu.ano_avaliacao = 2016 ");
			sql.append(" and atu.tx_periodo_avaliacao like '2%semestre' ");
			sql.append(" and atu.dt_atividade >= '31/12/2016' ");

			if(!coordenacao.equals("") && coordenacao != null){
				sql.append(" and atu.nu_unidade = (select distinct(codigo_unidade) from mv_bam_hr_unidade_v where upper(nome_unidade) like upper(?)) ");
			}

			PreparedStatement query = conexao.prepareStatement(sql.toString());
			if(!coordenacao.equals("") && coordenacao != null){
				query.setString(1, coordenacao);
			}

			resultado = query.executeQuery();

			while (resultado.next()) {


			}
			conexao.close();
		} catch (SQLException e) {
			System.out.println("erro" + e.getMessage());
		}
		return null;
	}

	public void salvar(AvaliacaoAtividadeUnidadeModel modelo)
	{

		StringBuilder sql = new StringBuilder();

		sql.append(" INSERT INTO `pratic_sgdpro`.`avaliacao_atividade_unidade` ( `descricao`, `id_atividade_unidade`, `id_text_atividade_unidade`, ");
		sql.append(" `id_avaliacao_unidade`, `id_escore`, `perc_realizado`, `qt_previsto`, `qt_realizado`, `in_carga_sistema`, `desvio`, `nota`, `peso`,  `unidade`, `id_unidade`) ");
		sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		try {

			  conexao = ConexaoMysqlSGDPro.getConexao();
			  query = conexao.prepareStatement(sql.toString());

			  query.setString(1, FileUtil.verificarCampoNulo(modelo.getDescricao()));
			  query.setString(2, FileUtil.verificarCampoNulo(modelo.getIdAtividadeUnidade()));
			  query.setString(3, FileUtil.verificarCampoNulo(modelo.getIdtextoAtividadeUnidade()));
			  query.setString(4, FileUtil.verificarCampoNulo(modelo.getIdAvaliacaoUnidade()));
			  query.setString(5, FileUtil.verificarCampoNulo(modelo.getIdEscore()));
			  query.setString(6, FileUtil.verificarCampoNulo(modelo.getPercRealizado()));
			  query.setString(7, FileUtil.verificarCampoNulo(modelo.getQtPrevisto()));
			  query.setString(8, FileUtil.verificarCampoNulo(modelo.getQtRealizado()));
			  query.setString(9, FileUtil.verificarCampoNulo(modelo.getInCargaSistema()));
			  query.setString(10, FileUtil.verificarCampoNulo(modelo.getDesvio()));
			  query.setString(11, FileUtil.verificarCampoNulo(modelo.getNota()));
			  query.setString(12, (FileUtil.transformaDecimal(FileUtil.verificarCampoNulo(modelo.getPeso())) == 0) ? null : String.valueOf(FileUtil.transformaDecimal(FileUtil.verificarCampoNulo(modelo.getPeso()))) );
			  query.setString(13, modelo.getUnidade());
			  query.setString(14, modelo.getIdUnidade());


			  query.execute();
			  query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public void atualizar(AvaliacaoAtividadeUnidadeModel modelo)
	{

		StringBuilder sql = new StringBuilder();

		sql.append(" UPDATE `pratic_sgdpro`.`avaliacao_atividade_unidade` SET `id_atividade_unidade` = ?,`id_text_atividade_unidade` = ? ");
		sql.append(" , `id_avaliacao_unidade` = ?  WHERE `avaliacao_atividade_unidade`.`id` =  ? ");

		try {

			conexao = ConexaoMysqlSGDPro.getConexao();
			query = conexao.prepareStatement(sql.toString());

			query.setString(1, modelo.getIdAtividadeUnidade());
			query.setString(2, modelo.getIdtextoAtividadeUnidade());
			query.setString(3, modelo.getIdAvaliacaoUnidade());
			query.setLong(4, modelo.getId());

			query.execute();
			query.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public void exportarArquivoCarga(String arquivo){
		try {

			StringBuilder conteudo = null;

			FileWriter fileWriter = new FileWriter(arquivo);
				PrintWriter printWriter = new PrintWriter(fileWriter);
				List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = new AvaliacaoAtividadeUnidadeDAO().listarTodasAvaliacoesAtividadeUnidade(null);
				printWriter.printf("+ --------INICIO DE PROCESSAMENTO ---------+%n");
				for (AvaliacaoAtividadeUnidadeModel avaliacao : avaliacoesUnidadesCarga) {
					conteudo = new StringBuilder();
					printWriter.printf(" -- "+ avaliacao.getUnidade() +" %n");

					conteudo.append(" insert into SAD.AVALIACAO_UNIDADE_ATIVIDADE(ID_ATIVIDADE_UNIDADE,ID_TEXTO_ATIVIDADE_UNIDADE,ID_AVALIACAO_UNIDADE, ");
					conteudo.append(" ID_ESCORE,PERC_DESEMPENHO_REALIZADO,QT_DESEMPENHO_PREVISTO,QT_DESEMPENHO_REALIZADO,IN_CARGA_SISTEMA,DESVIO,NOTA,PESO) ");

					conteudo.append(" %n values (" + avaliacao.getIdAtividadeUnidade()
						+ "," + avaliacao.getIdtextoAtividadeUnidade() + ","
						+ avaliacao.getIdAvaliacaoUnidade() + ", "
						+ avaliacao.getIdEscore() + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getPercRealizado(), ".", ""), ",", ".")) + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getQtPrevisto(), ".", ""), ",", ".")) + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getQtRealizado(), ".", ""), ",", "."))  + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getInCargaSistema(), ".", ""), ",", ".")) + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getDesvio(), ".", ""), ",", ".")) + ", "
						+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getNota() , ".", ""), ",", "."))+ ", "
						+ FileUtil.validaCampoExportacao(avaliacao.getPeso()+ "); "));
					printWriter.printf(conteudo.toString()+" %n");

				}
				printWriter.printf("+---------FIM DE PROCESSAMENTO-------------+%n");

				 fileWriter.close();
				 System.out.printf("\n Arquivo gravada com sucesso em "+ arquivo);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	public void exportarArquivoCargaUpdate(String caminhoExportacao)
	{
		try
		{

			StringBuilder conteudo = null;

			FileWriter fileWriter = new FileWriter(caminhoExportacao);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = new AvaliacaoAtividadeUnidadeDAO().listarTodasAvaliacoesAtividadeUnidade(null);
			printWriter.printf("+ --------INICIO DE PROCESSAMENTO ---------+%n");

			for (AvaliacaoAtividadeUnidadeModel avaliacao : avaliacoesUnidadesCarga)
			{
				conteudo = new StringBuilder();
				printWriter.printf(" -- "+ avaliacao.getUnidade() +" %n");

				 conteudo.append(" update  SAD.AVALIACAO_UNIDADE_ATIVIDADE set  ");
				 conteudo.append(" ID_ATIVIDADE_UNIDADE = "+avaliacao.getIdAtividadeUnidade()+" ,  ");
				 conteudo.append(" ID_TEXTO_ATIVIDADE_UNIDADE = " + avaliacao.getIdtextoAtividadeUnidade() + " , ");
				 conteudo.append(" ID_AVALIACAO_UNIDADE = "+ avaliacao.getIdAvaliacaoUnidade() + ",   ");
				 conteudo.append(" ID_ESCORE = "+ avaliacao.getIdEscore() + ", ");
				 conteudo.append(" PERC_DESEMPENHO_REALIZADO = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getPercRealizado(), ".", ""), ",", ".")) + ", ");
				 conteudo.append(" QT_DESEMPENHO_PREVISTO = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getQtPrevisto(), ".", ""), ",", ".")) + ", ");
				 conteudo.append(" QT_DESEMPENHO_REALIZADO = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getQtRealizado(), ".", ""), ",", "."))  + " , ");
				 conteudo.append(" IN_CARGA_SISTEMA = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getInCargaSistema(), ".", ""), ",", ".")) + ", ");
				 conteudo.append(" DESVIO = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getDesvio(), ".", ""), ",", ".")) + ", ");
				 conteudo.append(" NOTA = "+ FileUtil.validaCampoExportacao(FileUtil.modificaTexto(FileUtil.modificaTexto(avaliacao.getNota() , ".", ""), ",", "."))+ " , ");
				 conteudo.append(" PESO = "+ FileUtil.validaCampoExportacao(avaliacao.getPeso())+ " where  ");
				 conteudo.append(" id_atividade_unidade = "+avaliacao.getIdAtividadeUnidade()+" and id_texto_atividade_unidade = " + avaliacao.getIdtextoAtividadeUnidade() + " and id_avaliacao_unidade = "+ avaliacao.getIdAvaliacaoUnidade() + " and rownum < 2 ");

				printWriter.printf(conteudo.toString()+" %n");

			}
			printWriter.printf("+---------FIM DE PROCESSAMENTO-------------+%n");

			fileWriter.close();
			System.out.printf("\n Arquivo gravada com sucesso em "+ caminhoExportacao);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void carregarRegistraArquivo(/*String caminho*/String[] split){
		try {
			//List<String> lerArquivo = FileUtil.lerArquivo(caminho);
			AvaliacaoAtividadeUnidadeDAO atividadeUnidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
			int numero = 0;
			//if(lerArquivo.size() > 0) {
				//for (String linha : lerArquivo)
				//{
					++numero;
					//LOG.info(" Processamento na linha "+ linha.toString());
					//String[] split = linha.split(";");
					if(split.length > 0) {
						if(!"#".equals(split[0])) {
							if(split[0].equals("GSIST")) {
								System.out.println(split[0]);
							}
							if(!"Orcado".equals(split[1])) {
								if(FileUtil.validaCampo(split[7])) {
									//LOG.info("Processamento Coordenação "+ split[0]);

									AvaliacaoAtividadeUnidadeModel atividadeUnidadeModel = new AvaliacaoAtividadeUnidadeModel();
									String unidade = split[0].toUpperCase();
									atividadeUnidadeModel.setIdUnidade(retornaCodigoUnidade(unidade));

									atividadeUnidadeModel.setUnidade(unidade);
									atividadeUnidadeModel.setDescricao(FileUtil.mudarSimbolosPorAcento(split[1]));
									atividadeUnidadeModel.setQtPrevisto(FileUtil.mudarSimbolosPorAcento(split[2]));
									atividadeUnidadeModel.setQtRealizado(FileUtil.mudarSimbolosPorAcento(split[3]));
									atividadeUnidadeModel.setPercRealizado(FileUtil.mudarSimbolosPorAcento(split[4]));
									atividadeUnidadeModel.setDesvio(FileUtil.mudarSimbolosPorAcento(split[5]));
									atividadeUnidadeModel.setNota(FileUtil.mudarSimbolosPorAcento(split[6]));
									atividadeUnidadeModel.setPeso(FileUtil.mudarSimbolosPorAcento(split[7]));
									atividadeUnidadeModel.setInCargaSistema("1");

									atividadeUnidadeDAO.salvar(atividadeUnidadeModel);

								}
								//LOG.info("Quebra de linha número "+ numero+", "+ linha);
							}
						}

					}

				//}
			//}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	public void processarSGDCarga(String unidade)
	{

			AvaliacaoAtividadeUnidadeDAO dao =  new AvaliacaoAtividadeUnidadeDAO();
			String textoCarga = null;
			String textoSGD = null;

		List<AvaliacaoAtividadeUnidadeModel> avaliacoesUnidadesCarga = dao.listarTodasAvaliacoesAtividadeUnidade(unidade);
		if(avaliacoesUnidadesCarga.size() > 0 && avaliacoesUnidadesCarga != null)
		{
			for (AvaliacaoAtividadeUnidadeModel avaliacaoAtividadeUnidadeModel : avaliacoesUnidadesCarga)
			{

				textoCarga = FileUtil.cortaTexto(FileUtil.modificaTexto(avaliacaoAtividadeUnidadeModel.getDescricao(), " ", "_"));

				List<AvaliacaoAtividadeUnidadeModel> localizarTextoPorUnidade = dao
						.localizarTextoPorUnidade("",
								avaliacaoAtividadeUnidadeModel.getIdUnidade(),
								avaliacaoAtividadeUnidadeModel.getUnidade());

				for (AvaliacaoAtividadeUnidadeModel avaliacaSGD : localizarTextoPorUnidade) {
					try {
						textoSGD =  FileUtil.cortaTexto(FileUtil.modificaTexto(avaliacaSGD.getDescricao(), " ", "_"));

						float checkSimilarity = FileUtil.checkSimilarity(textoSGD, textoCarga);
						if(checkSimilarity >= 0.8){
							avaliacaoAtividadeUnidadeModel.setIdAtividadeUnidade(avaliacaSGD.getIdAtividadeUnidade());
							avaliacaoAtividadeUnidadeModel.setIdtextoAtividadeUnidade(avaliacaSGD.getIdtextoAtividadeUnidade());
							avaliacaoAtividadeUnidadeModel.setIdAvaliacaoUnidade(avaliacaSGD.getIdAvaliacaoUnidade());
							dao.atualizar(avaliacaoAtividadeUnidadeModel);

						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}


			}

		}
	}


	public void realizarCargaSGD(String coordenacao){
		try {
			List<String> lerArquivo = FileUtil.lerArquivo("C:\\CSADM\\SGD\\1 SEMESTRE 2016\\new_carga_1_semestre_2016.csv");
			AvaliacaoAtividadeUnidadeDAO atividadeUnidadeDAO = new AvaliacaoAtividadeUnidadeDAO();
			int numero = 0;
			if(lerArquivo.size() > 0)
			{
				for (String linha : lerArquivo)
				{
					if(numero > 1){
						break;
					}
					String[] split = linha.split(";");
					if(!"#".equals(split[0])){
						if(!"Or�ado".equals(split[1])){
							AvaliacaoAtividadeUnidadeModel atividadeUnidadeModel = new AvaliacaoAtividadeUnidadeModel();
							atividadeUnidadeModel.setDescricao(split[0]);
							atividadeUnidadeModel.setQtPrevisto(split[1]);
							atividadeUnidadeModel.setQtRealizado(split[2]);
							atividadeUnidadeModel.setPercRealizado(split[3]);
							atividadeUnidadeModel.setDesvio(split[4]);
							atividadeUnidadeModel.setNota(split[5]);
							atividadeUnidadeModel.setPeso(split[6]);
							atividadeUnidadeModel.setInCargaSistema("1");
							atividadeUnidadeDAO.salvar(atividadeUnidadeModel);


						}
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void inserirRankdasAgencias(List<RankAgenciaDTO> rankAgencia) throws SQLException {
		try {
			conexao = ConexaoMysqlSGDPro.getConexao();

			StringBuilder sql = new StringBuilder();

			sql.append(" INSERT INTO `pratic_sgdpro`.`agencia` ( `tx_periodo`, `cd_agencia`, `qt_desempenho_global`) ");
			sql.append(" VALUES (?, ?, ?) ");

			RankAgenciaDTO agenciaDTO = null;

			query = conexao.prepareStatement(sql.toString());

			for(int i = 0; i< rankAgencia.size(); i++) {

				agenciaDTO = new RankAgenciaDTO();
				agenciaDTO = rankAgencia.get(i);
				query.setString(1, FileUtil.verificarCampoNulo(agenciaDTO.getTxPeriodo()));
				query.setString(2, FileUtil.verificarCampoNulo(agenciaDTO.getCdAgencia()));
				query.setString(3, FileUtil.verificarCampoNulo(agenciaDTO.getQtDesempenhoGlobal()));

				query.execute();

			}
			query.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			conexao.close();
		}
	}

	public ObservableList<TvRankAgencia> buscaDadosRankAgencia(String periodo) throws SQLException {
		ObservableList<TvRankAgencia> listaRankAgencias = FXCollections.observableArrayList();
		try {

			TvRankAgencia rankAgencia = null;
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT * FROM `agencia` WHERE tx_periodo LIKE ?");

			conexao = ConexaoMysqlSGDPro.getConexao();

			query = conexao.prepareStatement(sql.toString());

			query.setString(1, periodo);

			resultado = query.executeQuery();

			while(resultado.next()) {
				rankAgencia = new TvRankAgencia();
				rankAgencia.setId(resultado.getString(1));
				rankAgencia.setTxPeriodo(resultado.getString(2));
				rankAgencia.setCdAgencia(resultado.getString(3));
				rankAgencia.setQtDesempenho(resultado.getString(4));

				listaRankAgencias.add(rankAgencia);
			}
			resultado.close();
			query.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			conexao.close();
		}
		return listaRankAgencias;
	}


}
