package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;
import java.math.BigDecimal;

public class AvaliacaoBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal 	ID_AVALIACAO_EMPREGADO;
	private BigDecimal 	NU_MATRICULA_EMPREGADO;
	private BigDecimal 	ID_ESCORE;
	private String 		NO_PADRAO_DESEMPENHO;
	private String 		DS_PADRAO_DESEMPENHO;
	private String 		TX_JUSTIFICATIVA_GESTOR;
	private String 		TX_ACAO_DESENVOLVIMENTO;
	private BigDecimal	escore;
	private BigDecimal	total100;
	private BigDecimal	total200;
	private BigDecimal	total300;
	private BigDecimal	total400;
	private BigDecimal	total500;
	private BigDecimal	total600;
	private BigDecimal	total700;
	private BigDecimal	total;

	public AvaliacaoBean() {
		super();
	}

	public AvaliacaoBean(BigDecimal iD_AVALIACAO_EMPREGADO, BigDecimal nU_MATRICULA_EMPREGADO, BigDecimal iD_ESCORE,
			String nO_PADRAO_DESEMPENHO, String dS_PADRAO_DESEMPENHO, String tX_JUSTIFICATIVA_GESTOR,
			String tX_ACAO_DESENVOLVIMENTO, BigDecimal escore, BigDecimal total100, BigDecimal total200,
			BigDecimal total300, BigDecimal total400, BigDecimal total500, BigDecimal total600, BigDecimal total700,
			BigDecimal total) {
		super();
		ID_AVALIACAO_EMPREGADO = iD_AVALIACAO_EMPREGADO;
		NU_MATRICULA_EMPREGADO = nU_MATRICULA_EMPREGADO;
		ID_ESCORE = iD_ESCORE;
		NO_PADRAO_DESEMPENHO = nO_PADRAO_DESEMPENHO;
		DS_PADRAO_DESEMPENHO = dS_PADRAO_DESEMPENHO;
		TX_JUSTIFICATIVA_GESTOR = tX_JUSTIFICATIVA_GESTOR;
		TX_ACAO_DESENVOLVIMENTO = tX_ACAO_DESENVOLVIMENTO;
		this.escore = escore;
		this.total100 = total100;
		this.total200 = total200;
		this.total300 = total300;
		this.total400 = total400;
		this.total500 = total500;
		this.total600 = total600;
		this.total700 = total700;
		this.total = total;
	}

	public BigDecimal getID_AVALIACAO_EMPREGADO() {
		return ID_AVALIACAO_EMPREGADO;
	}

	public void setID_AVALIACAO_EMPREGADO(BigDecimal iD_AVALIACAO_EMPREGADO) {
		ID_AVALIACAO_EMPREGADO = iD_AVALIACAO_EMPREGADO;
	}

	public BigDecimal getNU_MATRICULA_EMPREGADO() {
		return NU_MATRICULA_EMPREGADO;
	}

	public void setNU_MATRICULA_EMPREGADO(BigDecimal nU_MATRICULA_EMPREGADO) {
		NU_MATRICULA_EMPREGADO = nU_MATRICULA_EMPREGADO;
	}

	public BigDecimal getID_ESCORE() {
		return ID_ESCORE;
	}

	public void setID_ESCORE(BigDecimal iD_ESCORE) {
		ID_ESCORE = iD_ESCORE;
	}

	public String getNO_PADRAO_DESEMPENHO() {
		return NO_PADRAO_DESEMPENHO;
	}

	public void setNO_PADRAO_DESEMPENHO(String nO_PADRAO_DESEMPENHO) {
		NO_PADRAO_DESEMPENHO = nO_PADRAO_DESEMPENHO;
	}

	public String getDS_PADRAO_DESEMPENHO() {
		return DS_PADRAO_DESEMPENHO;
	}

	public void setDS_PADRAO_DESEMPENHO(String dS_PADRAO_DESEMPENHO) {
		DS_PADRAO_DESEMPENHO = dS_PADRAO_DESEMPENHO;
	}

	public String getTX_JUSTIFICATIVA_GESTOR() {
		return TX_JUSTIFICATIVA_GESTOR;
	}

	public void setTX_JUSTIFICATIVA_GESTOR(String tX_JUSTIFICATIVA_GESTOR) {
		TX_JUSTIFICATIVA_GESTOR = tX_JUSTIFICATIVA_GESTOR;
	}

	public String getTX_ACAO_DESENVOLVIMENTO() {
		return TX_ACAO_DESENVOLVIMENTO;
	}

	public void setTX_ACAO_DESENVOLVIMENTO(String tX_ACAO_DESENVOLVIMENTO) {
		TX_ACAO_DESENVOLVIMENTO = tX_ACAO_DESENVOLVIMENTO;
	}

	public BigDecimal getEscore() {
		return escore;
	}

	public void setEscore(BigDecimal escore) {
		this.escore = escore;
	}

	public BigDecimal getTotal100() {
		return total100;
	}

	public void setTotal100(BigDecimal total100) {
		this.total100 = total100;
	}

	public BigDecimal getTotal200() {
		return total200;
	}

	public void setTotal200(BigDecimal total200) {
		this.total200 = total200;
	}

	public BigDecimal getTotal300() {
		return total300;
	}

	public void setTotal300(BigDecimal total300) {
		this.total300 = total300;
	}

	public BigDecimal getTotal400() {
		return total400;
	}

	public void setTotal400(BigDecimal total400) {
		this.total400 = total400;
	}

	public BigDecimal getTotal500() {
		return total500;
	}

	public void setTotal500(BigDecimal total500) {
		this.total500 = total500;
	}

	public BigDecimal getTotal600() {
		return total600;
	}

	public void setTotal600(BigDecimal total600) {
		this.total600 = total600;
	}

	public BigDecimal getTotal700() {
		return total700;
	}

	public void setTotal700(BigDecimal total700) {
		this.total700 = total700;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((DS_PADRAO_DESEMPENHO == null) ? 0 : DS_PADRAO_DESEMPENHO.hashCode());
		result = prime * result + ((ID_AVALIACAO_EMPREGADO == null) ? 0 : ID_AVALIACAO_EMPREGADO.hashCode());
		result = prime * result + ((ID_ESCORE == null) ? 0 : ID_ESCORE.hashCode());
		result = prime * result + ((NO_PADRAO_DESEMPENHO == null) ? 0 : NO_PADRAO_DESEMPENHO.hashCode());
		result = prime * result + ((NU_MATRICULA_EMPREGADO == null) ? 0 : NU_MATRICULA_EMPREGADO.hashCode());
		result = prime * result + ((TX_ACAO_DESENVOLVIMENTO == null) ? 0 : TX_ACAO_DESENVOLVIMENTO.hashCode());
		result = prime * result + ((TX_JUSTIFICATIVA_GESTOR == null) ? 0 : TX_JUSTIFICATIVA_GESTOR.hashCode());
		result = prime * result + ((escore == null) ? 0 : escore.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		result = prime * result + ((total100 == null) ? 0 : total100.hashCode());
		result = prime * result + ((total200 == null) ? 0 : total200.hashCode());
		result = prime * result + ((total300 == null) ? 0 : total300.hashCode());
		result = prime * result + ((total400 == null) ? 0 : total400.hashCode());
		result = prime * result + ((total500 == null) ? 0 : total500.hashCode());
		result = prime * result + ((total600 == null) ? 0 : total600.hashCode());
		result = prime * result + ((total700 == null) ? 0 : total700.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoBean other = (AvaliacaoBean) obj;
		if (DS_PADRAO_DESEMPENHO == null) {
			if (other.DS_PADRAO_DESEMPENHO != null)
				return false;
		} else if (!DS_PADRAO_DESEMPENHO.equals(other.DS_PADRAO_DESEMPENHO))
			return false;
		if (ID_AVALIACAO_EMPREGADO == null) {
			if (other.ID_AVALIACAO_EMPREGADO != null)
				return false;
		} else if (!ID_AVALIACAO_EMPREGADO.equals(other.ID_AVALIACAO_EMPREGADO))
			return false;
		if (ID_ESCORE == null) {
			if (other.ID_ESCORE != null)
				return false;
		} else if (!ID_ESCORE.equals(other.ID_ESCORE))
			return false;
		if (NO_PADRAO_DESEMPENHO == null) {
			if (other.NO_PADRAO_DESEMPENHO != null)
				return false;
		} else if (!NO_PADRAO_DESEMPENHO.equals(other.NO_PADRAO_DESEMPENHO))
			return false;
		if (NU_MATRICULA_EMPREGADO == null) {
			if (other.NU_MATRICULA_EMPREGADO != null)
				return false;
		} else if (!NU_MATRICULA_EMPREGADO.equals(other.NU_MATRICULA_EMPREGADO))
			return false;
		if (TX_ACAO_DESENVOLVIMENTO == null) {
			if (other.TX_ACAO_DESENVOLVIMENTO != null)
				return false;
		} else if (!TX_ACAO_DESENVOLVIMENTO.equals(other.TX_ACAO_DESENVOLVIMENTO))
			return false;
		if (TX_JUSTIFICATIVA_GESTOR == null) {
			if (other.TX_JUSTIFICATIVA_GESTOR != null)
				return false;
		} else if (!TX_JUSTIFICATIVA_GESTOR.equals(other.TX_JUSTIFICATIVA_GESTOR))
			return false;
		if (escore == null) {
			if (other.escore != null)
				return false;
		} else if (!escore.equals(other.escore))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		if (total100 == null) {
			if (other.total100 != null)
				return false;
		} else if (!total100.equals(other.total100))
			return false;
		if (total200 == null) {
			if (other.total200 != null)
				return false;
		} else if (!total200.equals(other.total200))
			return false;
		if (total300 == null) {
			if (other.total300 != null)
				return false;
		} else if (!total300.equals(other.total300))
			return false;
		if (total400 == null) {
			if (other.total400 != null)
				return false;
		} else if (!total400.equals(other.total400))
			return false;
		if (total500 == null) {
			if (other.total500 != null)
				return false;
		} else if (!total500.equals(other.total500))
			return false;
		if (total600 == null) {
			if (other.total600 != null)
				return false;
		} else if (!total600.equals(other.total600))
			return false;
		if (total700 == null) {
			if (other.total700 != null)
				return false;
		} else if (!total700.equals(other.total700))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AvaliacaoBean [ID_AVALIACAO_EMPREGADO=");
		builder.append(ID_AVALIACAO_EMPREGADO);
		builder.append(", NU_MATRICULA_EMPREGADO=");
		builder.append(NU_MATRICULA_EMPREGADO);
		builder.append(", ID_ESCORE=");
		builder.append(ID_ESCORE);
		builder.append(", NO_PADRAO_DESEMPENHO=");
		builder.append(NO_PADRAO_DESEMPENHO);
		builder.append(", DS_PADRAO_DESEMPENHO=");
		builder.append(DS_PADRAO_DESEMPENHO);
		builder.append(", TX_JUSTIFICATIVA_GESTOR=");
		builder.append(TX_JUSTIFICATIVA_GESTOR);
		builder.append(", TX_ACAO_DESENVOLVIMENTO=");
		builder.append(TX_ACAO_DESENVOLVIMENTO);
		builder.append(", escore=");
		builder.append(escore);
		builder.append(", total100=");
		builder.append(total100);
		builder.append(", total200=");
		builder.append(total200);
		builder.append(", total300=");
		builder.append(total300);
		builder.append(", total400=");
		builder.append(total400);
		builder.append(", total500=");
		builder.append(total500);
		builder.append(", total600=");
		builder.append(total600);
		builder.append(", total700=");
		builder.append(total700);
		builder.append(", total=");
		builder.append(total);
		builder.append("]");
		return builder.toString();
	}



}
