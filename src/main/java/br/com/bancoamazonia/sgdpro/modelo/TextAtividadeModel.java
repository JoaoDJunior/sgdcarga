package br.com.bancoamazonia.sgdpro.modelo;

import java.util.List;

import br.com.bancoamazonia.sgdpro.util.FileUtil;

public class TextAtividadeModel {
	
	private Integer idTextoAtividadeUnidade;
	private Integer idAtividadeUnidade;
	private Integer idAvaliacaoUnidade;
	private String txAtividade;
	
	public TextAtividadeModel() {
	}
	
	

	public Integer getIdAtividadeUnidade() {
		return idAtividadeUnidade;
	}

	public void setIdAtividadeUnidade(Integer idAtividadeUnidade) {
		this.idAtividadeUnidade = idAtividadeUnidade;
	}

	public Integer getIdTextoAtividadeUnidade() {
		return idTextoAtividadeUnidade;
	}

	public void setIdTextoAtividadeUnidade(Integer idTextoAtividadeUnidade) {
		this.idTextoAtividadeUnidade = idTextoAtividadeUnidade;
	}

	public Integer getIdAvaliacaoUnidade() {
		return idAvaliacaoUnidade;
	}

	public void setIdAvaliacaoUnidade(Integer idAvaliacaoUnidade) {
		this.idAvaliacaoUnidade = idAvaliacaoUnidade;
	}

	public String getTxAtividade() {
		return txAtividade;
	}

	public void setTxAtividade(String txAtividade) {
		this.txAtividade = txAtividade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idAtividadeUnidade == null) ? 0 : idAtividadeUnidade
						.hashCode());
		result = prime
				* result
				+ ((idAvaliacaoUnidade == null) ? 0 : idAvaliacaoUnidade
						.hashCode());
		result = prime
				* result
				+ ((idTextoAtividadeUnidade == null) ? 0
						: idTextoAtividadeUnidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextAtividadeModel other = (TextAtividadeModel) obj;
		if (idAtividadeUnidade == null) {
			if (other.idAtividadeUnidade != null)
				return false;
		} else if (!idAtividadeUnidade.equals(other.idAtividadeUnidade))
			return false;
		if (idAvaliacaoUnidade == null) {
			if (other.idAvaliacaoUnidade != null)
				return false;
		} else if (!idAvaliacaoUnidade.equals(other.idAvaliacaoUnidade))
			return false;
		if (idTextoAtividadeUnidade == null) {
			if (other.idTextoAtividadeUnidade != null)
				return false;
		} else if (!idTextoAtividadeUnidade
				.equals(other.idTextoAtividadeUnidade))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TextAtividadeModel [idAtividadeUnidade=");
		builder.append(idAtividadeUnidade);
		builder.append(", idTextoAtividadeUnidade=");
		builder.append(idTextoAtividadeUnidade);
		builder.append(", idAvaliacaoUnidade=");
		builder.append(idAvaliacaoUnidade);
		builder.append(", txAtividade=");
		builder.append(txAtividade);
		builder.append("]");
		return builder.toString();
	}

	
	public static void carregarTextoAtividadeArquivo(){
		try {
			
			List<String> lerArquivo = FileUtil.lerArquivo("C:\\CSADM\\SGD\\1 SEMESTRE 2016\\v2_carga_1_semestre_2016.csv");
			int numero = 0;
			if(lerArquivo.size() > 0)
			{
				for (String linha : lerArquivo) 
				{	
					System.out.println("LINHA "+ numero +" "+linha);
					
					if(numero >= 10){
						break;
					}
					numero ++;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
