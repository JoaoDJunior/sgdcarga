package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AvaliacaoEmpregado implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idAvaliacaoEmpregado;
	private Empregado empregado;
	private Empregado gestor;
	private String periodo;
	private String ano;
	private List<Avaliacao> avaliacaos = new ArrayList<Avaliacao>();
	
	public AvaliacaoEmpregado() {

	}


	public AvaliacaoEmpregado(String idAvaliacaoEmpregado, Empregado empregado,
			Empregado gestor, String periodo, String ano,
			List<Avaliacao> avaliacaos) {
		super();
		this.setIdAvaliacaoEmpregado(idAvaliacaoEmpregado);
		this.empregado = empregado;
		this.gestor = gestor;
		this.periodo = periodo;
		this.ano = ano;
		this.avaliacaos = avaliacaos;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result
				+ ((avaliacaos == null) ? 0 : avaliacaos.hashCode());
		result = prime * result
				+ ((empregado == null) ? 0 : empregado.hashCode());
		result = prime * result + ((gestor == null) ? 0 : gestor.hashCode());
		result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoEmpregado other = (AvaliacaoEmpregado) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (avaliacaos == null) {
			if (other.avaliacaos != null)
				return false;
		} else if (!avaliacaos.equals(other.avaliacaos))
			return false;
		if (empregado == null) {
			if (other.empregado != null)
				return false;
		} else if (!empregado.equals(other.empregado))
			return false;
		if (gestor == null) {
			if (other.gestor != null)
				return false;
		} else if (!gestor.equals(other.gestor))
			return false;
		if (periodo == null) {
			if (other.periodo != null)
				return false;
		} else if (!periodo.equals(other.periodo))
			return false;
		return true;
	}

	public Empregado getEmpregado() {
		return empregado;
	}

	public void setEmpregado(Empregado empregado) {
		this.empregado = empregado;
	}

	public Empregado getGestor() {
		return gestor;
	}

	public void setGestor(Empregado gestor) {
		this.gestor = gestor;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public List<Avaliacao> getAvaliacaos() {
		return avaliacaos;
	}

	public void setAvaliacaos(List<Avaliacao> avaliacaos) {
		this.avaliacaos = avaliacaos;
	}

	
	public String getIdAvaliacaoEmpregado() {
		return idAvaliacaoEmpregado;
	}


	public void setIdAvaliacaoEmpregado(String idAvaliacaoEmpregado) {
		this.idAvaliacaoEmpregado = idAvaliacaoEmpregado;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AvaliacaoEmpregado [idAvaliacaoEmpregado=");
		builder.append(idAvaliacaoEmpregado);
		builder.append(", empregado=");
		builder.append(empregado);
		builder.append(", gestor=");
		builder.append(gestor);
		builder.append(", periodo=");
		builder.append(periodo);
		builder.append(", ano=");
		builder.append(ano);
		builder.append(", avaliacaos=");
		builder.append(avaliacaos);
		builder.append("]");
		return builder.toString();
	}
	

}
