package br.com.bancoamazonia.sgdpro.modelo.dto;

import java.io.Serializable;

public class SGDEmpregadoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String matricula;
	private String nomeEmpregado;
	private String cargo;
	private String funcao;
	private String dataIniciofuncao;
	private String caraterGestor;
	private String unidade;
	private String codigoUnidade;
	private String matriculaGestorImediato;
	private String nomeGestorImediato;
	
	public SGDEmpregadoDTO() {
	}
	
	

	public SGDEmpregadoDTO(String matricula) {
		super();
		this.matricula = matricula;
	}



	public SGDEmpregadoDTO(String matricula, String nomeEmpregado, String cargo, String funcao, String dataIniciofuncao,
			String caraterGestor, String unidade, String codigoUnidade, String matriculaGestorImediato,
			String nomeGestorImediato) {
		super();
		this.matricula = matricula;
		this.nomeEmpregado = nomeEmpregado;
		this.cargo = cargo;
		this.funcao = funcao;
		this.dataIniciofuncao = dataIniciofuncao;
		this.caraterGestor = caraterGestor;
		this.unidade = unidade;
		this.codigoUnidade = codigoUnidade;
		this.matriculaGestorImediato = matriculaGestorImediato;
		this.nomeGestorImediato = nomeGestorImediato;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNomeEmpregado() {
		return nomeEmpregado;
	}

	public void setNomeEmpregado(String nomeEmpregado) {
		this.nomeEmpregado = nomeEmpregado;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getDataIniciofuncao() {
		return dataIniciofuncao;
	}

	public void setDataIniciofuncao(String dataIniciofuncao) {
		this.dataIniciofuncao = dataIniciofuncao;
	}

	public String getCaraterGestor() {
		return caraterGestor;
	}

	public void setCaraterGestor(String caraterGestor) {
		this.caraterGestor = caraterGestor;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	public String getMatriculaGestorImediato() {
		return matriculaGestorImediato;
	}

	public void setMatriculaGestorImediato(String matriculaGestorImediato) {
		this.matriculaGestorImediato = matriculaGestorImediato;
	}

	public String getNomeGestorImediato() {
		return nomeGestorImediato;
	}

	public void setNomeGestorImediato(String nomeGestorImediato) {
		this.nomeGestorImediato = nomeGestorImediato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((funcao == null) ? 0 : funcao.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nomeEmpregado == null) ? 0 : nomeEmpregado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SGDEmpregadoDTO other = (SGDEmpregadoDTO) obj;
		if (funcao == null) {
			if (other.funcao != null)
				return false;
		} else if (!funcao.equals(other.funcao))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nomeEmpregado == null) {
			if (other.nomeEmpregado != null)
				return false;
		} else if (!nomeEmpregado.equals(other.nomeEmpregado))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SGDEmpregadoDTO [matricula=");
		builder.append(matricula);
		builder.append(", nomeEmpregado=");
		builder.append(nomeEmpregado);
		builder.append(", cargo=");
		builder.append(cargo);
		builder.append(", funcao=");
		builder.append(funcao);
		builder.append(", dataIniciofuncao=");
		builder.append(dataIniciofuncao);
		builder.append(", caraterGestor=");
		builder.append(caraterGestor);
		builder.append(", unidade=");
		builder.append(unidade);
		builder.append(", codigoUnidade=");
		builder.append(codigoUnidade);
		builder.append(", matriculaGestorImediato=");
		builder.append(matriculaGestorImediato);
		builder.append(", nomeGestorImediato=");
		builder.append(nomeGestorImediato);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	

}
