package br.com.bancoamazonia.sgdpro.modelo;

public class AvaliacaoAtividadeUnidadeModel {
	
	private Long id;
	private String descricao;
	private String unidade;
	private String idUnidade;
	private String idAtividadeUnidade;
	private String idtextoAtividadeUnidade;
	private String idAvaliacaoUnidade;
	private String idEscore;
	private String percRealizado;
	private String qtPrevisto;
	private String qtRealizado;
	private String inCargaSistema;
	private String desvio;
	private String nota;
	private String peso;
	
	public AvaliacaoAtividadeUnidadeModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getIdAtividadeUnidade() {
		return idAtividadeUnidade;
	}

	public void setIdAtividadeUnidade(String idAtividadeUnidade) {
		this.idAtividadeUnidade = idAtividadeUnidade;
	}

	public String getIdtextoAtividadeUnidade() {
		return idtextoAtividadeUnidade;
	}

	public void setIdtextoAtividadeUnidade(String idtextoAtividadeUnidade) {
		this.idtextoAtividadeUnidade = idtextoAtividadeUnidade;
	}

	public String getIdAvaliacaoUnidade() {
		return idAvaliacaoUnidade;
	}

	public void setIdAvaliacaoUnidade(String idAvaliacaoUnidade) {
		this.idAvaliacaoUnidade = idAvaliacaoUnidade;
	}

	public String getIdEscore() {
		return idEscore;
	}

	public void setIdEscore(String idEscore) {
		this.idEscore = idEscore;
	}

	public String getPercRealizado() {
		return percRealizado;
	}

	public void setPercRealizado(String percRealizado) {
		this.percRealizado = percRealizado;
	}

	public String getQtPrevisto() {
		return qtPrevisto;
	}

	public void setQtPrevisto(String qtPrevisto) {
		this.qtPrevisto = qtPrevisto;
	}

	public String getQtRealizado() {
		return qtRealizado;
	}

	public void setQtRealizado(String qtRealizado) {
		this.qtRealizado = qtRealizado;
	}

	public String getInCargaSistema() {
		return inCargaSistema;
	}

	public void setInCargaSistema(String inCargaSistema) {
		this.inCargaSistema = inCargaSistema;
	}

	public String getDesvio() {
		return desvio;
	}

	public void setDesvio(String desvio) {
		this.desvio = desvio;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoAtividadeUnidadeModel other = (AvaliacaoAtividadeUnidadeModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

		public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(String idUnidade) {
		this.idUnidade = idUnidade;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AvaliacaoAtividadeUnidadeModel [id=");
		builder.append(id);
		builder.append(", descricao=");
		builder.append(descricao);
		builder.append(", unidade=");
		builder.append(unidade);
		builder.append(", idUnidade=");
		builder.append(idUnidade);
		builder.append(", idAtividadeUnidade=");
		builder.append(idAtividadeUnidade);
		builder.append(", idtextoAtividadeUnidade=");
		builder.append(idtextoAtividadeUnidade);
		builder.append(", idAvaliacaoUnidade=");
		builder.append(idAvaliacaoUnidade);
		builder.append(", idEscore=");
		builder.append(idEscore);
		builder.append(", percRealizado=");
		builder.append(percRealizado);
		builder.append(", qtPrevisto=");
		builder.append(qtPrevisto);
		builder.append(", qtRealizado=");
		builder.append(qtRealizado);
		builder.append(", inCargaSistema=");
		builder.append(inCargaSistema);
		builder.append(", desvio=");
		builder.append(desvio);
		builder.append(", nota=");
		builder.append(nota);
		builder.append(", peso=");
		builder.append(peso);
		builder.append("]");
		return builder.toString();
	}
	
}