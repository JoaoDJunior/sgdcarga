package br.com.bancoamazonia.sgdpro.modelo;

import javafx.scene.control.CheckBox;

public class TvAvaliacaoEmpregado {

	private CheckBox	selection;
	private String 		matricula;
	private String 		empregado;
	private String 		avaliador;
	private String 		periodo;
	private String 		ano;
	private String 		idAvaliacao;
	private String 		unidade;

	public TvAvaliacaoEmpregado() {
		super();
	}

	public TvAvaliacaoEmpregado(CheckBox selection, String matricula, String empregado, String avaliador,
			String periodo, String ano, String idAvaliacao, String unidade) {
		super();
		this.selection = selection;
		this.matricula = matricula;
		this.empregado = empregado;
		this.avaliador = avaliador;
		this.periodo = periodo;
		this.ano = ano;
		this.idAvaliacao = idAvaliacao;
		this.unidade = unidade;
	}

	public CheckBox getSelection() {
		return selection;
	}

	public void setSelection(CheckBox selection) {
		this.selection = selection;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmpregado() {
		return empregado;
	}

	public void setEmpregado(String empregado) {
		this.empregado = empregado;
	}

	public String getAvaliador() {
		return avaliador;
	}

	public void setAvaliador(String avaliador) {
		this.avaliador = avaliador;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getIdAvaliacao() {
		return idAvaliacao;
	}

	public void setIdAvaliacao(String idAvaliacao) {
		this.idAvaliacao = idAvaliacao;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((avaliador == null) ? 0 : avaliador.hashCode());
		result = prime * result + ((empregado == null) ? 0 : empregado.hashCode());
		result = prime * result + ((idAvaliacao == null) ? 0 : idAvaliacao.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
		result = prime * result + ((unidade == null) ? 0 : unidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TvAvaliacaoEmpregado other = (TvAvaliacaoEmpregado) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (avaliador == null) {
			if (other.avaliador != null)
				return false;
		} else if (!avaliador.equals(other.avaliador))
			return false;
		if (empregado == null) {
			if (other.empregado != null)
				return false;
		} else if (!empregado.equals(other.empregado))
			return false;
		if (idAvaliacao == null) {
			if (other.idAvaliacao != null)
				return false;
		} else if (!idAvaliacao.equals(other.idAvaliacao))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (periodo == null) {
			if (other.periodo != null)
				return false;
		} else if (!periodo.equals(other.periodo))
			return false;
		if (unidade == null) {
			if (other.unidade != null)
				return false;
		} else if (!unidade.equals(other.unidade))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TvAvaliacaoEmpregado [matricula=");
		builder.append(matricula);
		builder.append(", empregado=");
		builder.append(empregado);
		builder.append(", avaliador=");
		builder.append(avaliador);
		builder.append(", periodo=");
		builder.append(periodo);
		builder.append(", ano=");
		builder.append(ano);
		builder.append(", idAvaliacao=");
		builder.append(idAvaliacao);
		builder.append(", unidade=");
		builder.append(unidade);
		builder.append("]");
		return builder.toString();
	}



}
