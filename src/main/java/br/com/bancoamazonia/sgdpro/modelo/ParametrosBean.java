package br.com.bancoamazonia.sgdpro.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;



public class ParametrosBean {

	private String matricula;
	private List<String> semestre;
	private List<BigDecimal> ano;

	public ParametrosBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ParametrosBean(String matricula, List<String> semestre, List<BigDecimal> ano) {
		super();
		this.matricula = matricula;
		this.semestre = semestre;
		this.ano = ano;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public List<String> getSemestre() {
		return semestre;
	}
	public void setSemestre(List<String> semestre) {
		this.semestre = semestre;
	}
	public List<BigDecimal> getAno() {
		return ano;
	}
	public void setAno(List<BigDecimal> ano) {
		this.ano = ano;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((semestre == null) ? 0 : semestre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametrosBean other = (ParametrosBean) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (semestre == null) {
			if (other.semestre != null)
				return false;
		} else if (!semestre.equals(other.semestre))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parametros [matricula=");
		builder.append(matricula);
		builder.append(", semestre=");
		builder.append(semestre);
		builder.append(", ano=");
		builder.append(ano);
		builder.append("]");
		return builder.toString();
	}

}

