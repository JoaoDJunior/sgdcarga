package br.com.bancoamazonia.sgdpro.modelo;

import java.util.List;

import br.com.bancoamazonia.sgdpro.application.controller.ResultadoIndicadores;

public class EmpregadoBean {

	private	List<DadosBean>			dadosBean;
	private	List<AvaliacaoBean>		avaliacaoBean;
	private	List<IndicadoresBean>	indicadoresBean;
	private	List<GraficoBean>		graficoBean;
	private	String					nomedoArquivo;
	private List<ResultadoIndicadores>	resultadoIndicadores;


	public EmpregadoBean() {
		super();
	}

	public EmpregadoBean(List<DadosBean> dadosBean, List<AvaliacaoBean> avaliacaoBean,
			List<IndicadoresBean> indicadoresBean, List<GraficoBean> graficoBean, String nomedoArquivo,
			List<ResultadoIndicadores> resultadoIndicadores) {
		super();
		this.dadosBean = dadosBean;
		this.avaliacaoBean = avaliacaoBean;
		this.indicadoresBean = indicadoresBean;
		this.graficoBean = graficoBean;
		this.nomedoArquivo = nomedoArquivo;
		this.resultadoIndicadores = resultadoIndicadores;
	}

	public List<DadosBean> getDadosBean() {
		return dadosBean;
	}

	public void setDadosBean(List<DadosBean> dadosBean) {
		this.dadosBean = dadosBean;
	}

	public List<AvaliacaoBean> getAvaliacaoBean() {
		return avaliacaoBean;
	}

	public void setAvaliacaoBean(List<AvaliacaoBean> avaliacaoBean) {
		this.avaliacaoBean = avaliacaoBean;
	}

	public List<IndicadoresBean> getIndicadoresBean() {
		return indicadoresBean;
	}

	public void setIndicadoresBean(List<IndicadoresBean> indicadoresBean) {
		this.indicadoresBean = indicadoresBean;
	}

	public List<GraficoBean> getGraficoBean() {
		return graficoBean;
	}

	public void setGraficoBean(List<GraficoBean> graficoBean) {
		this.graficoBean = graficoBean;
	}

	public String getNomedoArquivo() {
		return nomedoArquivo;
	}

	public void setNomedoArquivo(String nomedoArquivo) {
		this.nomedoArquivo = nomedoArquivo;
	}

	public List<ResultadoIndicadores> getResultadoIndicadores() {
		return resultadoIndicadores;
	}

	public void setResultadoIndicadores(List<ResultadoIndicadores> resultadoIndicadores) {
		this.resultadoIndicadores = resultadoIndicadores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avaliacaoBean == null) ? 0 : avaliacaoBean.hashCode());
		result = prime * result + ((dadosBean == null) ? 0 : dadosBean.hashCode());
		result = prime * result + ((graficoBean == null) ? 0 : graficoBean.hashCode());
		result = prime * result + ((indicadoresBean == null) ? 0 : indicadoresBean.hashCode());
		result = prime * result + ((nomedoArquivo == null) ? 0 : nomedoArquivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpregadoBean other = (EmpregadoBean) obj;
		if (avaliacaoBean == null) {
			if (other.avaliacaoBean != null)
				return false;
		} else if (!avaliacaoBean.equals(other.avaliacaoBean))
			return false;
		if (dadosBean == null) {
			if (other.dadosBean != null)
				return false;
		} else if (!dadosBean.equals(other.dadosBean))
			return false;
		if (graficoBean == null) {
			if (other.graficoBean != null)
				return false;
		} else if (!graficoBean.equals(other.graficoBean))
			return false;
		if (indicadoresBean == null) {
			if (other.indicadoresBean != null)
				return false;
		} else if (!indicadoresBean.equals(other.indicadoresBean))
			return false;
		if (nomedoArquivo == null) {
			if (other.nomedoArquivo != null)
				return false;
		} else if (!nomedoArquivo.equals(other.nomedoArquivo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmpregadoBean [dadosBean=");
		builder.append(dadosBean);
		builder.append(", avaliacaoBean=");
		builder.append(avaliacaoBean);
		builder.append(", indicadoresBean=");
		builder.append(indicadoresBean);
		builder.append(", graficoBean=");
		builder.append(graficoBean);
		builder.append(", nomedoArquivo=");
		builder.append(nomedoArquivo);
		builder.append(", resultadoIndicadores=");
		builder.append(resultadoIndicadores);
		builder.append("]");
		return builder.toString();
	}




}