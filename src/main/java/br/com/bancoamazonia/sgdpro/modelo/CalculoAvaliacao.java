package br.com.bancoamazonia.sgdpro.modelo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.groovy.transform.NotYetImplementedASTTransformation;

public class CalculoAvaliacao {

	public List<BigDecimal> CalculoEscore(List<AvaliacaoBean> avaliacaoBeans) {
		BigDecimal pontuacao = null;
		List<BigDecimal> listaPontuacao = new ArrayList<BigDecimal>();
		for(int i = 0; i<avaliacaoBeans.size(); i++) {

			AvaliacaoBean avaliacaoBean = avaliacaoBeans.get(i);
			BigDecimal escore = avaliacaoBean.getID_ESCORE();
			pontuacao = escore.multiply(BigDecimal.valueOf(100));
			listaPontuacao.add(i, pontuacao);
		}

		return listaPontuacao;

	}

	public List<AvaliacaoBean> CalculoNotas(List<BigDecimal> listaPontuacao, List<AvaliacaoBean> avaliacaoBeans) {

		AvaliacaoBean avaliacaoBean = new AvaliacaoBean();

		BigDecimal E1 = new BigDecimal(100);
		BigDecimal E2 = new BigDecimal(200);
		BigDecimal E3 = new BigDecimal(300);
		BigDecimal E4 = new BigDecimal(400);
		BigDecimal E5 = new BigDecimal(500);
		BigDecimal E6 = new BigDecimal(600);
		BigDecimal E7 = new BigDecimal(700);
		BigDecimal T1 = new BigDecimal(0);
		BigDecimal T2 = new BigDecimal(0);
		BigDecimal T3 = new BigDecimal(0);
		BigDecimal T4 = new BigDecimal(0);
		BigDecimal T5 = new BigDecimal(0);
		BigDecimal T6 = new BigDecimal(0);
		BigDecimal T7 = new BigDecimal(0);
		BigDecimal resultado = new BigDecimal(0);
		BigDecimal pontuacao = new BigDecimal(0);

		for(int i = 0; i<listaPontuacao.size(); i++) {

			pontuacao = listaPontuacao.get(i);

			if (E1.equals(pontuacao)) {
				T1 = E1.add(T1);
				resultado = E1.add(resultado);
			}
			if (E2.equals(pontuacao)) {
				T2 = E2.add(T2);
				resultado = E2.add(resultado);
			}
			if (E3.equals(pontuacao)) {
				T3 = E3.add(T3);
				resultado = E3.add(resultado);
			}
			if (E4.equals(pontuacao)) {
				T4 = E4.add(T4);
				resultado = E4.add(resultado);
			}
			if (E5.equals(pontuacao)) {
				T5 = E5.add(T5);
				resultado = E5.add(resultado);
			}
			if (E6.equals(pontuacao)) {
				T6 = E6.add(T6);
				resultado = E6.add(resultado);
			}
			if (E7.equals(pontuacao)) {
				T7 = E7.add(T7);
				resultado = E7.add(resultado);
			}
	}
		for(int i = 0; i < avaliacaoBeans.size(); i++) {
			avaliacaoBean = avaliacaoBeans.get(i);
			avaliacaoBean.setTotal100(T1);
			avaliacaoBean.setTotal200(T2);
			avaliacaoBean.setTotal300(T3);
			avaliacaoBean.setTotal400(T4);
			avaliacaoBean.setTotal500(T5);
			avaliacaoBean.setTotal600(T6);
			avaliacaoBean.setTotal700(T7);
			avaliacaoBean.setTotal(resultado);
			avaliacaoBeans.set(i, avaliacaoBean);
		}
		return avaliacaoBeans;

	}

	public static double CalculoNotaUnidade(List<IndicadoresBean> indicadoresBeans, PerspectivaAgencia perspectivaAgencia) {

		double totalDePontosUnidade = 0;		//NotaUnidade

		double notaFinalUnidade = 0;		//NotaFinalUnidade

		double pesoMaisResultado = 0;		//PesoMaisResultado

		double pesoMaisFoco = 0;		//PesoMaisFoco

		double percentual = 100;

		for(IndicadoresBean ib : indicadoresBeans) {
			totalDePontosUnidade += ((ib.getNOTA().doubleValue() * percentual) * ib.getPESO().doubleValue());
		}

		if (perspectivaAgencia.getPeso() == 0) {
			pesoMaisResultado = 0.3;
		} else {
			pesoMaisResultado = perspectivaAgencia.getPeso();
		}

		pesoMaisFoco = 1 - pesoMaisResultado;

		notaFinalUnidade = (totalDePontosUnidade * pesoMaisFoco) + (pesoMaisResultado * perspectivaAgencia.getDesempenhoGlobal());

		return notaFinalUnidade;

	}



	public static double CalculoNotaFinalEmpregado(double totalPontosEmpregado, double notaFinalUnidade) {

		double indiceEmpregado = 0.6;

		double indiceUnidade = 0.4;

		double notaFinal = 0;

		notaFinal = ((totalPontosEmpregado/*/10*/) * indiceEmpregado) + (notaFinalUnidade * indiceUnidade);

		return notaFinal;

	}

}