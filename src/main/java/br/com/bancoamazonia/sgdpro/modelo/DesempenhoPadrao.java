package br.com.bancoamazonia.sgdpro.modelo;

public enum DesempenhoPadrao {
	
	TEMPO_ORGANIZACAO(14),
	INICIATIVA(15),
	TRABALHO_EM_EQUIPE(16),
	COMUNICACAO(17),
	FLEXIBILIDADE(18),
	NEGOCIACAO(19),
	TOMADA_DE_DECISAO(20),
	VISAO_SISTEMICA_E_ESTRATEGICA(21),
	RELACIONAMENTO_INTERPESSOAL(22),
	RESPONSABILIDADE_SOCIAL(24);
	
	
	public int valor;
	
	DesempenhoPadrao(int valor) {
		this.valor = valor; 
	}
	
	public int getValor(){
		return this.valor;
	}
}
