package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;
import java.math.BigDecimal;

public class IndicadoresBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal 	NU_UNIDADE;
	private String 		TX_PERIODO_AVALIACAO;
	private BigDecimal 	ANO_AVALIACAO;
	private String		TI_ATIVIDADE;
	private String		TX_ATIVIDADE;
	private BigDecimal	PERC_DESEMPENHO_REALIZADO;
	private BigDecimal	QT_DESEMPENHO_PREVISTO;
	private BigDecimal	QT_DESEMPENHO_REALIZADO;
	private BigDecimal	DESVIO;
	private BigDecimal	NOTA;
	private BigDecimal	PESO;

	public IndicadoresBean() {
		super();
	}



	public IndicadoresBean(BigDecimal nU_UNIDADE, String tX_PERIODO_AVALIACAO, BigDecimal aNO_AVALIACAO,
			String tI_ATIVIDADE, String tX_ATIVIDADE, BigDecimal pERC_DESEMPENHO_REALIZADO,
			BigDecimal qT_DESEMPENHO_PREVISTO, BigDecimal qT_DESEMPENHO_REALIZADO, BigDecimal dESVIO, BigDecimal nOTA,
			BigDecimal pESO) {
		super();
		NU_UNIDADE = nU_UNIDADE;
		TX_PERIODO_AVALIACAO = tX_PERIODO_AVALIACAO;
		ANO_AVALIACAO = aNO_AVALIACAO;
		TI_ATIVIDADE = tI_ATIVIDADE;
		TX_ATIVIDADE = tX_ATIVIDADE;
		PERC_DESEMPENHO_REALIZADO = pERC_DESEMPENHO_REALIZADO;
		QT_DESEMPENHO_PREVISTO = qT_DESEMPENHO_PREVISTO;
		QT_DESEMPENHO_REALIZADO = qT_DESEMPENHO_REALIZADO;
		DESVIO = dESVIO;
		NOTA = nOTA;
		PESO = pESO;
	}

	public BigDecimal getNU_UNIDADE() {
		return NU_UNIDADE;
	}
	public void setNU_UNIDADE(BigDecimal nU_UNIDADE) {
		NU_UNIDADE = nU_UNIDADE;
	}
	public String getTX_PERIODO_AVALIACAO() {
		return TX_PERIODO_AVALIACAO;
	}
	public void setTX_PERIODO_AVALIACAO(String tX_PERIODO_AVALIACAO) {
		TX_PERIODO_AVALIACAO = tX_PERIODO_AVALIACAO;
	}
	public BigDecimal getANO_AVALIACAO() {
		return ANO_AVALIACAO;
	}
	public void setANO_AVALIACAO(BigDecimal aNO_AVALIACAO) {
		ANO_AVALIACAO = aNO_AVALIACAO;
	}
	public String getTI_ATIVIDADE() {
		return TI_ATIVIDADE;
	}
	public void setTI_ATIVIDADE(String tI_ATIVIDADE) {
		TI_ATIVIDADE = tI_ATIVIDADE;
	}
	public String getTX_ATIVIDADE() {
		return TX_ATIVIDADE;
	}
	public void setTX_ATIVIDADE(String tX_ATIVIDADE) {
		TX_ATIVIDADE = tX_ATIVIDADE;
	}
	public BigDecimal getPERC_DESEMPENHO_REALIZADO() {
		return PERC_DESEMPENHO_REALIZADO;
	}
	public void setPERC_DESEMPENHO_REALIZADO(BigDecimal pERC_DESEMPENHO_REALIZADO) {
		PERC_DESEMPENHO_REALIZADO = pERC_DESEMPENHO_REALIZADO;
	}
	public BigDecimal getQT_DESEMPENHO_PREVISTO() {
		return QT_DESEMPENHO_PREVISTO;
	}
	public void setQT_DESEMPENHO_PREVISTO(BigDecimal qT_DESEMPENHO_PREVISTO) {
		QT_DESEMPENHO_PREVISTO = qT_DESEMPENHO_PREVISTO;
	}
	public BigDecimal getQT_DESEMPENHO_REALIZADO() {
		return QT_DESEMPENHO_REALIZADO;
	}
	public void setQT_DESEMPENHO_REALIZADO(BigDecimal qT_DESEMPENHO_REALIZADO) {
		QT_DESEMPENHO_REALIZADO = qT_DESEMPENHO_REALIZADO;
	}
	public BigDecimal getDESVIO() {
		return DESVIO;
	}
	public void setDESVIO(BigDecimal dESVIO) {
		DESVIO = dESVIO;
	}
	public BigDecimal getNOTA() {
		return NOTA;
	}
	public void setNOTA(BigDecimal nOTA) {
		NOTA = nOTA;
	}
	public BigDecimal getPESO() {
		return PESO;
	}
	public void setPESO(BigDecimal pESO) {
		PESO = pESO;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ANO_AVALIACAO == null) ? 0 : ANO_AVALIACAO.hashCode());
		result = prime * result + ((DESVIO == null) ? 0 : DESVIO.hashCode());
		result = prime * result + ((NOTA == null) ? 0 : NOTA.hashCode());
		result = prime * result + ((NU_UNIDADE == null) ? 0 : NU_UNIDADE.hashCode());
		result = prime * result + ((PERC_DESEMPENHO_REALIZADO == null) ? 0 : PERC_DESEMPENHO_REALIZADO.hashCode());
		result = prime * result + ((PESO == null) ? 0 : PESO.hashCode());
		result = prime * result + ((QT_DESEMPENHO_PREVISTO == null) ? 0 : QT_DESEMPENHO_PREVISTO.hashCode());
		result = prime * result + ((QT_DESEMPENHO_REALIZADO == null) ? 0 : QT_DESEMPENHO_REALIZADO.hashCode());
		result = prime * result + ((TI_ATIVIDADE == null) ? 0 : TI_ATIVIDADE.hashCode());
		result = prime * result + ((TX_ATIVIDADE == null) ? 0 : TX_ATIVIDADE.hashCode());
		result = prime * result + ((TX_PERIODO_AVALIACAO == null) ? 0 : TX_PERIODO_AVALIACAO.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndicadoresBean other = (IndicadoresBean) obj;
		if (ANO_AVALIACAO == null) {
			if (other.ANO_AVALIACAO != null)
				return false;
		} else if (!ANO_AVALIACAO.equals(other.ANO_AVALIACAO))
			return false;
		if (DESVIO == null) {
			if (other.DESVIO != null)
				return false;
		} else if (!DESVIO.equals(other.DESVIO))
			return false;
		if (NOTA == null) {
			if (other.NOTA != null)
				return false;
		} else if (!NOTA.equals(other.NOTA))
			return false;
		if (NU_UNIDADE == null) {
			if (other.NU_UNIDADE != null)
				return false;
		} else if (!NU_UNIDADE.equals(other.NU_UNIDADE))
			return false;
		if (PERC_DESEMPENHO_REALIZADO == null) {
			if (other.PERC_DESEMPENHO_REALIZADO != null)
				return false;
		} else if (!PERC_DESEMPENHO_REALIZADO.equals(other.PERC_DESEMPENHO_REALIZADO))
			return false;
		if (PESO == null) {
			if (other.PESO != null)
				return false;
		} else if (!PESO.equals(other.PESO))
			return false;
		if (QT_DESEMPENHO_PREVISTO == null) {
			if (other.QT_DESEMPENHO_PREVISTO != null)
				return false;
		} else if (!QT_DESEMPENHO_PREVISTO.equals(other.QT_DESEMPENHO_PREVISTO))
			return false;
		if (QT_DESEMPENHO_REALIZADO == null) {
			if (other.QT_DESEMPENHO_REALIZADO != null)
				return false;
		} else if (!QT_DESEMPENHO_REALIZADO.equals(other.QT_DESEMPENHO_REALIZADO))
			return false;
		if (TI_ATIVIDADE == null) {
			if (other.TI_ATIVIDADE != null)
				return false;
		} else if (!TI_ATIVIDADE.equals(other.TI_ATIVIDADE))
			return false;
		if (TX_ATIVIDADE == null) {
			if (other.TX_ATIVIDADE != null)
				return false;
		} else if (!TX_ATIVIDADE.equals(other.TX_ATIVIDADE))
			return false;
		if (TX_PERIODO_AVALIACAO == null) {
			if (other.TX_PERIODO_AVALIACAO != null)
				return false;
		} else if (!TX_PERIODO_AVALIACAO.equals(other.TX_PERIODO_AVALIACAO))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndicadoresBean [NU_UNIDADE=");
		builder.append(NU_UNIDADE);
		builder.append(", TX_PERIODO_AVALIACAO=");
		builder.append(TX_PERIODO_AVALIACAO);
		builder.append(", ANO_AVALIACAO=");
		builder.append(ANO_AVALIACAO);
		builder.append(", TI_ATIVIDADE=");
		builder.append(TI_ATIVIDADE);
		builder.append(", TX_ATIVIDADE=");
		builder.append(TX_ATIVIDADE);
		builder.append(", PERC_DESEMPENHO_REALIZADO=");
		builder.append(PERC_DESEMPENHO_REALIZADO);
		builder.append(", QT_DESEMPENHO_PREVISTO=");
		builder.append(QT_DESEMPENHO_PREVISTO);
		builder.append(", QT_DESEMPENHO_REALIZADO=");
		builder.append(QT_DESEMPENHO_REALIZADO);
		builder.append(", DESVIO=");
		builder.append(DESVIO);
		builder.append(", NOTA=");
		builder.append(NOTA);
		builder.append(", PESO=");
		builder.append(PESO);
		builder.append("]");
		return builder.toString();
	}



}

