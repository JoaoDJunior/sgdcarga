package br.com.bancoamazonia.sgdpro.modelo.dto;

import java.io.Serializable;

public class RankAgenciaDTO implements Serializable  {

	private static final long serialVersionUID = 1L;
	
	private String txPeriodo;
	private String cdAgencia;
	private String QtDesempenhoGlobal;
	
	public RankAgenciaDTO() {
	}

	public RankAgenciaDTO(String txPeriodo, String cdAgencia,
			String qtDesempenhoGlobal) {
		super();
		this.txPeriodo = txPeriodo;
		this.cdAgencia = cdAgencia;
		QtDesempenhoGlobal = qtDesempenhoGlobal;
	}

	public String getTxPeriodo() {
		return txPeriodo;
	}

	public void setTxPeriodo(String txPeriodo) {
		this.txPeriodo = txPeriodo;
	}

	public String getCdAgencia() {
		return cdAgencia;
	}

	public void setCdAgencia(String cdAgencia) {
		this.cdAgencia = cdAgencia;
	}

	public String getQtDesempenhoGlobal() {
		return QtDesempenhoGlobal;
	}

	public void setQtDesempenhoGlobal(String qtDesempenhoGlobal) {
		QtDesempenhoGlobal = qtDesempenhoGlobal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdAgencia == null) ? 0 : cdAgencia.hashCode());
		result = prime * result
				+ ((txPeriodo == null) ? 0 : txPeriodo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RankAgenciaDTO other = (RankAgenciaDTO) obj;
		if (cdAgencia == null) {
			if (other.cdAgencia != null)
				return false;
		} else if (!cdAgencia.equals(other.cdAgencia))
			return false;
		if (txPeriodo == null) {
			if (other.txPeriodo != null)
				return false;
		} else if (!txPeriodo.equals(other.txPeriodo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RankAgencia [txPeriodo=");
		builder.append(txPeriodo);
		builder.append(", cdAgencia=");
		builder.append(cdAgencia);
		builder.append(", QtDesempenhoGlobal=");
		builder.append(QtDesempenhoGlobal);
		builder.append("]");
		return builder.toString();
	}
	
	

}
