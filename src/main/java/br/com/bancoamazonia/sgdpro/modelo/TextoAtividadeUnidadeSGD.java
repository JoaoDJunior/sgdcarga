package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;

public class TextoAtividadeUnidadeSGD implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer idTextoAtividadeUnidade;
	private String tiAtividade;
	private String txAtividade;
	private Integer idAtividadeUnidade;
	
	public TextoAtividadeUnidadeSGD() {
	}

	public TextoAtividadeUnidadeSGD(Integer idTextoAtividadeUnidade, String tiAtividade, String txAtividade,
			Integer idAtividadeUnidade) {
		super();
		this.idTextoAtividadeUnidade = idTextoAtividadeUnidade;
		this.tiAtividade = tiAtividade;
		this.txAtividade = txAtividade;
		this.idAtividadeUnidade = idAtividadeUnidade;
	}

	public Integer getIdTextoAtividadeUnidade() {
		return idTextoAtividadeUnidade;
	}

	public void setIdTextoAtividadeUnidade(Integer idTextoAtividadeUnidade) {
		this.idTextoAtividadeUnidade = idTextoAtividadeUnidade;
	}

	public String getTiAtividade() {
		return tiAtividade;
	}

	public void setTiAtividade(String tiAtividade) {
		this.tiAtividade = tiAtividade;
	}

	public String getTxAtividade() {
		return txAtividade;
	}

	public void setTxAtividade(String txAtividade) {
		this.txAtividade = txAtividade;
	}

	public Integer getIdAtividadeUnidade() {
		return idAtividadeUnidade;
	}

	public void setIdAtividadeUnidade(Integer idAtividadeUnidade) {
		this.idAtividadeUnidade = idAtividadeUnidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAtividadeUnidade == null) ? 0 : idAtividadeUnidade.hashCode());
		result = prime * result + ((txAtividade == null) ? 0 : txAtividade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextoAtividadeUnidadeSGD other = (TextoAtividadeUnidadeSGD) obj;
		if (idAtividadeUnidade == null) {
			if (other.idAtividadeUnidade != null)
				return false;
		} else if (!idAtividadeUnidade.equals(other.idAtividadeUnidade))
			return false;
		if (txAtividade == null) {
			if (other.txAtividade != null)
				return false;
		} else if (!txAtividade.equals(other.txAtividade))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TextoAtividadeUnidadeSGD [idTextoAtividadeUnidade=");
		builder.append(idTextoAtividadeUnidade);
		builder.append(", tiAtividade=");
		builder.append(tiAtividade);
		builder.append(", txAtividade=");
		builder.append(txAtividade);
		builder.append(", idAtividadeUnidade=");
		builder.append(idAtividadeUnidade);
		builder.append("]");
		return builder.toString();
	}
	
	

}
