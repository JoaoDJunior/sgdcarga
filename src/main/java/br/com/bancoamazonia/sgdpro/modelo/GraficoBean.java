package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;
import java.math.BigDecimal;

public class GraficoBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal 	ID_ESCORE;
	private String 		NO_PADRAO_DESEMPENHO;

	public GraficoBean() {
		super();
	}

	public GraficoBean(BigDecimal iD_ESCORE, String nO_PADRAO_DESEMPENHO) {
		super();
		ID_ESCORE = iD_ESCORE;
		NO_PADRAO_DESEMPENHO = nO_PADRAO_DESEMPENHO;
	}
	public BigDecimal getID_ESCORE() {
		return ID_ESCORE;
	}
	public void setID_ESCORE(BigDecimal iD_ESCORE) {
		ID_ESCORE = iD_ESCORE;
	}
	public String getNO_PADRAO_DESEMPENHO() {
		return NO_PADRAO_DESEMPENHO;
	}
	public void setNO_PADRAO_DESEMPENHO(String nO_PADRAO_DESEMPENHO) {
		NO_PADRAO_DESEMPENHO = nO_PADRAO_DESEMPENHO;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID_ESCORE == null) ? 0 : ID_ESCORE.hashCode());
		result = prime * result + ((NO_PADRAO_DESEMPENHO == null) ? 0 : NO_PADRAO_DESEMPENHO.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraficoBean other = (GraficoBean) obj;
		if (ID_ESCORE == null) {
			if (other.ID_ESCORE != null)
				return false;
		} else if (!ID_ESCORE.equals(other.ID_ESCORE))
			return false;
		if (NO_PADRAO_DESEMPENHO == null) {
			if (other.NO_PADRAO_DESEMPENHO != null)
				return false;
		} else if (!NO_PADRAO_DESEMPENHO.equals(other.NO_PADRAO_DESEMPENHO))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GraficoBean [ID_ESCORE=");
		builder.append(ID_ESCORE);
		builder.append(", NO_PADRAO_DESEMPENHO=");
		builder.append(NO_PADRAO_DESEMPENHO);
		builder.append("]");
		return builder.toString();
	}



}
