package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;

public class AvaliacaoSGD implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idAvaliacaoEmpregado;
	private String nuMatriculaEmpregado;
	private String txPeriodoAvaliacao;
	private String nuMatriculaAvaliador;
	private String dtAvaliacao;
	private String dtCiencia;
	private String anoAvaliacao;
	private String cargoEmpregado;
	private String funcaoEmpregado;
	private String nomeAvaliador;
	private String codigoUnidade;
	private String nomeUnidade;
	
	public AvaliacaoSGD() {
	}

	public AvaliacaoSGD(String idAvaliacaoEmpregado,
			String nuMatriculaEmpregado, String txPeriodoAvaliacao,
			String nuMatriculaAvaliador, String dtAvaliacao, String dtCiencia,
			String anoAvaliacao, String cargoEmpregado, String funcaoEmpregado,
			String nomeAvaliador, String codigoUnidade, String nomeUnidade) {
		super();
		this.idAvaliacaoEmpregado = idAvaliacaoEmpregado;
		this.nuMatriculaEmpregado = nuMatriculaEmpregado;
		this.txPeriodoAvaliacao = txPeriodoAvaliacao;
		this.nuMatriculaAvaliador = nuMatriculaAvaliador;
		this.dtAvaliacao = dtAvaliacao;
		this.dtCiencia = dtCiencia;
		this.anoAvaliacao = anoAvaliacao;
		this.cargoEmpregado = cargoEmpregado;
		this.funcaoEmpregado = funcaoEmpregado;
		this.nomeAvaliador = nomeAvaliador;
		this.codigoUnidade = codigoUnidade;
		this.nomeUnidade = nomeUnidade;
	}

	public String getIdAvaliacaoEmpregado() {
		return idAvaliacaoEmpregado;
	}

	public void setIdAvaliacaoEmpregado(String idAvaliacaoEmpregado) {
		this.idAvaliacaoEmpregado = idAvaliacaoEmpregado;
	}

	public String getNuMatriculaEmpregado() {
		return nuMatriculaEmpregado;
	}

	public void setNuMatriculaEmpregado(String nuMatriculaEmpregado) {
		this.nuMatriculaEmpregado = nuMatriculaEmpregado;
	}

	public String getTxPeriodoAvaliacao() {
		return txPeriodoAvaliacao;
	}

	public void setTxPeriodoAvaliacao(String txPeriodoAvaliacao) {
		this.txPeriodoAvaliacao = txPeriodoAvaliacao;
	}

	public String getNuMatriculaAvaliador() {
		return nuMatriculaAvaliador;
	}

	public void setNuMatriculaAvaliador(String nuMatriculaAvaliador) {
		this.nuMatriculaAvaliador = nuMatriculaAvaliador;
	}

	public String getDtAvaliacao() {
		return dtAvaliacao;
	}

	public void setDtAvaliacao(String dtAvaliacao) {
		this.dtAvaliacao = dtAvaliacao;
	}

	public String getDtCiencia() {
		return dtCiencia;
	}

	public void setDtCiencia(String dtCiencia) {
		this.dtCiencia = dtCiencia;
	}

	public String getAnoAvaliacao() {
		return anoAvaliacao;
	}

	public void setAnoAvaliacao(String anoAvaliacao) {
		this.anoAvaliacao = anoAvaliacao;
	}

	public String getCargoEmpregado() {
		return cargoEmpregado;
	}

	public void setCargoEmpregado(String cargoEmpregado) {
		this.cargoEmpregado = cargoEmpregado;
	}

	public String getFuncaoEmpregado() {
		return funcaoEmpregado;
	}

	public void setFuncaoEmpregado(String funcaoEmpregado) {
		this.funcaoEmpregado = funcaoEmpregado;
	}

	public String getNomeAvaliador() {
		return nomeAvaliador;
	}

	public void setNomeAvaliador(String nomeAvaliador) {
		this.nomeAvaliador = nomeAvaliador;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	public String getNomeUnidade() {
		return nomeUnidade;
	}

	public void setNomeUnidade(String nomeUnidade) {
		this.nomeUnidade = nomeUnidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idAvaliacaoEmpregado == null) ? 0 : idAvaliacaoEmpregado
						.hashCode());
		result = prime
				* result
				+ ((nuMatriculaAvaliador == null) ? 0 : nuMatriculaAvaliador
						.hashCode());
		result = prime
				* result
				+ ((nuMatriculaEmpregado == null) ? 0 : nuMatriculaEmpregado
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoSGD other = (AvaliacaoSGD) obj;
		if (idAvaliacaoEmpregado == null) {
			if (other.idAvaliacaoEmpregado != null)
				return false;
		} else if (!idAvaliacaoEmpregado.equals(other.idAvaliacaoEmpregado))
			return false;
		if (nuMatriculaAvaliador == null) {
			if (other.nuMatriculaAvaliador != null)
				return false;
		} else if (!nuMatriculaAvaliador.equals(other.nuMatriculaAvaliador))
			return false;
		if (nuMatriculaEmpregado == null) {
			if (other.nuMatriculaEmpregado != null)
				return false;
		} else if (!nuMatriculaEmpregado.equals(other.nuMatriculaEmpregado))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AvaliacaoSGD [idAvaliacaoEmpregado=");
		builder.append(idAvaliacaoEmpregado);
		builder.append(", nuMatriculaEmpregado=");
		builder.append(nuMatriculaEmpregado);
		builder.append(", txPeriodoAvaliacao=");
		builder.append(txPeriodoAvaliacao);
		builder.append(", nuMatriculaAvaliador=");
		builder.append(nuMatriculaAvaliador);
		builder.append(", dtAvaliacao=");
		builder.append(dtAvaliacao);
		builder.append(", dtCiencia=");
		builder.append(dtCiencia);
		builder.append(", anoAvaliacao=");
		builder.append(anoAvaliacao);
		builder.append(", cargoEmpregado=");
		builder.append(cargoEmpregado);
		builder.append(", funcaoEmpregado=");
		builder.append(funcaoEmpregado);
		builder.append(", nomeAvaliador=");
		builder.append(nomeAvaliador);
		builder.append(", codigoUnidade=");
		builder.append(codigoUnidade);
		builder.append(", nomeUnidade=");
		builder.append(nomeUnidade);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
