package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;

public class Empregado implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nome;
	private String matricula;
	private String cargo;
	private String funcao;
	private String unidade;
	private String codigoUnidade;
	
	public Empregado() {
	}
	
	

	public Empregado(String matricula) {
		super();
		this.matricula = matricula;
	}



	public Empregado(String nome, String matricula, String cargo,
			String funcao, String unidade, String codigoUnidade) {
		super();
		this.nome = nome;
		this.matricula = matricula;
		this.cargo = cargo;
		this.funcao = funcao;
		this.unidade = unidade;
		this.codigoUnidade = codigoUnidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result
				+ ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empregado other = (Empregado) obj;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empregado [nome=");
		builder.append(nome);
		builder.append(", matricula=");
		builder.append(matricula);
		builder.append(", cargo=");
		builder.append(cargo);
		builder.append(", funcao=");
		builder.append(funcao);
		builder.append(", unidade=");
		builder.append(unidade);
		builder.append(", codigoUnidade=");
		builder.append(codigoUnidade);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
	

}
