package br.com.bancoamazonia.sgdpro.modelo.dto;

import java.io.Serializable;

public class HistoricoLotacaoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String idHistoricoLotacao;
	private String matricula;
	private String codigoUnidade;
	private String dataInicioLotacao;
	private String dataFimLotacao;
	
	public HistoricoLotacaoDTO() {
	}

	public String getIdHistoricoLotacao() {
		return idHistoricoLotacao;
	}

	public void setIdHistoricoLotacao(String idHistoricoLotacao) {
		this.idHistoricoLotacao = idHistoricoLotacao;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	public String getDataInicioLotacao() {
		return dataInicioLotacao;
	}

	public void setDataInicioLotacao(String dataInicioLotacao) {
		this.dataInicioLotacao = dataInicioLotacao;
	}

	public String getDataFimLotacao() {
		return dataFimLotacao;
	}

	public void setDataFimLotacao(String dataFimLotacao) {
		this.dataFimLotacao = dataFimLotacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoUnidade == null) ? 0 : codigoUnidade.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoLotacaoDTO other = (HistoricoLotacaoDTO) obj;
		if (codigoUnidade == null) {
			if (other.codigoUnidade != null)
				return false;
		} else if (!codigoUnidade.equals(other.codigoUnidade))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HistoricoLotacaoDTO [idHistoricoLotacao=");
		builder.append(idHistoricoLotacao);
		builder.append(", matricula=");
		builder.append(matricula);
		builder.append(", codigoUnidade=");
		builder.append(codigoUnidade);
		builder.append(", dataInicioLotacao=");
		builder.append(dataInicioLotacao);
		builder.append(", dataFimLotacao=");
		builder.append(dataFimLotacao);
		builder.append("]");
		return builder.toString();
	}
	
	


}
