package br.com.bancoamazonia.sgdpro.modelo;

import java.io.Serializable;

public class Avaliacao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idAvaliacaoEmpregado;
	private String nome;
	private String justificativa;
	private String nota;
	
	public Avaliacao() {
	}

	public Avaliacao(String nome, String justificativa, String nota) {
		super();
		this.nome = nome;
		this.justificativa = justificativa;
		this.nota = nota;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}
	

	public String getIdAvaliacaoEmpregado() {
		return idAvaliacaoEmpregado;
	}

	public void setIdAvaliacaoEmpregado(String idAvaliacaoEmpregado) {
		this.idAvaliacaoEmpregado = idAvaliacaoEmpregado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((justificativa == null) ? 0 : justificativa.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nota == null) ? 0 : nota.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avaliacao other = (Avaliacao) obj;
		if (justificativa == null) {
			if (other.justificativa != null)
				return false;
		} else if (!justificativa.equals(other.justificativa))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nota == null) {
			if (other.nota != null)
				return false;
		} else if (!nota.equals(other.nota))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Avaliacao [nome=");
		builder.append(nome);
		builder.append(", justificativa=");
		builder.append(justificativa);
		builder.append(", nota=");
		builder.append(nota);
		builder.append(", idAvaliacaoEmpregado=");
		builder.append(idAvaliacaoEmpregado);
		builder.append("]");
		return builder.toString();
	}
	
	

}
