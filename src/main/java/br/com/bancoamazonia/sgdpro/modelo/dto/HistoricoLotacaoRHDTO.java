package br.com.bancoamazonia.sgdpro.modelo.dto;

import java.io.Serializable;

public class HistoricoLotacaoRHDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String matricula;
	private String nome;
	private String lotacao;
	private String codigoUnidade;
	private String dataInicioLotacao;
	private String dataFimLotacao;
	private String motivo;
	
	public HistoricoLotacaoRHDTO() {
	}

	public HistoricoLotacaoRHDTO(String matricula, String nome, String lotacao, String codigoUnidade,
			String dataInicioLotacao, String dataFimLotacao, String motivo) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.lotacao = lotacao;
		this.codigoUnidade = codigoUnidade;
		this.dataInicioLotacao = dataInicioLotacao;
		this.dataFimLotacao = dataFimLotacao;
		this.motivo = motivo;
	}
	
	

	public String getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(String codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLotacao() {
		return lotacao;
	}

	public void setLotacao(String lotacao) {
		this.lotacao = lotacao;
	}

	public String getDataInicioLotacao() {
		return dataInicioLotacao;
	}

	public void setDataInicioLotacao(String dataInicioLotacao) {
		this.dataInicioLotacao = dataInicioLotacao;
	}

	public String getDataFimLotacao() {
		return dataFimLotacao;
	}

	public void setDataFimLotacao(String dataFimLotacao) {
		this.dataFimLotacao = dataFimLotacao;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoLotacaoRHDTO other = (HistoricoLotacaoRHDTO) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HistoricoLotacaoRHDTO [matricula=");
		builder.append(matricula);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", lotacao=");
		builder.append(lotacao);
		builder.append(", codigoUnidade=");
		builder.append(codigoUnidade);
		builder.append(", dataInicioLotacao=");
		builder.append(dataInicioLotacao);
		builder.append(", dataFimLotacao=");
		builder.append(dataFimLotacao);
		builder.append(", motivo=");
		builder.append(motivo);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

}
