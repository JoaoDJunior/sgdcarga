package br.com.bancoamazonia.sgdpro.modelo;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class DadosBean {

	private String MAT;
	private BigDecimal NU_MATRICULA_EMPREGADO;
	private String NOME;
	private String TX_PERIODO_AVALIACAO;
	private BigDecimal ANO_AVALIACAO;
	private Timestamp DT_AVALIACAO;
	private String CARGO_EMPREGADO;
	private String FUNCAO_EMPREGADO;
	private String NOME_UNIDADE;
	private String CODIGO_UNIDADE;
	private String MAT_AVALIADOR;
	private String NOME_AVALIADOR;
	private BigDecimal ID_AVALIACAO_EMPREGADO;
	private String LOTACAO;
	private String TX_ATIVIDADE;


	public DadosBean() {

	}



	public DadosBean(String mAT, BigDecimal nU_MATRICULA_EMPREGADO, String nOME, String tX_PERIODO_AVALIACAO,
			BigDecimal aNO_AVALIACAO, Timestamp dT_AVALIACAO, String cARGO_EMPREGADO, String fUNCAO_EMPREGADO,
			String nOME_UNIDADE, String cODIGO_UNIDADE, String mAT_AVALIADOR, String nOME_AVALIADOR,
			BigDecimal iD_AVALIACAO_EMPREGADO, String lOTACAO, String tX_ATIVIDADE) {
		super();
		MAT = mAT;
		NU_MATRICULA_EMPREGADO = nU_MATRICULA_EMPREGADO;
		NOME = nOME;
		TX_PERIODO_AVALIACAO = tX_PERIODO_AVALIACAO;
		ANO_AVALIACAO = aNO_AVALIACAO;
		DT_AVALIACAO = dT_AVALIACAO;
		CARGO_EMPREGADO = cARGO_EMPREGADO;
		FUNCAO_EMPREGADO = fUNCAO_EMPREGADO;
		NOME_UNIDADE = nOME_UNIDADE;
		CODIGO_UNIDADE = cODIGO_UNIDADE;
		MAT_AVALIADOR = mAT_AVALIADOR;
		NOME_AVALIADOR = nOME_AVALIADOR;
		ID_AVALIACAO_EMPREGADO = iD_AVALIACAO_EMPREGADO;
		LOTACAO = lOTACAO;
		TX_ATIVIDADE = tX_ATIVIDADE;
	}



	public String getMAT() {
		return MAT;
	}

	public void setMAT(String mAT) {
		MAT = mAT;
	}

	public BigDecimal getNU_MATRICULA_EMPREGADO() {
		return NU_MATRICULA_EMPREGADO;
	}

	public void setNU_MATRICULA_EMPREGADO(BigDecimal nU_MATRICULA_EMPREGADO) {
		NU_MATRICULA_EMPREGADO = nU_MATRICULA_EMPREGADO;
	}

	public String getNOME() {
		return NOME;
	}

	public void setNOME(String nOME) {
		NOME = nOME;
	}

	public String getTX_PERIODO_AVALIACAO() {
		return TX_PERIODO_AVALIACAO;
	}

	public void setTX_PERIODO_AVALIACAO(String tX_PERIODO_AVALIACAO) {
		TX_PERIODO_AVALIACAO = tX_PERIODO_AVALIACAO;
	}

	public BigDecimal getANO_AVALIACAO() {
		return ANO_AVALIACAO;
	}

	public void setANO_AVALIACAO(BigDecimal aNO_AVALIACAO) {
		ANO_AVALIACAO = aNO_AVALIACAO;
	}

	public Timestamp getDT_AVALIACAO() {
		return DT_AVALIACAO;
	}

	public void setDT_AVALIACAO(Timestamp dT_AVALIACAO) {
		DT_AVALIACAO = dT_AVALIACAO;
	}

	public String getCARGO_EMPREGADO() {
		return CARGO_EMPREGADO;
	}

	public void setCARGO_EMPREGADO(String cARGO_EMPREGADO) {
		CARGO_EMPREGADO = cARGO_EMPREGADO;
	}

	public String getFUNCAO_EMPREGADO() {
		return FUNCAO_EMPREGADO;
	}

	public void setFUNCAO_EMPREGADO(String fUNCAO_EMPREGADO) {
		FUNCAO_EMPREGADO = fUNCAO_EMPREGADO;
	}

	public String getNOME_UNIDADE() {
		return NOME_UNIDADE;
	}

	public void setNOME_UNIDADE(String nOME_UNIDADE) {
		NOME_UNIDADE = nOME_UNIDADE;
	}

	public String getCODIGO_UNIDADE() {
		return CODIGO_UNIDADE;
	}

	public void setCODIGO_UNIDADE(String cODIGO_UNIDADE) {
		CODIGO_UNIDADE = cODIGO_UNIDADE;
	}

	public String getMAT_AVALIADOR() {
		return MAT_AVALIADOR;
	}

	public void setMAT_AVALIADOR(String mAT_AVALIADOR) {
		MAT_AVALIADOR = mAT_AVALIADOR;
	}

	public String getNOME_AVALIADOR() {
		return NOME_AVALIADOR;
	}

	public void setNOME_AVALIADOR(String nOME_AVALIADOR) {
		NOME_AVALIADOR = nOME_AVALIADOR;
	}

	public BigDecimal getID_AVALIACAO_EMPREGADO() {
		return ID_AVALIACAO_EMPREGADO;
	}

	public void setID_AVALIACAO_EMPREGADO(BigDecimal iD_AVALIACAO_EMPREGADO) {
		ID_AVALIACAO_EMPREGADO = iD_AVALIACAO_EMPREGADO;
	}

	public String getLOTACAO() {
		return LOTACAO;
	}

	public void setLOTACAO(String lOTACAO) {
		LOTACAO = lOTACAO;
	}

	public String getTX_ATIVIDADE() {
		return TX_ATIVIDADE;
	}

	public void setTX_ATIVIDADE(String tX_ATIVIDADE) {
		TX_ATIVIDADE = tX_ATIVIDADE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ANO_AVALIACAO == null) ? 0 : ANO_AVALIACAO.hashCode());
		result = prime * result + ((CARGO_EMPREGADO == null) ? 0 : CARGO_EMPREGADO.hashCode());
		result = prime * result + ((CODIGO_UNIDADE == null) ? 0 : CODIGO_UNIDADE.hashCode());
		result = prime * result + ((DT_AVALIACAO == null) ? 0 : DT_AVALIACAO.hashCode());
		result = prime * result + ((FUNCAO_EMPREGADO == null) ? 0 : FUNCAO_EMPREGADO.hashCode());
		result = prime * result + ((ID_AVALIACAO_EMPREGADO == null) ? 0 : ID_AVALIACAO_EMPREGADO.hashCode());
		result = prime * result + ((LOTACAO == null) ? 0 : LOTACAO.hashCode());
		result = prime * result + ((MAT == null) ? 0 : MAT.hashCode());
		result = prime * result + ((MAT_AVALIADOR == null) ? 0 : MAT_AVALIADOR.hashCode());
		result = prime * result + ((NOME == null) ? 0 : NOME.hashCode());
		result = prime * result + ((NOME_AVALIADOR == null) ? 0 : NOME_AVALIADOR.hashCode());
		result = prime * result + ((NOME_UNIDADE == null) ? 0 : NOME_UNIDADE.hashCode());
		result = prime * result + ((NU_MATRICULA_EMPREGADO == null) ? 0 : NU_MATRICULA_EMPREGADO.hashCode());
		result = prime * result + ((TX_ATIVIDADE == null) ? 0 : TX_ATIVIDADE.hashCode());
		result = prime * result + ((TX_PERIODO_AVALIACAO == null) ? 0 : TX_PERIODO_AVALIACAO.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DadosBean other = (DadosBean) obj;
		if (ANO_AVALIACAO == null) {
			if (other.ANO_AVALIACAO != null)
				return false;
		} else if (!ANO_AVALIACAO.equals(other.ANO_AVALIACAO))
			return false;
		if (CARGO_EMPREGADO == null) {
			if (other.CARGO_EMPREGADO != null)
				return false;
		} else if (!CARGO_EMPREGADO.equals(other.CARGO_EMPREGADO))
			return false;
		if (CODIGO_UNIDADE == null) {
			if (other.CODIGO_UNIDADE != null)
				return false;
		} else if (!CODIGO_UNIDADE.equals(other.CODIGO_UNIDADE))
			return false;
		if (DT_AVALIACAO == null) {
			if (other.DT_AVALIACAO != null)
				return false;
		} else if (!DT_AVALIACAO.equals(other.DT_AVALIACAO))
			return false;
		if (FUNCAO_EMPREGADO == null) {
			if (other.FUNCAO_EMPREGADO != null)
				return false;
		} else if (!FUNCAO_EMPREGADO.equals(other.FUNCAO_EMPREGADO))
			return false;
		if (ID_AVALIACAO_EMPREGADO == null) {
			if (other.ID_AVALIACAO_EMPREGADO != null)
				return false;
		} else if (!ID_AVALIACAO_EMPREGADO.equals(other.ID_AVALIACAO_EMPREGADO))
			return false;
		if (LOTACAO == null) {
			if (other.LOTACAO != null)
				return false;
		} else if (!LOTACAO.equals(other.LOTACAO))
			return false;
		if (MAT == null) {
			if (other.MAT != null)
				return false;
		} else if (!MAT.equals(other.MAT))
			return false;
		if (MAT_AVALIADOR == null) {
			if (other.MAT_AVALIADOR != null)
				return false;
		} else if (!MAT_AVALIADOR.equals(other.MAT_AVALIADOR))
			return false;
		if (NOME == null) {
			if (other.NOME != null)
				return false;
		} else if (!NOME.equals(other.NOME))
			return false;
		if (NOME_AVALIADOR == null) {
			if (other.NOME_AVALIADOR != null)
				return false;
		} else if (!NOME_AVALIADOR.equals(other.NOME_AVALIADOR))
			return false;
		if (NOME_UNIDADE == null) {
			if (other.NOME_UNIDADE != null)
				return false;
		} else if (!NOME_UNIDADE.equals(other.NOME_UNIDADE))
			return false;
		if (NU_MATRICULA_EMPREGADO == null) {
			if (other.NU_MATRICULA_EMPREGADO != null)
				return false;
		} else if (!NU_MATRICULA_EMPREGADO.equals(other.NU_MATRICULA_EMPREGADO))
			return false;
		if (TX_ATIVIDADE == null) {
			if (other.TX_ATIVIDADE != null)
				return false;
		} else if (!TX_ATIVIDADE.equals(other.TX_ATIVIDADE))
			return false;
		if (TX_PERIODO_AVALIACAO == null) {
			if (other.TX_PERIODO_AVALIACAO != null)
				return false;
		} else if (!TX_PERIODO_AVALIACAO.equals(other.TX_PERIODO_AVALIACAO))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmpregadoBean [MAT=");
		builder.append(MAT);
		builder.append(", NU_MATRICULA_EMPREGADO=");
		builder.append(NU_MATRICULA_EMPREGADO);
		builder.append(", NOME=");
		builder.append(NOME);
		builder.append(", TX_PERIODO_AVALIACAO=");
		builder.append(TX_PERIODO_AVALIACAO);
		builder.append(", ANO_AVALIACAO=");
		builder.append(ANO_AVALIACAO);
		builder.append(", DT_AVALIACAO=");
		builder.append(DT_AVALIACAO);
		builder.append(", CARGO_EMPREGADO=");
		builder.append(CARGO_EMPREGADO);
		builder.append(", FUNCAO_EMPREGADO=");
		builder.append(FUNCAO_EMPREGADO);
		builder.append(", NOME_UNIDADE=");
		builder.append(NOME_UNIDADE);
		builder.append(", CODIGO_UNIDADE=");
		builder.append(CODIGO_UNIDADE);
		builder.append(", MAT_AVALIADOR=");
		builder.append(MAT_AVALIADOR);
		builder.append(", NOME_AVALIADOR=");
		builder.append(NOME_AVALIADOR);
		builder.append(", ID_AVALIACAO_EMPREGADO=");
		builder.append(ID_AVALIACAO_EMPREGADO);
		builder.append(", LOTACAO=");
		builder.append(LOTACAO);
		builder.append(", TX_ATIVIDADE=");
		builder.append(TX_ATIVIDADE);
		builder.append("]");
		return builder.toString();
	}

}
