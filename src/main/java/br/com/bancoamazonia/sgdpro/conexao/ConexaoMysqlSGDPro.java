package br.com.bancoamazonia.sgdpro.conexao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ConexaoMysqlSGDPro implements Serializable {

	//private static final Logger LOG = Logger.getLogger(ConexaoMysqlSGDPro.class);
	private static final long serialVersionUID = 1L;

	private static Connection conexao;

	private  ConexaoMysqlSGDPro() {

	}

	public static Connection getConexao() {
		 try {

			Class.forName("com.mysql.jdbc.Driver");
			//LOG.info("Carregando conexao");
			try {
				conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/pratic_sgdpro", "root", "");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
          return conexao;
	}

	public static void fecharConexao() throws Exception {
		try {
			if (!conexao.isClosed()) {
				conexao.close();
			}
		} catch (SQLException e) {
			throw new Exception("Nao foi possivel fechar a conexao");
		}
	}


}
