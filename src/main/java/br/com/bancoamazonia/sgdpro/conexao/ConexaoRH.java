package br.com.bancoamazonia.sgdpro.conexao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ConexaoRH implements Serializable {

	private static final Logger LOG = Logger.getLogger(ConexaoRH.class);
	private static final long serialVersionUID = 1L;

	private static Connection conexao;

	private  ConexaoRH() {

	}

	public static Connection getConexao() {
		 try {
			Class.forName("oracle.jdbc.OracleDriver");
			LOG.info("Carregando conexao");
			try {
				conexao = DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.4.8.12)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = ebsp1)))", "apps", "vagabonds");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
          return conexao;
	}

	public static void fecharConexao() throws Exception {
		try {
			if (!conexao.isClosed()) {
				conexao.close();
			}
		} catch (SQLException e) {
			throw new Exception("Nao foi possivel fechar a conexao");
		}
	}


}
