package br.com.bancoamazonia.sgdpro.vo;

public class AvaliacaoEmpregadoVO {
	
	private String matricula; 
	private String nome;
	
	public AvaliacaoEmpregadoVO() {
		super();
	}

	public AvaliacaoEmpregadoVO(String matricula, String nome) {
		super();
		this.matricula = matricula;
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AvaliacaoEmpregadoVO [matricula=");
		builder.append(matricula);
		builder.append(", nome=");
		builder.append(nome);
		builder.append("]");
		return builder.toString();
	}
	
	
}
