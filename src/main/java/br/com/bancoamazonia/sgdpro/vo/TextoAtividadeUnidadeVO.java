package br.com.bancoamazonia.sgdpro.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author 7874
 *
 */
public class TextoAtividadeUnidadeVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String descricao;
	private List<TextAtividadeUnidadeResultadoVO> atividadeUnidadeResultadoVOs;
	
	public TextoAtividadeUnidadeVO() {
	}


	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public List<TextAtividadeUnidadeResultadoVO> getAtividadeUnidadeResultadoVOs() {
		return atividadeUnidadeResultadoVOs;
	}


	public void setAtividadeUnidadeResultadoVOs(
			List<TextAtividadeUnidadeResultadoVO> atividadeUnidadeResultadoVOs) {
		this.atividadeUnidadeResultadoVOs = atividadeUnidadeResultadoVOs;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TextoAtividadeUnidadeVO [descricao=");
		builder.append(descricao);
		builder.append(", atividadeUnidadeResultadoVOs=");
		builder.append(atividadeUnidadeResultadoVOs);
		builder.append("]");
		return builder.toString();
	}

	
	
	
	


}
