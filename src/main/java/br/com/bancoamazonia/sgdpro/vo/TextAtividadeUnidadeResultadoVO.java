package br.com.bancoamazonia.sgdpro.vo;

import java.io.Serializable;

public class TextAtividadeUnidadeResultadoVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Double orcamento;
	private Double realizado;
	private Double meta;
	private Double desvio;
	private Double nota;
	private Double peso;
	private Double notaMF;
	private Double notaGeral;
	
	public TextAtividadeUnidadeResultadoVO() {
	}

	public Double getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Double orcamento) {
		this.orcamento = orcamento;
	}

	public Double getRealizado() {
		return realizado;
	}

	public void setRealizado(Double realizado) {
		this.realizado = realizado;
	}

	public Double getMeta() {
		return meta;
	}

	public void setMeta(Double meta) {
		this.meta = meta;
	}

	public Double getDesvio() {
		return desvio;
	}

	public void setDesvio(Double desvio) {
		this.desvio = desvio;
	}

	public Double getNota() {
		return nota;
	}

	public void setNota(Double nota) {
		this.nota = nota;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Double getNotaMF() {
		return notaMF;
	}

	public void setNotaMF(Double notaMF) {
		this.notaMF = notaMF;
	}

	public Double getNotaGeral() {
		return notaGeral;
	}

	public void setNotaGeral(Double notaGeral) {
		this.notaGeral = notaGeral;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desvio == null) ? 0 : desvio.hashCode());
		result = prime * result + ((meta == null) ? 0 : meta.hashCode());
		result = prime * result + ((nota == null) ? 0 : nota.hashCode());
		result = prime * result
				+ ((notaGeral == null) ? 0 : notaGeral.hashCode());
		result = prime * result + ((notaMF == null) ? 0 : notaMF.hashCode());
		result = prime * result
				+ ((orcamento == null) ? 0 : orcamento.hashCode());
		result = prime * result + ((peso == null) ? 0 : peso.hashCode());
		result = prime * result
				+ ((realizado == null) ? 0 : realizado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextAtividadeUnidadeResultadoVO other = (TextAtividadeUnidadeResultadoVO) obj;
		if (desvio == null) {
			if (other.desvio != null)
				return false;
		} else if (!desvio.equals(other.desvio))
			return false;
		if (meta == null) {
			if (other.meta != null)
				return false;
		} else if (!meta.equals(other.meta))
			return false;
		if (nota == null) {
			if (other.nota != null)
				return false;
		} else if (!nota.equals(other.nota))
			return false;
		if (notaGeral == null) {
			if (other.notaGeral != null)
				return false;
		} else if (!notaGeral.equals(other.notaGeral))
			return false;
		if (notaMF == null) {
			if (other.notaMF != null)
				return false;
		} else if (!notaMF.equals(other.notaMF))
			return false;
		if (orcamento == null) {
			if (other.orcamento != null)
				return false;
		} else if (!orcamento.equals(other.orcamento))
			return false;
		if (peso == null) {
			if (other.peso != null)
				return false;
		} else if (!peso.equals(other.peso))
			return false;
		if (realizado == null) {
			if (other.realizado != null)
				return false;
		} else if (!realizado.equals(other.realizado))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TextAtividadeUnidadeResultadoVO [orcamento=");
		builder.append(orcamento);
		builder.append(", realizado=");
		builder.append(realizado);
		builder.append(", meta=");
		builder.append(meta);
		builder.append(", desvio=");
		builder.append(desvio);
		builder.append(", nota=");
		builder.append(nota);
		builder.append(", peso=");
		builder.append(peso);
		builder.append(", notaMF=");
		builder.append(notaMF);
		builder.append(", notaGeral=");
		builder.append(notaGeral);
		builder.append("]");
		return builder.toString();
	}
	
	

}
