package br.com.bancoamazonia.sgdpro.exportacao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.AvaliacaoMySqlProDAO;
import br.com.bancoamazonia.sgdpro.dao.SgdDAO;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoEmpregado;
import br.com.bancoamazonia.sgdpro.modelo.AvaliacaoSGD;

public class ExportaAvaliacaoEmpregadoCorrigir {


	public static void main(String[] args)
	{
		final Logger LOG = Logger.getLogger(ExportaAvaliacaoEmpregadoCorrigir.class);
		LOG.info("============================ CORRIGIR CARGA ANTERIOR ================================");

		AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
		SgdDAO sgdDAO = new SgdDAO();

		int possuiAvaliacao = 0;
		int naopossuiavalicao = 0;

		List<AvaliacaoEmpregado> listarAvaliacoes = avaliacaoDAO.listarAvaliacoes();
		if(listarAvaliacoes != null && listarAvaliacoes.size() > 0)
		{

			for (AvaliacaoEmpregado avaliacaoEmpregado : listarAvaliacoes)
			{
				LOG.info(" //MATRICULA ->" + avaliacaoEmpregado.getEmpregado().getMatricula());
				AvaliacaoSGD buscarAvaliacaoFuncionario = sgdDAO.buscarAvaliacaoFuncionario(avaliacaoEmpregado.getEmpregado().getMatricula(), avaliacaoEmpregado.getPeriodo(), avaliacaoEmpregado.getAno());
				if(buscarAvaliacaoFuncionario != null)
				{
					possuiAvaliacao++;
					sgdDAO.gerarSqlRemoverAvalicaoFuncionario(avaliacaoEmpregado.getEmpregado().getMatricula());
					List<String> geraSqlAvaliacoesEmpregado = avaliacaoDAO.geraSqlAvaliacoesEmpregado(avaliacaoEmpregado);

					for (String sqlAvaliacaoFuncionario : geraSqlAvaliacoesEmpregado) {
						LOG.info(sqlAvaliacaoFuncionario);
					}
				}else{
					naopossuiavalicao++;
					List<String> exportarAvaliacaoEmpregado = avaliacaoDAO.exportarAvaliacaoPorFuncionario(avaliacaoEmpregado);
					for (String linha : exportarAvaliacaoEmpregado) {
						LOG.info(linha);
					}

				}


			}
		}

		LOG.info(" Total de "+ possuiAvaliacao +" possui avaliação e Total de "+ naopossuiavalicao+" não possui avaliação. ");
		LOG.info("============================ FIM DO PROCESSAMENTO DA CARGA ANTERIOR ================================");

	}

	public List<String> gerarSql() {
		final Logger LOG = Logger.getLogger(ExportaAvaliacaoEmpregadoCorrigir.class);
		LOG.info("============================ CORRIGIR CARGA ANTERIOR ================================");

		AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
		SgdDAO sgdDAO = new SgdDAO();
		List<String> dados = new ArrayList<>();

		int possuiAvaliacao = 0;
		int naopossuiavalicao = 0;

		List<AvaliacaoEmpregado> listarAvaliacoes = avaliacaoDAO.listarAvaliacoes();
		if(listarAvaliacoes != null && listarAvaliacoes.size() > 0)
		{

			for (AvaliacaoEmpregado avaliacaoEmpregado : listarAvaliacoes)
			{
				LOG.info(" //MATRICULA ->" + avaliacaoEmpregado.getEmpregado().getMatricula());
				AvaliacaoSGD buscarAvaliacaoFuncionario = sgdDAO.buscarAvaliacaoFuncionario(avaliacaoEmpregado.getEmpregado().getMatricula(), avaliacaoEmpregado.getPeriodo(), avaliacaoEmpregado.getAno());
				if(buscarAvaliacaoFuncionario != null)
				{
					possuiAvaliacao++;
					String sqlRemoverAvaliacaoFuncionario = sgdDAO.gerarSqlRemoverAvalicaoFuncionario(avaliacaoEmpregado.getEmpregado().getMatricula());
					List<String> geraSqlAvaliacoesEmpregado = avaliacaoDAO.geraSqlAvaliacoesEmpregado(avaliacaoEmpregado);
					geraSqlAvaliacoesEmpregado.add(sqlRemoverAvaliacaoFuncionario);

					for (String sqlAvaliacaoFuncionario : geraSqlAvaliacoesEmpregado) {
						LOG.info(sqlAvaliacaoFuncionario);
						dados.add(sqlAvaliacaoFuncionario);
					}
				}else{
					naopossuiavalicao++;
					List<String> exportarAvaliacaoEmpregado = avaliacaoDAO.exportarAvaliacaoPorFuncionario(avaliacaoEmpregado);
					for (String linha : exportarAvaliacaoEmpregado) {
						LOG.info(linha);
						dados.add(linha);
					}

				}


			}
		}

		LOG.info(" Total de "+ possuiAvaliacao +" possui avaliação e Total de "+ naopossuiavalicao+" não possui avaliação. ");
		LOG.info("============================ FIM DO PROCESSAMENTO DA CARGA ANTERIOR ================================");
		return dados;

	}

}
