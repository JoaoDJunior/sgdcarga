package br.com.bancoamazonia.sgdpro.exportacao;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.dao.AvaliacaoMySqlProDAO;





public class ExportaAvaliacaoEmpregado {
	
	private static final Logger LOG = Logger.getLogger(ExportaAvaliacaoEmpregado.class);
	
	public static void main(String[] args) 
	{
		LOG.info("==================== CARREGA DADOS ================================");
		AvaliacaoMySqlProDAO avaliacaoDAO = new AvaliacaoMySqlProDAO();
		List<String> exportarAvaliacaoEmpregado = avaliacaoDAO.exportarAvaliacaoEmpregado("1", "2016");
		
		for (String linha : exportarAvaliacaoEmpregado) {
			LOG.info(linha);
		}
		
		
	}

}
