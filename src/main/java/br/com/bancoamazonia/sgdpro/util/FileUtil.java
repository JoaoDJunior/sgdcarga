package br.com.bancoamazonia.sgdpro.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.bancoamazonia.sgdpro.modelo.DesempenhoPadrao;

public class FileUtil {

	private static final Logger LOG = Logger.getLogger(FileUtil.class);

	public static String cortaTexto(String texto){
		int valor = texto.length();

		if(valor >= 100)
		{
			valor = valor / 2;
			texto = texto.substring(0, valor);

		}else if(valor >= 200 )
		{
			valor = valor / 4;
			texto = texto.substring(0, valor);

		}else if( valor > 200)
		{
			valor = valor / 8;
			texto = texto.substring(0, valor);

		}
		return texto;
	}

	public static String getDataPorPeriodo(String periodo, String ano){
		return ("2".equals(periodo)) ? "01/12/"+ano : "01/01/"+ano;
	}

	public static int analisarDesempenho(String texto){
		for (DesempenhoPadrao desempenho : DesempenhoPadrao.values()) {
			if(String.valueOf(desempenho).equals(texto)){
				return desempenho.getValor();
			}
		}
		return 0;
	}


	public static boolean validaCampo(String valor){
		valor = FileUtil.modificaTexto(valor, " ", "");
		if(valor != null && !"".equals(valor.trim()) && !"Peso".equals(valor)){
			return true;
		}
		return false;
	}


	public static String validaCampoExportacao(String valor){

			if(valor != null && !valor.trim().equals("") && !valor.trim().equals("-")){
				valor = FileUtil.modificaTexto(valor, " ", "");
				return valor;
			}
		return null;
	}


	public static double transformaDecimal(String valor){
		if(!"NULL".equals(valor) && valor != null){
			String replace = valor.replace(",", ".");
			return Double.parseDouble(replace) /100;
		}
		return 0;
	}

	public static String verificarCampoNulo(String valor)
	{
		if("-".equals(valor)){
			return null;
		}
		return valor;
	}

	public static String modificaTexto(String texto, String flag, String newFlag){
		if(texto != null){
			return texto.replace(flag, newFlag);
		}
		return null;
	}

	public static String mudarSimbolosPorAcento(String texto){

		texto = texto.replaceAll("#","á");
		texto = texto.replaceAll("¢","ó");
		texto = texto.replaceAll("‚","é");
		texto = texto.replaceAll("‡","ç");
		texto = texto.replaceAll("Æ","ã");
		texto = texto.replaceAll("¡","í");
		texto = texto.replaceAll("£","ú");
		texto = texto.replaceAll("ä","õ");
		texto = texto.replaceAll("‚","é");
		texto = texto.replaceAll("…","à");
		texto = texto.replaceAll("“","ô");
		texto = texto.replaceAll("ˆ","ê");
		texto = texto.replaceAll("ƒ","â");
		return texto;
	}

	public static List<String> lerArquivo(String ARQUIVO) throws Exception {

		File arq = new File(ARQUIVO);
//		BufferedReader ler = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "UTF-8"));
		BufferedReader ler = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "Cp1252"));
//		BufferedReader ler = new BufferedReader(new InputStreamReader(new FileInputStream(arq), "ISO-8859-1"));

		try {
			if (arq.exists()) {
				List<String> lista = new ArrayList<String>();
				while (ler.ready())	{
					lista.add(ler.readLine());
				}
				return lista;
			} else 	{
				LOG.info("Erro: Arquivo nao encontrado!!");
			}
		} finally {
			ler.close();
		}
		return null;
	}


	public static float checkSimilarity(String nome, String checkNome) throws Exception {


	    // Se as strings têm tamanho distinto, obtêm a similaridade de todas as
	        // combinações em que tantos caracteres quanto a diferença entre elas são
	        // inseridos na string de menor tamanho. Retorna a similaridade máxima
	        // entre todas as combinações, descontando um percentual que representa
	        // a diferença em número de caracteres.

	        if(nome.length() != checkNome.length()) {
	            int iDiff = Math.abs(nome.length() - checkNome.length());
	            int iLen = Math.max(nome.length(), checkNome.length());
	            String sBigger, sSmaller, sAux;

	            if(iLen == nome.length()) {
	                sBigger = nome;
	                sSmaller = checkNome;
	            }
	            else {
	                sBigger = checkNome;
	                sSmaller = nome;
	            }

	            float fSim, fMaxSimilarity = Float.MIN_VALUE;
	            for(int i = 0; i <= sSmaller.length(); i++) {
	                sAux = sSmaller.substring(0, i) + sBigger.substring(i, i+iDiff) + sSmaller.substring(i);
	                fSim = checkSimilaritySameSize(sBigger,  sAux);
	                if(fSim > fMaxSimilarity)
	                    fMaxSimilarity = fSim;
	            }
	            return fMaxSimilarity - (1f * iDiff) / iLen;

	        // Se as strings têm o mesmo tamanho, simplesmente compara-as caractere
	        // a caractere. A similaridade advém das diferenças em cada posição.
	        } else
	            return checkSimilaritySameSize(nome, checkNome);
	    }

	    public static float checkSimilaritySameSize(String sString1, String sString2) throws Exception {

	        if(sString1.length() != sString2.length())
	            throw new Exception("Strings devem ter o mesmo tamanho!");

	        int iLen = sString1.length();
	        int iDiffs = 0;

	        // Conta as diferenças entre as strings
	        for(int i = 0; i < iLen; i++)
	            if(sString1.charAt(i) != sString2.charAt(i))
	                iDiffs++;

	        // Calcula um percentual entre 0 e 1, sendo 0 completamente diferente e
	        // 1 completamente igual
	        return 1f - (float) iDiffs / iLen;
	    }

}
