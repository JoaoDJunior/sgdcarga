package br.com.bancoamazonia.relatorios;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class AbreRelatorio {

		private InputStream caminho;

		private String caminhoPack;

		public void BuscaRelatorio() {
			this.caminho = this.getClass().getClassLoader().getResourceAsStream("");
			this.caminhoPack = this.caminho + "br/com/bancoamazonia/relatorios/";
			System.out.println(caminho);
		}

		public String getCaminhoPack() {
			return this.caminhoPack;
		}

		public InputStream getCaminho() {
			return this.caminho;
		}

		public Map Parametros() {
			Map parametros = new HashMap();
			parametros.put("matricula", "4482");
			parametros.put("semestre", "1%");
			parametros.put("ano", "2005");

			return parametros;

		}

		public void ExecutaRelatorio() throws JRException, SQLException, FileNotFoundException {
		// note que estamos chamando o novo relatorio
			File caminho = new File("C:/Users/14208/JaspersoftWorkspace/SGD_prod/Empregado.jasper");

			JRDataSource dataSource = new JREmptyDataSource();
			//JasperReport report = JasperCompileManager.compileReport( caminho );
			ConnectionFactory connectionFactory = new ConnectionFactory();

			Map parametros = new HashMap();
			parametros.put("matricula", "4482");
			parametros.put("semestre", "1%");
			parametros.put("ano", "2005");
			parametros.put("REPORT_CONNECTION", connectionFactory.getSGDConnection());
			//parametros.put("SUBREPORT_DIR", "C:/Users/14208/JaspersoftWorkspace/SGD_prod/");


			Connection conn = connectionFactory.getSGDConnection();

			//JRDataSource jrDataSource = new JRBeanCollectionDataSource(lista);

			JasperPrint print = JasperFillManager.fillReport(caminho.getPath(), parametros , conn);

			File outDir = new File("C:/Users/14208/Documents/Jars/JasperOut");
			outDir.mkdir();

			JasperExportManager.exportReportToPdfFile(print,"C:/Users/14208/Documents/Jars/JasperOut/Relatorio.pdf");
			JasperViewer view = new JasperViewer(print, false);
			view.setExtendedState(JasperViewer.MAXIMIZED_BOTH);
			view.setDefaultCloseOperation(JasperViewer.DISPOSE_ON_CLOSE);
			view.setVisible(true);
	//		JRPdfExporter exporter = new JRPdfExporter();

	//		ExporterInput exporterInput = new SimpleExporterInput(print);

	//		OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput("C:/Users/14208/Documents/Jars/JasperOut/Relatorio.pdf");

	//		exporter.setExporterOutput(exporterOutput);

	//		SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
	//        exporter.setConfiguration(configuration);

	 //       exporter.exportReport();

			System.out.println("Selou!");
		}

}
