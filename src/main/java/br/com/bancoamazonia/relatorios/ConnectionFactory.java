package br.com.bancoamazonia.relatorios;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.management.Query;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;

public class ConnectionFactory implements Serializable {

    static {
        try {
            Class.forName( "oracle.jdbc.OracleDriver" );


        } catch ( ClassNotFoundException exc ) {

            exc.printStackTrace();

        }
    }

    public static Connection getConnection(
            String url,
            String usuario,
            String senha ) throws SQLException {
        return DriverManager.getConnection( url, usuario, senha );

    }

    public static Connection getSGDConnection() throws SQLException {

        return getConnection(
                "jdbc:oracle:thin:sad@//oranexp.bancoamazonia.sa:1521/nexp.basa.com",
                "sad",
                "sad_prd" );

    }

    public List<EmpregadoBean> consultaEmpregado() throws SQLException, JRException {

    	List<EmpregadoBean> empregado = new ArrayList<EmpregadoBean>();

	    StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder.append("select ai.*, ai2.lotacao, dbms_lob.substr( TX_ATIVIDADE, 4000, 1 ) tx_atividade from ( ");
		sqlBuilder.append("select b.employee_number mat, ");
		sqlBuilder.append("NU_MATRICULA_EMPREGADO, ");
		sqlBuilder.append("b.last_name nome, ");
		sqlBuilder.append("a.TX_PERIODO_AVALIACAO, ");
		sqlBuilder.append("a.ANO_AVALIACAO, ");
		sqlBuilder.append("a.DT_AVALIACAO, ");
		sqlBuilder.append("a.CARGO_EMPREGADO, ");
		sqlBuilder.append("a.FUNCAO_EMPREGADO, ");
		sqlBuilder.append("a.NOME_UNIDADE, ");
		sqlBuilder.append("a.CODIGO_UNIDADE, ");
		sqlBuilder.append("c.employee_number mat_avaliador, ");
		sqlBuilder.append("c.last_name nome_avaliador, ");
		sqlBuilder.append("a.ID_AVALIACAO_EMPREGADO ");
		sqlBuilder.append("from AVALIACAO_EMPREGADO a, ");
		sqlBuilder.append("apps.per_all_people_f@DBL_HR_PROD.BASA.COM b, ");
		sqlBuilder.append("apps.per_all_people_f@DBL_HR_PROD.BASA.COM c ");
		sqlBuilder.append("where ltrim(a.NU_MATRICULA_EMPREGADO) =ltrim(b.employee_number) ");
		sqlBuilder.append("and   ltrim(a.NU_MATRICULA_AVALIADOR) =ltrim(c.employee_number) ");
		sqlBuilder.append("and b.effective_start_date = (select max(a1.effective_start_date) ");
		sqlBuilder.append("                                        from apps.per_all_people_f@DBL_HR_PROD.BASA.COM a1 ");
		sqlBuilder.append("                                        where a1.person_id=b.person_id) ");
		sqlBuilder.append("and c.effective_start_date = (select max(a1.effective_start_date) ");
		sqlBuilder.append("                                        from apps.per_all_people_f@DBL_HR_PROD.BASA.COM a1 ");
		sqlBuilder.append("                                        where a1.person_id=c.person_id) ");
		sqlBuilder.append("   and b.employee_number= ? AND a.TX_PERIODO_AVALIACAO LIKE ? AND a.ANO_AVALIACAO = ?  ) ai ");
		sqlBuilder.append("   left join (select ltrim(rtrim(b.employee_number)) matricula  , ");
		sqlBuilder.append("                 ltrim(rtrim(b.last_name))       nome  , ");
		sqlBuilder.append("                e.name lotacao, ");
		sqlBuilder.append("                f1.date_from inicio, ");
		sqlBuilder.append("                f1.date_to fim, ");
		sqlBuilder.append("                d.meaning motivo ");
		sqlBuilder.append("                                   from PER_PERSON_ANALYSES@DBL_HR_PROD.BASA.COM f1, ");
		sqlBuilder.append("                                   apps.PER_ANALYSIS_CRITERIA@DBL_HR_PROD.BASA.COM g1, ");
		sqlBuilder.append("                                   apps.per_all_people_f@DBL_HR_PROD.BASA.COM          b, ");
		sqlBuilder.append("                                   apps.hr_all_organization_units@DBL_HR_PROD.BASA.COM e, ");
		sqlBuilder.append("                                   apps.fnd_lookup_values@DBL_HR_PROD.BASA.COM d ");
		sqlBuilder.append("                                  where f1.analysis_criteria_id = g1.analysis_criteria_id ");
		sqlBuilder.append("                                    and  f1.ID_FLEX_NUM=50334 ");
		sqlBuilder.append("                                     and b.person_id = f1.person_id ");
		sqlBuilder.append("                                     and e.organization_id= g1.segment2 ");
		sqlBuilder.append("                                     and d.lookup_type = 'EMP_ASSIGN_REASON' ");
		sqlBuilder.append("                                     and d.language = 'PTB' ");
		sqlBuilder.append("                                     and d.lookup_code = g1.segment1 ");
		sqlBuilder.append("                                     and b.EFFECTIVE_START_DATE in ( ");
		sqlBuilder.append("                                         select max(EFFECTIVE_START_DATE) from apps.per_all_people_f@DBL_HR_PROD.BASA.COM  b1 ");
		sqlBuilder.append("                                         where b1.person_id=b.person_id ) ");
		sqlBuilder.append("                                     and rtrim(b.employee_number)= ? ");
		sqlBuilder.append("                                     order by f1.date_from desc) ai2 ");
		sqlBuilder.append("on ai.mat=ai2.matricula ");
		sqlBuilder.append("and ai.dt_avaliacao between ai2.inicio and nvl(ai2.fim,sysdate) ");
		sqlBuilder.append("left join ATIVIDADE_EMPREGADO c ");
		sqlBuilder.append("on c.NU_MATRICULA_EMPREGADO = ai.NU_MATRICULA_EMPREGADO ");
		sqlBuilder.append("and c.TX_PERIODO_AVALIACAO=ai.TX_PERIODO_AVALIACAO ");
		sqlBuilder.append("and c.ano_avaliacao= ai.ano_avaliacao ");
		sqlBuilder.append("order by dt_avaliacao DESC ");

		System.out.println(sqlBuilder.toString());

	    PreparedStatement stmt = null;
	    ResultSet rs = null;
		try {
		stmt = getSGDConnection().prepareStatement(sqlBuilder.toString());
		stmt.setString(1, "7874");
		stmt.setString(2, "2%");
		stmt.setString(3, "2016");
		stmt.setString(4, "7874");
	    rs = stmt.executeQuery();
	   // JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
	    EmpregadoBean empregadoBean = new EmpregadoBean();

	    while(rs.next()) {

	    	empregadoBean.setMAT(rs.getString("MAT"));
	    	empregadoBean.setNU_MATRICULA_EMPREGADO(rs.getBigDecimal("NU_MATRICULA_EMPREGADO"));
	    	empregadoBean.setNOME(rs.getString("NOME"));
	    	empregadoBean.setMAT_AVALIADOR(rs.getString("MAT_AVALIADOR"));
	    	empregadoBean.setNOME_AVALIADOR(rs.getString("NOME_AVALIADOR"));
	    	empregadoBean.setLOTACAO(rs.getString("LOTACAO"));
	    	empregadoBean.setCARGO_EMPREGADO(rs.getString("CARGO_EMPREGADO"));
	    	empregadoBean.setCODIGO_UNIDADE(rs.getString("CODIGO_UNIDADE"));
	    	empregadoBean.setNOME_UNIDADE(rs.getString("NOME_UNIDADE"));
	    	empregadoBean.setID_AVALIACAO_EMPREGADO(rs.getBigDecimal("iD_AVALIACAO_EMPREGADO"));
	    	empregadoBean.setDT_AVALIACAO(rs.getTimestamp("DT_AVALIACAO"));
	    	empregadoBean.setANO_AVALIACAO(rs.getBigDecimal("ANO_AVALIACAO"));
	    	empregadoBean.setTX_PERIODO_AVALIACAO(rs.getString("TX_PERIODO_AVALIACAO"));
	    	empregadoBean.setTX_ATIVIDADE(rs.getString("TX_ATIVIDADE"));

	    	empregado.add(empregadoBean);

	    }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
		}
		return empregado;

		//AbreRelatorio abreRelatorio = new AbreRelatorio();
		//abreRelatorio.ExecutaRelatorio(empregado);


	    }
}
