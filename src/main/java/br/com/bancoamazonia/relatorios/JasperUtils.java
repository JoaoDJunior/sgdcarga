package br.com.bancoamazonia.relatorios;

import java.awt.BorderLayout;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;

public class JasperUtils {
	/**
     * Abre um relat�rio usando uma conex�o como datasource.
     *
     * @param titulo T�tulo usado na janela do relat�rio.
     * @param inputStream InputStream que cont�m o relat�rio.
     * @param parametros Par�metros utilizados pelo relat�rio.
     * @param conexao Conex�o utilizada para a execu��o da query.
     * @throws JRException Caso ocorra algum problema na execu��o do relat�rio
     */
    public static void openReport(
            String titulo,
            InputStream inputStream,
            Map parametros,
            Connection conexao ) throws JRException {

        JasperPrint print = JasperFillManager.fillReport(
                inputStream, parametros, conexao );

    }


    public static void openReport(
            String titulo,
            InputStream inputStream,
            Map parametros,
            JRDataSource dataSource ) throws JRException {

        JasperPrint print = JasperFillManager.fillReport(
                inputStream, parametros, dataSource );


    }
}
