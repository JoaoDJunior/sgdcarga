package br.com.bancoamazonia.relatorios;

import java.util.List;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ConsultarAvaliacao {

	private String path;

	private String pathToReportPackage;


	public ConsultarAvaliacao() {
		this.path = this.getClass().getClassLoader().getResource("").getPath();
		this.pathToReportPackage = this.path + "br/com/bancoamazonia/relatorios/";
		System.out.println(path);
	}

		public void imprimir(List<EmpregadoBean> empregado) throws Exception
		{
			JasperReport report = JasperCompileManager.compileReport(this.getPathToReportPackage() + "Clientes.jrxml");

			JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(empregado));

			JasperExportManager.exportReportToPdfFile(print, "c:/Relatorio_de_Clientes.pdf");
		}

		public String getPathToReportPackage() {
			return this.pathToReportPackage;
		}

		public String getPath() {
			return this.path;
		}

}