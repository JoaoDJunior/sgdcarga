-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 18-Abr-2017 às 15:41
-- Versão do servidor: 5.7.16
-- PHP Version: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pratic_sgdpro`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE `avaliacao` (
  `Id` int(11) NOT NULL,
  `id_avaliacao_empregado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao_atividade_unidade`
--

CREATE TABLE `avaliacao_atividade_unidade` (
  `id` int(11) NOT NULL,
  `descricao` longtext,
  `id_atividade_unidade` varchar(255) DEFAULT NULL,
  `id_text_atividade_unidade` varchar(255) DEFAULT NULL,
  `id_avaliacao_unidade` varchar(255) DEFAULT NULL,
  `id_escore` varchar(255) DEFAULT NULL,
  `perc_realizado` varchar(255) DEFAULT NULL,
  `qt_previsto` longtext,
  `qt_realizado` varchar(255) DEFAULT NULL,
  `in_carga_sistema` varchar(255) DEFAULT NULL,
  `desvio` longtext,
  `nota` varchar(255) DEFAULT NULL,
  `peso` varchar(255) DEFAULT NULL,
  `unidade` varchar(255) DEFAULT NULL,
  `id_unidade` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao_empregado`
--

CREATE TABLE `avaliacao_empregado` (
  `Id` int(11) NOT NULL,
  `matriculaEmpregado` varchar(8) DEFAULT NULL,
  `matriculaGestor` varchar(8) DEFAULT NULL,
  `periodo` char(1) DEFAULT NULL,
  `ano` char(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `avaliacao_atividade_unidade`
--
ALTER TABLE `avaliacao_atividade_unidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `avaliacao_empregado`
--
ALTER TABLE `avaliacao_empregado`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avaliacao`
--
ALTER TABLE `avaliacao`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `avaliacao_atividade_unidade`
--
ALTER TABLE `avaliacao_atividade_unidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `avaliacao_empregado`
--
ALTER TABLE `avaliacao_empregado`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
